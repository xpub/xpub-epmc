const path = require('path')
const mime = require('mime-types')
const libxml = require('libxmljs')
const xslt4node = require('xslt4node')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const tar = require('../utils/unTar.js')
const fileUtils = require('../utils/files.js')
const db = require('../utils/db.js')
const logger = require('@pubsweet/logger')
const Manuscript = require('../xpub-model/entities/manuscript')
const File = require('../xpub-model/entities/file/data-access')
const config = require('config')

const { baseUrl } = config.get('pubsweet-server')
const xlinkNamespace = 'http://www.w3.org/1999/xlink'

xslt4node.addLibrary('./saxon9he.jar')
xslt4node.addOptions('-Xmx1g')

module.exports.pushXML = async function pushXML(fileUrl, manuscriptId, userId) {
  const man = await Manuscript.findById(manuscriptId, userId)
  const changeStatus = ['tagging', 'xml-qa'].includes(man.status)
  try {
    await Manuscript.update(
      { id: manuscriptId, formState: null, pdfDepositState: 'CREATING_NXML' },
      userId,
    )
    const xml = await fileUtils.fetchFile(baseUrl + fileUrl)
    const xsdBlue = await fileUtils.readData(
      path.resolve(__dirname, 'xsl/xsd/publishing/journalpublishing3.xsd'),
    )
    const xsdGreen = await fileUtils.readData(
      path.resolve(__dirname, 'xsl/xsd/archiving/archivearticle3.xsd'),
    )

    const xmlIsWellformed = libxml.parseXml(xml)

    const baseUrlB = path.resolve(__dirname, 'xsl/xsd/publishing/')
    const xsdBlueDoc = libxml.parseXml(xsdBlue, {
      baseUrl: `${baseUrlB}/`,
    })

    const baseUrlG = path.resolve(__dirname, 'xsl/xsd/archiving/')
    const xsdGreenDoc = libxml.parseXml(xsdGreen, {
      baseUrl: `${baseUrlG}/`,
    })

    const xmlIsValid = xmlIsWellformed.validate(xsdBlueDoc)

    const errors = xmlIsWellformed.validationErrors
    if (!xmlIsValid) {
      let errString = 'Invalid XML: \n'
      errors.forEach((err, i) => {
        errString += `${err}\nLine: ${err.line}.`
        if (err.str1) {
          errString += ` ID: ${err.str1}.`
        }
        if (i !== errors.length - 1) {
          errString += `\n\n`
        }
      })
      await Manuscript.update(
        {
          id: manuscriptId,
          formState: errString,
          status: changeStatus ? 'xml-triage' : man.status,
          pdfDepositState: null,
        },
        userId,
      )
      logger.error(errString)
    } else {
      // Check XML against the stylechecker
      const checked = await transformXML(
        xml,
        path.resolve(__dirname, 'xsl/stylechecker/nlm-stylechecker.xsl'),
      )

      const result = libxml.parseXml(checked)

      const styleErrors = result.find('//error')

      if (styleErrors.length === 0) {
        const nxml = await transformXML(
          xml,
          path.resolve(__dirname, 'xsl/pnihms2pmc3.xsl'),
        )

        const nxmlIsWellformed = libxml.parseXml(nxml)
        const nxmlIsValid = nxmlIsWellformed.validate(xsdGreenDoc)
        const nxmlErrors = nxmlIsWellformed.validationErrors

        if (!nxmlIsValid) {
          let errString = 'Invalid NXML: \n\n'
          nxmlErrors.forEach((err, i) => {
            errString += `${err}\nLine: ${err.line}.`
            if (err.str1) {
              errString += ` ID: ${err.str1}.`
            }
            if (i !== nxmlErrors.length - 1) {
              errString += `\n\n`
            }
          })
          await Manuscript.update(
            {
              id: manuscriptId,
              formState: errString,
              status: changeStatus ? 'xml-triage' : man.status,
              pdfDepositState: null,
            },
            userId,
          )
          logger.error(errString)
        } else {
          const manuscriptFiles = await File.selectByManuscriptId(manuscriptId)
          const filelist = getFilelist(manuscriptFiles)

          const tmpPath = await tar.createTempDir()
          const filesArr = []

          // now we will transform the nXML into HTML:
          const html = await transformXML(
            nxml,
            path.resolve(__dirname, 'xsl/jats2html.xsl'),
            filelist,
          )

          // save converted.html to tmp folder
          fs.writeFileSync(`${tmpPath}/${manuscriptId}.html`, html)

          // save converted.nxml to tmp folder
          fs.writeFileSync(`${tmpPath}/${manuscriptId}.nxml`, nxml)

          filesArr.push(
            getFileInfo(
              `${tmpPath}/${manuscriptId}.html`,
              manuscriptId,
              userId,
            ),
          )
          filesArr.push(
            getFileInfo(
              `${tmpPath}/${manuscriptId}.nxml`,
              manuscriptId,
              userId,
            ),
          )
          const uuid = uuidv4()

          // upload to minio
          for (let i = 0; i < filesArr.length; i += 1) {
            fileUtils.uploadFileToMinio(
              `${uuid}${filesArr[i].extension}`,
              filesArr[i].filename,
              filesArr[i].url,
              filesArr[i].mimeType,
            )
          }
          const dbFilesArr = filesArr.map(obj => {
            obj.url = `/download/${uuid}${obj.extension}`
            delete obj.extension
            return obj
          })

          await db.upsertFileUrl(dbFilesArr, manuscriptId, userId)

          await Manuscript.update(
            {
              id: manuscriptId,
              pdfDepositState: manuscriptFiles.find(f => f.type === 'pdf4load')
                ? null
                : 'WAITING_FOR_PDF_CONVERSION',
              formState: null,
            },
            userId,
          )

          // Check for referenced files in xml
          const xmlDoc = libxml.parseXml(xml)

          // check for @xlink:href attribute
          const nodes = xmlDoc
            .find('//*[@xlink:href]', {
              xlink: xlinkNamespace,
            })
            .filter(el => ['media', 'graphic'].includes(el.name()))

          const fileNotFound = []
          let fileFound = false

          nodes.forEach(node => {
            const nodeTag = node.name()

            const filename = node
              .get('@xlink:href', { xlink: xlinkNamespace })
              .value()

            if (nodeTag === 'media') {
              // check entire filename including extension
              fileFound = manuscriptFiles.find(el => el.filename === filename)
              if (fileFound === undefined) fileNotFound.push(filename)
            } else if (nodeTag === 'graphic') {
              // check that filename.tif and filename.jpg exist

              const tifFound = manuscriptFiles.find(
                el => el.filename === `${filename}.tif`,
              )

              const jpgFound = manuscriptFiles.find(
                el => el.filename === `${filename}.jpg`,
              )

              if (tifFound === undefined) fileNotFound.push(`${filename}.tif`)
              if (jpgFound === undefined) fileNotFound.push(`${filename}.jpg`)
              fileFound = tifFound && jpgFound
            }
          })

          // update manuscript.formState and send to xml-triage
          if (fileNotFound.length > 0) {
            let errString =
              'Files referenced in the xml are not in the database: \n'
            fileNotFound.forEach(file => {
              errString += `${file}\n`
            })

            await Manuscript.update(
              {
                id: manuscriptId,
                formState: errString,
                status: changeStatus ? 'xml-triage' : man.status,
              },
              userId,
            )
            logger.info(errString)
          }

          await fileUtils.tidyUp(tmpPath)
          logger.info(
            'Uploading of nxml and html files to Minio and the database has been completed.',
          )
        }
      } else {
        let styleErrString = `Style Errors: <br/><br/>`
        styleErrors.forEach((err, i) => {
          styleErrString += err.text()
          if (i !== styleErrors.length - 1) {
            styleErrString += `<br/><br/>`
          }
        })
        await Manuscript.update(
          {
            id: manuscriptId,
            formState: styleErrString,
            status: changeStatus ? 'xml-triage' : man.status,
            pdfDepositState: null,
          },
          userId,
        )
        logger.error(styleErrString)
      }
    }
  } catch (err) {
    await Manuscript.update(
      {
        id: manuscriptId,
        formState: err.message,
        status: changeStatus ? 'xml-triage' : man.status,
        pdfDepositState: null,
      },
      userId,
    )
    logger.error(err.message)
  }
}

function getFileInfo(filepath, manuscriptId, userId) {
  const filename = filepath.substring(filepath.lastIndexOf('/') + 1)
  const fileExt = path.extname(filepath)
  const fileSize = fs.statSync(filepath).size

  const fileInfo = {
    url: filepath,
    filename,
    type: (function getType() {
      if (fileExt === '.nxml') return 'PMCfinal'
      else if (fileExt === '.html') return 'tempHTML'
      else if (fileExt === '.xml') return 'PMC'
      return ''
    })(),
    label: '',
    size: fileSize,
    extension: fileExt,
    mimeType: `${mime.contentType(fileExt)}`,
    manuscriptId,
    updatedBy: userId,
  }
  return fileInfo
}

function transformXML(xmlString, xslPath, params) {
  xmlString = xmlString.replace(/<!DOCTYPE[^>[]*(\\[[^]]*\\])?>/, '')
  const config = {
    xsltPath: xslPath,
    source: xmlString,
    result: String,
    props: {
      indent: 'yes',
    },
  }
  if (params) {
    config.params = params
  }
  return new Promise((resolve, reject) => {
    xslt4node.transform(config, (err, result) => {
      if (err) {
        reject(result)
      } else {
        resolve(result)
      }
    })
  })
}

function getFilelist(files) {
  const types = ['IMGview', 'supplement', 'supplement_tag']
  const list = files.reduce((list, file) => {
    if (!file.deleted && types.includes(file.type)) {
      if (file.type === 'supplement') {
        file.filename = fileUtils.normalizeFilename(file)
      }
      list.push(file)
    }
    return list
  }, [])
  return {
    filelist: list
      .map(
        file =>
          `${file.filename.substring(
            0,
            file.filename.lastIndexOf('.'),
          )}:/api/files/${file.url.split('/').pop()};`,
      )
      .join(''),
  }
}
