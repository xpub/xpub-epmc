const path = require('path')
const chokidar = require('chokidar')
const logger = require('@pubsweet/logger')
const getUser = require('../utils/user.js')
const tar = require('../utils/unTar.js')
const files = require('../utils/files.js')
const db = require('../utils/db.js')
const { execSync } = require('child_process')
const uuidv4 = require('uuid/v4')
const config = require('config')
const manuscriptModel = require('../xpub-model/entities/manuscript')
const { pushXML } = require('../xml-validation')

const ftpTagger = config.get('ftp_tagger')
const {
  processedTaggerEmail,
  bulkUploaderEmail,
  errorDevEmail,
} = require('../email')

const parentRootPath = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}`

const rootPath = `${parentRootPath}/Done`

const ignoreNewFolder = `${parentRootPath}/New`

const errorFolder = `${parentRootPath}/Error`

const cmd = `mkdir -p ${rootPath} ${ignoreNewFolder} ${errorFolder}; chmod 777 ${rootPath} ${ignoreNewFolder} ${errorFolder}`
execSync(cmd)

// chokidar monitors tagged folder
const watcher = chokidar.watch(`${rootPath}**/*.tar.gz`, {
  persistent: true,
  ignored: ignoreNewFolder,
  awaitWriteFinish: {
    stabilityThreshold: 2000,
    pollInterval: 100,
  },
})

process
  .on('uncaughtException', err => {
    logger.error('Uncaught exception', err.stack)
  })
  .on('unhandledRejection', reason => {
    logger.error(`Unhandled Rejection at: ${reason.stack || reason}`)
  })

watcher
  .on('add', path => logger.debug(`File ${path} has been added.`))
  .on('add', path => processFile(path))
  .on('change', path => logger.debug(`File ${path} has been changed`))
  .on('unlink', path => logger.debug(`File ${path} has been removed`))
  .on('error', error => {
    logger.error(`Watcher error: ${error}`)
    errorDevEmail(`Watcher error: ${error}`)
  })
  .on('ready', () =>
    logger.info(
      `Initial scan complete. Ready for changes under folder ${rootPath}`,
    ),
  )
  .on('raw', (event, path, details) => {
    logger.debug('Raw event info:', event, path, details)
  })

watcher.on('change', (path, stats) => {
  if (stats) logger.debug(`File ${path} changed size to ${stats.size}`)
})

async function processFile(filePath) {
  try {
    logger.info(`Processing tagged XML packages: ${filePath}`)
    const user = await getUser.getTaggerUser()
    const userId = user.id
    const tmpPath = await tar.createTempDir()
    const extractedFilePath = await tar.untar(filePath, tmpPath)
    const manifestFilename = await files.getManifestFilename(extractedFilePath)
    const [manuscriptId, filesData] = await files.getManifestFileData(
      extractedFilePath,
      manifestFilename,
    )
    const filesArr = await files.checkFiles(filesData, tmpPath, userId)

    // delete current existing 'tagging' files
    await db.deleteTaggingFiles(manuscriptId, userId)

    // upload to minio
    await Promise.all(
      filesArr.map(async file => {
        const uuid = uuidv4()
        file.minioFileName = `${uuid}${file.extension}`
        await files.uploadFileToMinio(
          file.minioFileName,
          file.filename,
          file.url,
          file.mimeType,
        )
      }),
    )

    const dbFilesArr = filesArr.map(obj => {
      obj.url = `/download/${obj.minioFileName}`
      delete obj.extension
      delete obj.minioFileName
      return obj
    })

    // create rows in 'file' table
    await db.createTaggersFiles(dbFilesArr)

    // generate HTML and nxml files from xml
    const xmlFile = dbFilesArr.find(file => file.mimeType === 'application/xml')
    if (typeof xmlFile !== 'undefined') {
      await pushXML(xmlFile.url, manuscriptId, userId)
    }
    manuscriptModel.update(
      { id: manuscriptId, pdfDepositState: 'WAITING_FOR_PDF_CONVERSION' },
      userId,
    )
    logger.info(
      'Uploading of taggers files to Minio and the database has been completed.',
    )

    const manuscriptObj = await manuscriptModel.findById(manuscriptId, userId)

    // send email to taggers
    processedTaggerEmail(
      ftpTagger.email,
      manuscriptId,
      manuscriptObj.meta.title,
      `${path.basename(filePath)}`,
    )
    // move file to dated folder
    await files.renameFile(filePath)
    // clean up tmp folder
    await files.tidyUp(tmpPath)
  } catch (err) {
    logger.error('Error', err.message)
    const filename = filePath.split('/').pop()
    bulkUploaderEmail(ftpTagger.email, filename, filename)
    files.moveErroneousFile(filePath, errorFolder)
    logger.error('File has been moved to the Error folder')
  }
}
