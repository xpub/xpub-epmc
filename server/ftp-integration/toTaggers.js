const dateFormat = require('dateformat')
const getUser = require('../utils/user.js')
const fileUtils = require('../utils/files.js')
const rfr = require('rfr')
const moment = require('moment')
const logger = require('@pubsweet/logger')
const { exec } = require('child_process')
const http = require('https')
const fs = require('fs')
const os = require('os')
// const fetch = require('node-fetch')
const config = require('config')
const passport = require('passport')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const manuscriptModel = require('../xpub-model/entities/manuscript')

const { taggerEmail, errorDevEmail } = rfr('server/email')
const pubsweetServer = config.get('pubsweet-server.baseUrl')
const ftpTagger = config.get('ftp_tagger')

const ftpLocation = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}/New`

const authBearer = passport.authenticate('bearer', { session: false })

module.exports = app => {
  app.get('/tagging/send', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { manid } = req.query

    await createPackageForTaggers(manid)
    res.send(JSON.stringify('result:success'))
  })
}

// createPackageForTaggers('EMS90001')

// noinspection JSAnnotator
async function createPackageForTaggers(manid) {
  const manuscript = await getManuscript(manid)
  const datedFolder = dateFormat(new Date(), 'yyyy-mm-dd')
  if (manuscript.id) {
    try {
      logger.info(`Sending package ${manid} to tagger FTP location.`)
      const tmpPath = await createTempDir(manuscript)
      await writeFiles(tmpPath, manuscript)
      await createMetadataXML(tmpPath, manuscript)
      await createManifest(tmpPath, manuscript)
      await compress(tmpPath, manuscript, datedFolder)
      await tidyUp(tmpPath)
      logger.info('Tagging package created. Sending email to taggers.')
      await taggerEmail(
        ftpTagger.email,
        manuscript.id,
        manuscript['meta,title'],
        `ftp://${process.env.PUBSWEET_HOST}/New/${datedFolder}/${
          manuscript.id
        }.tar.gz`,
      )
    } catch (error) {
      try {
        if (manuscript) {
          const user = await getUser.getAdminUser()
          await manuscriptModel.update(
            {
              id: manuscript.id,
              status: 'xml-triage',
              formState: error.toString(),
            },
            user.id,
          )
        }
      } catch (e) {
        logger.error('Error', e)
      }
      logger.error('Error', error)
      errorDevEmail(`Error when creating package for manuscript ${manid}
      Error: ${error}`)
    }
  } else {
    logger.error('Error: Unable to retrieve manuscript record')
    errorDevEmail(`Error when creating package for manuscript ${manid}
    Error: Unable to retrieve manuscript record`)
  }
}

function getManuscript(manId) {
  return new Promise((resolve, reject) => {
    Manuscript.findByFieldEager('id', manId, '[journal, files, audits]')
      .then(rows => {
        const manuscript = rows[0]
        manuscript.files = manuscript.files.reduce((list, file) => {
          if (!file.deleted) {
            if (
              ['manuscript', 'supplement', 'figure', 'table'].includes(
                file.type,
              )
            ) {
              if (
                file.type === 'manuscript' &&
                list.some(file => file.type === 'manuscript') &&
                !file.label
              ) {
                file.label = list.length().toString()
              }
              file.filename = fileUtils.normalizeFilename(file)
            }
            list.push(file)
          }
          return list
        }, [])
        resolve(manuscript)
      })
      .catch(err => reject(err))
  })
}

function createTempDir(manuscript) {
  return new Promise((resolve, reject) => {
    // noinspection JSCheckFunctionSignatures

    const directory = `${os.tmpdir()}/${manuscript.id}`

    // if temp directory exists, remove its contents:
    fs.access(directory, fs.constants.F_OK, async err => {
      if (!err) {
        await tidyUp(directory)
      }
      fs.mkdir(directory, err => {
        if (err) reject(err)
        resolve(directory)
      })
    })
  })
}

async function createMetadataXML(tmpPath, manuscript) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
<!-- use DTD http://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd -->
<?properties manuscript?>
<?origin ukpmcpa?>
<nihms-source-xml xmlns:xlink="http://www.w3.org/1999/xlink">
  <journal-meta>`
  if (manuscript.journal) {
    xml += `    <journal-id journal-id-type="nlm-ta">${
      manuscript.journal['meta,nlmta']
    }</journal-id>
    <journal-title-group>
      <journal-title>${manuscript.journal.journalTitle}</journal-title>
    </journal-title-group>
    ${
      manuscript.journal['meta,issn'].length > 0
        ? manuscript.journal['meta,issn']
            .map(
              issn =>
                `<issn pub-type="${issn.type.toLowerCase().charAt(0)}pub">${
                  issn.id
                }</issn>`,
            )
            .join('\n')
        : '<issn pub-type="ppub"/>\n'
    }`
  } else {
    // else try from the unmatched journal column
    xml += `<journal-title-group>
      <journal-title>${manuscript['meta,unmatchedJournal']}</journal-title>
    </journal-title-group>`
  }
  xml += `\n  </journal-meta>
  <article-id pub-id-type="manuscript">${manuscript.id}</article-id>
  ${
    manuscript['meta,publicationDates'] &&
    manuscript['meta,publicationDates'].length > 0
      ? manuscript['meta,publicationDates']
          .map(pDate => {
            let dateStr = `<pub-date pub-type="${pDate.type}">\n`
            if (pDate.jatsDate) {
              dateStr += Object.keys(pDate.jatsDate)
                .sort()
                .map(el =>
                  pDate.jatsDate[el]
                    ? `    <${el}>${pDate.jatsDate[el]}</${el}>\n`
                    : '',
                )
                .join('')
            } else {
              dateStr += moment(pDate.date).format(
                '[<day>]DD[</day>\n    <month>]MM[</month>\n    <year>]YYYY[</year>\n]',
              )
            }
            dateStr += '</pub-date>'
            return dateStr
          })
          .join('\n')
      : ''
  }`
  // #874 As discussed the NIHMS submitted date should now be the date the first manuscript file was first uploaded.
  try {
    const submittedDate = manuscript.audits.find(
      audit =>
        audit.objectType === 'file' && audit.changes.type === 'manuscript',
    ).created
    xml += `  <pub-date pub-type="nihms-submitted">
    ${moment(submittedDate).format(
      '[<day>]DD[</day>\n    <month>]MM[</month>\n    <year>]YYYY[</year>\n]',
    )}
  </pub-date>\n`
  } catch (ignored) {
    logger.debug('Did not create nihms-submitted date ')
  }

  try {
    const metadata = await fs.readFileSync(`${tmpPath}/metadata.xml`, 'utf8')
    xml += `${metadata}\n`
  } catch (ignored) {
    // ignored
  }

  // supplementary files Issue #290
  const suppFiles = manuscript.files
    .filter(file => file.type === 'supplement')
    .sort((file1, file2) => file1.label - file2.label)

  if (suppFiles && suppFiles.length > 0) {
    xml += `  <sec sec-type="supplementary-material" id="SM">
    <title>Supplementary Material</title>
    ${suppFiles
      .map((file, i) => {
        const mimetypes = file.mimeType.split('/')
        return `<supplementary-material id="SD${i +
          1}" content-type="local-data">
      <label>${file.label ? file.label : ''}</label>
      <media xlink:href="${file.filename}" mimetype="${
          mimetypes[0]
        }" mime-subtype="${mimetypes[1]}"/>
    </supplementary-material>`
      })
      .join('\n')}
  </sec>`
  }
  xml += '</nihms-source-xml>'

  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/${manuscript.id}.xml`, xml, err => {
      if (err) reject(err)
      resolve(`${manuscript.id}.xml`)
    })
  })
}

// file types included for taggers
function filterFiles(files) {
  const allowedTypes = [
    'manuscript',
    'supplement',
    'figure',
    'table',
    'PMC',
    'IMGview',
    'IMGprint',
  ]
  const result = files.filter(file => allowedTypes.includes(file.type))
  return result
}

function writeFiles(tmpPath, manuscript) {
  const promises = [] // array of promises

  const filteredFiles = filterFiles(manuscript.files)

  // do not export the metadata file
  filteredFiles.forEach(file => {
    const promise = new Promise((resolve, reject) => {
      const fileOnDisk = fs.createWriteStream(`${tmpPath}/${file.filename}`)

      http
        .get(`${pubsweetServer}${file.url}`, response => {
          const stream = response.pipe(fileOnDisk)
          stream.on('finish', () => resolve(true))
        })
        .on('error', err => {
          // Handle errors
          reject(err)
        })
    })
    promises.push(promise)
  })
  return Promise.all(promises)
}

function createManifest(tmpPath, manuscript) {
  const order = []
  order.meta = 0
  order.manuscript = 1
  order.figure = 2
  order.table = 3
  order.supplement = 4
  order.PDF = 5

  manuscript.files.sort((file1, file2) => order[file1.type] - order[file2.type])

  let text = `meta\t\t${manuscript.id}.xml\n`
  manuscript.files
    .filter(file => file.type !== 'metadata')
    .forEach(file => {
      text += `${file.type}\t\t${file.label ? file.label : ''}\t\t${
        file.filename
      }\n`
    })
  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/manifest.txt`, text, err => {
      if (err) reject(err)
      resolve(true)
    })
  })
}

function compress(tmpPath, manuscript, datedFolder) {
  return new Promise((resolve, reject) => {
    const dest = `${ftpLocation}/${datedFolder}/${manuscript.id}.tar.gz`
    const cmd =
      `mkdir -p ${ftpLocation}/${datedFolder}; sleep 1 ;` +
      `find ${tmpPath} -printf "%P\n" -type f | tar --exclude='metadata.xml' --no-recursion -czf ${dest} -C ${tmpPath} -T -`
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        logger.error(err)
        reject(err)
      }
      logger.info(`Created package ${dest} to tagger FTP location.`)
      resolve(dest)
    })
  })
}

process
  .on('uncaughtException', err => {
    logger.error('Error', err.stack)
    // tidyUp()
  })
  .on('unhandledRejection', reason => {
    logger.error(`Unhandled Rejection at: ${reason.stack || reason}`)
  })

function tidyUp(tmpPath) {
  try {
    const cmd = `rm -rf ${tmpPath}`
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err)
        }
        // logger.info(`Finished process`)
        resolve(true)
      })
    })
  } catch (e) {
    logger.error('Error while tidying up after another error!')
  }
}

module.exports.createPackageForTaggers = createPackageForTaggers
