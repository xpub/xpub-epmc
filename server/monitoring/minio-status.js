const Prometheus = require('prom-client')
const superagent = require('superagent')
const logger = require('@pubsweet/logger')

const minioLiveStatus = new Prometheus.Gauge({
  name: 'minio_live_status',
  help:
    'Situation where Minio is running but may not behave optimally: 1 means OK, 0 means Error',
  labelNames: ['status'],
})

const minioReadyStatus = new Prometheus.Gauge({
  name: 'minio_ready_status',
  help:
    'Situation where the server is not ready to accept requests yet: 1 means OK, 0 means Error',
  labelNames: ['status'],
})

const checkMinioLive = async () => {
  const url = `http://${process.env.MINIO_ENDPOINT}:${
    process.env.MINIO_PORT
  }/minio/health/live`

  try {
    const response = await superagent.get(url)
    const status = response && response.status === 200 ? 1 : 0
    minioLiveStatus.set(status)
  } catch (e) {
    logger.error('Error checking Minio live: ', e)
    minioLiveStatus.set(0)
  }
}

const checkMinioReady = async () => {
  const url = `http://${process.env.MINIO_ENDPOINT}:${
    process.env.MINIO_PORT
  }/minio/health/ready`

  try {
    const response = await superagent.get(url)
    const status = response && response.status === 200 ? 1 : 0
    minioReadyStatus.set(status)
  } catch (e) {
    logger.error('Error checking Minio ready: ', e)
    minioReadyStatus.set(0)
  }
}

const checkMinioStatus = async () => {
  await checkMinioLive()
  await checkMinioReady()
}

module.exports = {
  checkMinioStatus,
}
