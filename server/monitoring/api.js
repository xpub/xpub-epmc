const Prometheus = require('prom-client')
const { checkDBStatus, getDBStatus } = require('./postgres-status')
const { checkMinioStatus } = require('./minio-status')

module.exports = app => {
  Prometheus.collectDefaultMetrics()

  app.get('/metrics', async (req, res) => {
    await checkDBStatus()
    await checkMinioStatus()
    res.set('Content-Type', Prometheus.register.contentType)
    res.end(Prometheus.register.metrics())
  })

  app.get('/postgres/healthcheck', async (req, res) => {
    const status = await getDBStatus()
    if (status === 1) {
      res.status(200).send('OK')
    } else {
      res.status(500).send('Error')
    }
  })
}
