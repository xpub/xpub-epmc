const config = require('config')
const fs = require('fs')
const rp = require('request-promise')
const tar = require('../utils/unTar.js')
const logger = require('@pubsweet/logger')
const mime = require('mime-types')
const uuidv4 = require('uuid/v4')
const path = require('path')
const files = require('../utils/files.js')
const db = require('../utils/db.js')

const pubsweetServer = config.get('pubsweet-server.internalBaseUrl')
const label = 'xsweet conversion'

/* xsweetConvert(
  '/home/yogmatee/projects/xmlValidation_files/xweet_files/tiana.docx',
  'EMS90000',
  '3394122e-9caa-4635-938f-a279a6a4c152',
) */

module.exports.xsweetConvert = async function xsweetConvert(
  fileUrl,
  manuscriptId,
  userId,
) {
  const options = {
    method: 'POST',
    uri: `${pubsweetServer}/convertDocxToHTML`,
    formData: {
      docx: fs.createReadStream(fileUrl),
    },
    headers: {
      /* 'content-type': 'multipart/form-data' */
      // Is set automatically
    },
  }

  await rp(options)
    .then(async body => {
      try {
        const tmpPath = await tar.createTempDir()
        fs.writeFileSync(`${tmpPath}/${manuscriptId}.html`, body)
        const fileInfo = getFileInfo(
          `${tmpPath}/${manuscriptId}.html`,
          manuscriptId,
          userId,
          label,
        )
        const uuid = uuidv4()

        // upload to minio
        files.uploadFileToMinio(
          `${uuid}${fileInfo.extension}`,
          fileInfo.filename,
          fileInfo.url,
          fileInfo.mimeType,
        )

        // update object details for database upsert
        fileInfo.url = `/download/${uuid}${fileInfo.extension}`
        delete fileInfo.extension

        await db.upsertHtmlPrevFile(fileInfo, manuscriptId)
        logger.info('HTML converted file has been uploaded to Minio and to db')
      } catch (err) {
        throw err
      }
    })
    .catch(err => {
      logger.error('Conversion failed:', err)
    })
}

function getFileInfo(filepath, manuscriptId, userId, label) {
  const filename = filepath.substring(filepath.lastIndexOf('/') + 1)
  const fileExt = path.extname(filepath)
  const fileSize = fs.statSync(filepath).size

  const fileInfo = {
    url: filepath,
    filename,
    type: 'source',
    label,
    size: fileSize,
    extension: fileExt,
    mimeType: `${mime.contentType(fileExt)}`,
    manuscriptId,
    updatedBy: userId,
  }
  return fileInfo
}
