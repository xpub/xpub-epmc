const https = require('https')

const PrivacyNotice = require('../xpub-model/entities/privacyNotice/data-access')

const privacyNoticeCheck = async () => {
  PrivacyNotice.selectLastVersion().then(privacyNotice => {
    getLatestVersion().then(data => {
      const privacyNotesData = data.nodes[0].node
      if (privacyNotesData['version Count'] !== privacyNotice.version) {
        const latestPrivacyNotice = {
          version: privacyNotesData['version Count'],
          effectiveDate: new Date(
            privacyNotesData.changed.split('-')[0].trim(),
          ),
        }
        PrivacyNotice.insert(latestPrivacyNotice).then(() => {
          PrivacyNotice.knex().destroy()
          process.exit()
        })
      } else {
        PrivacyNotice.knex().destroy()
        process.exit()
      }
    })
  })
}

function getLatestVersion() {
  return new Promise((resolve, reject) => {
    https
      .get(
        'https://www.ebi.ac.uk/data-protection/privacy-notice/json/5d182831-5e99-4072-816b-380cda5f58df',
        res => {
          res.setEncoding('utf8')
          let data = ''
          res.on('data', chunk => {
            data += chunk
          })
          res.on('end', () => {
            resolve(JSON.parse(data))
            res = null
          })
        },
      )
      .on('error', err => {
        // Handle errors
        reject(err)
      })
  })
}

;(() => {
  privacyNoticeCheck()
})()

module.export = privacyNoticeCheck
