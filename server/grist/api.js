const superagent = require('superagent')
const config = require('config')

module.exports = app => {
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })

  app.get('/grist/rest/api/search', authBearer, (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { query, page } = req.query
    const url = `https://www.ebi.ac.uk/europepmc/GristAPI/rest/get/secure/query=${query}&page=${page}&showExtraInformation=true&format=json`

    superagent
      .get(url)
      .auth(config.get('grist-api').username, config.get('grist-api').password)
      .then(response => {
        res.send(JSON.stringify(response.body))
      })
      .catch(err => {
        res.send(JSON.stringify({ error: { status: err.status } }))
      })
  })
}
