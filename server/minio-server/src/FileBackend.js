const config = require('config')
const expressMinio = require('@europepmc/express-middleware-minio')
const rfr = require('rfr')

const FileModel = rfr('server/xpub-model/entities/file/data-access')

const minioMiddleware = expressMinio.middleware()
const download_path = config.file.url.download

module.exports = app => {
  app.post(
    '/api/upload',
    minioMiddleware({ op: expressMinio.Ops.post }),
    (req, res) => {
      if (req.minio.error) {
        res.status(400).json({ error: req.minio.error })
      } else {
        res.send(`${download_path}/${req.minio.post.filename}`)
      }
    },
  )

  app.get(
    '/api/files',
    minioMiddleware({ op: expressMinio.Ops.list }),
    (req, res) => {
      if (req.minio.error) {
        res.status(400).json({ error: req.minio.error })
      } else {
        res.send(req.minio.list)
      }
    },
  )

  app.get(
    `/api/files/:filename`,
    minioMiddleware({ op: expressMinio.Ops.getStream }),
    async (req, res) => {
      if (req.minio.error) {
        res.status(400).json({ error: req.minio.error })
        return
      }

      req.minio.get.contentLength &&
        res.set('Content-Length', req.minio.get.contentLength)

      const { mimeType } = await FileModel.selectByUrl(req.params.filename)
      mimeType && res.set('Content-Type', mimeType)

      req.minio.get.stream.pipe(res)
    },
  )

  app.delete(
    '/api/files/:filename',
    minioMiddleware({ op: expressMinio.Ops.delete }),
    (req, res) => {
      if (req.minio.error) {
        res.status(400).json({ error: req.minio.error })
      } else {
        res.send(req.minio.delete)
      }
    },
  )

  /* todo: make sure there is always a public method like this for NCBI to download the necessary
   * files for the pdf conversion stage. */
  app.get(
    `${download_path}/:filename`,
    minioMiddleware({ op: expressMinio.Ops.getStream }),
    async (req, res) => {
      if (req.minio.error) {
        res.status(400).json({ error: req.minio.error })
        return
      }

      res.attachment(req.minio.get.originalName)
      res.set('Content-Length', req.minio.get.contentLength)

      let mimeType
      if (req.minio.get.contentType) {
        mimeType = req.minio.get.contentType
      } else {
        mimeType = await FileModel.selectByUrl(req.params.filename)
      }
      mimeType && res.set('Content-Type', mimeType)

      req.minio.get.stream.pipe(res)
    },
  )
}
