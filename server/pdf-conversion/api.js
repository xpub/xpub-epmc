const logger = require('@pubsweet/logger')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
require('dotenv').config()
const https = require('https')
const config = require('config')
const fetch = require('node-fetch')
// const ManuscriptManager = require('../xpub-model/entities/manuscript')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const getUser = require('../utils/user.js')
const manuscriptModel = require('../xpub-model/entities/manuscript')

const files = require('../utils/files.js')

const { baseUrl } = config.get('pubsweet-server')
const pdfTransformerApi = config.get('ncbiPdfTransformerApi')

;(async () => {
  const beforeUpdate = Date.now()
  const adminUser = await getUser.getAdminUser()
  await getDeposits(adminUser)
  teardownDBConnectionPool()
  logger.info(
    `PDF conversion process finished in ${Date.now() - beforeUpdate} ms`,
  )
})()

process
  .on('uncaughtException', err => {
    logger.error('Uncaught Exception thrown: ', err)
    teardownDBConnectionPool()
    process.exit(1)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection at Promise: ', promise)
    logger.error('Reason: ', reason)
    teardownDBConnectionPool()
    process.exit(1)
  })

// runMonitor()

/*
async function runMonitor() {
  /!* eslint-disable no-await-in-loop *!/
  while (true) {
    getDeposits()
    await sleep(pdfTransformerApi.freq)
  }
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}
*/

function teardownDBConnectionPool() {
  logger.info('PDF conversion: closing connection to the database...')
  Manuscript.knex().destroy()
}

async function getDeposits(adminUser) {
  logger.info('Polling our database for manuscripts to be converted to PDF...')

  await Manuscript.findByFieldEager(
    'pdf_deposit_state',
    'WAITING_FOR_PDF_CONVERSION',
    'files',
    // await Manuscript.findByFieldEager(
    //   'id',
    //   'EMS90062',
    //   'files',
  ).then(async resultSet => {
    /* eslint-disable no-await-in-loop */
    /* eslint-disable no-restricted-syntax */
    for (const manuscript of resultSet) {
      await deposit(manuscript, adminUser)
    }
  })

  logger.info('Polling NCBI for replies from the PDF conversion service...')
  const manuscript = new Manuscript()
  const pdfDepositStates = ['COMMITTED', 'WAITING_FOR_RESULT']

  await manuscript
    .findByFieldValuesIn('pdf_deposit_state', pdfDepositStates, 'files')
    .then(async resultSet => {
      /* eslint-disable no-await-in-loop */
      /* eslint-disable no-restricted-syntax */
      for (const manuscript of resultSet) {
        logger.info(`Check status of conversion: ${manuscript.id}`)
        await retryNcbiApiCall(adminUser, 1, manuscript, null)
      }
    })
}

function getFile(url, dest, cb) {
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(dest)
    https
      .get(url, response => {
        const stream = response.pipe(file)
        stream.on('finish', () => {
          resolve(true)
        })
        /*

      await response.pipe(file)
      await file.on('finish', async () => {
        await file.close(cb) // close() is async, call cb after close completes.
      })
*/
      })
      .on('error', err => {
        // Handle errors
        fs.unlink(dest) // Delete the file async. (But we don't check the result)
        reject(err)
      })
  })
}

async function uploadPDF(
  filename,
  manuscript,
  filePath,
  item_size,
  item,
  adminUser,
) {
  await files
    .uploadFileToMinio(
      filename,
      `${manuscript.id}.pdf`,
      filePath,
      'application/pdf',
    )
    .then(async () => {
      logger.info('PDF file uploaded to minio')
      // todo: need to delete this but minio still needs it
      // await fs.unlink(filePath, async () => {
      const pdf4print = 'pdf4print'

      // use existing ID if it already exists
      const existing = manuscript.files.findIndex(
        file => file.type === pdf4print && !file.deleted,
      )

      const pdfFile = {
        manuscriptId: manuscript.id,
        url: `/download/${filename}`,
        size: item_size,
        filename: item,
        type: pdf4print,
        label: 'PDF Conversion',
        mimeType: 'application/pdf',
        updatedBy: adminUser.id,
      }
      if (existing > -1) {
        pdfFile.id = manuscript.files[existing].id
        manuscript.files[existing] = pdfFile
      } else {
        manuscript.files.push(pdfFile)
      }
      manuscript.pdfDepositState = null
      logger.info('before line 175')
      await new Manuscript(manuscript).save()
      logger.info('after line 175')
    })
    .catch(async () => {
      await manuscriptModel.update(
        {
          id: manuscript.id,
          pdfDepositState: null,
          formState: 'PDF result storage failure',
        },
        adminUser.id,
      )
    })
}

async function processDeposit(manuscript, deposit, adminUser) {
  logger.info(`NCBI conversion status for : ${manuscript.id}: ${deposit.state}`)
  if (deposit.state === 'RESULT_IS_READY') {
    logger.info(`PDF conversion is ready: ${manuscript.id}. Fetching...`)
    const pdfItem = deposit.items.filter(item => item.result)[0]
    const { item, s3_get_url, item_size } = pdfItem

    const uuidv = await uuidv4()
    const filename = `${uuidv}.pdf`
    const filePath = `/tmp/${filename}`
    await getFile(s3_get_url, filePath)
      .then(async () => {
        await uploadPDF(
          filename,
          manuscript,
          filePath,
          item_size,
          item,
          adminUser,
        ).then(async () => {
          if (manuscript.status === 'tagging') {
            const newMan = {
              id: manuscript.id,
              status: 'xml-qa',
              formState: null,
            }
            if (
              manuscript['meta,fundingGroup'] &&
              manuscript['meta,fundingGroup'].some(g =>
                ['Swiss National Science Foundation', 'EMBL'].includes(
                  g.fundingSource,
                ),
              )
            ) {
              newMan.status = 'xml-triage'
              newMan.formState =
                'Manuscript has SNSF and/or EMBL funding. Ready for XML QA.'
            }
            await manuscriptModel.update(newMan, adminUser.id)
            logger.info(
              `PDF retrieved and saved successfully for manuscript ${
                manuscript.id
              }`,
            )
          }
        })
      })
      .catch(async error => {
        if (manuscript.status === 'tagging') {
          manuscript.status = 'xml-triage'
        }
        await manuscriptModel.update(
          {
            id: manuscript.id,
            pdfDepositState: null,
            formState: `PDF result not retrieved: ${error.toString()}`,
            status: manuscript.status,
          },
          adminUser.id,
        )
        logger.error(error)
      })
  } else if (deposit.state === 'NO_RESULT' || deposit.state === 'EXPIRED') {
    if (manuscript.status === 'tagging') {
      manuscript.status = 'xml-triage'
    }
    await manuscriptModel.update(
      {
        id: manuscript.id,
        pdfDepositState: null,
        formState: `PDF ${deposit.state}: ${deposit.details}`,
        status: manuscript.status,
      },
      adminUser.id,
    )
    logger.error(
      `Error getting result from PDF conversion service: ${deposit.details}`,
    )
  } else {
    logger.info(`Nothing found to be processed.`)
  }
}

/*
function deleteDeposit(deposit_id) {
  return ncbiApiCall('/deposits', 'DELETE', null, `/${deposit_id}`)
}
*/

function storeIds(manuscript, depositId, status, adminUser) {
  manuscript.pdfDepositId = depositId
  manuscript.pdfDepositState = status
  return manuscriptModel.update(
    {
      id: manuscript.id,
      pdfDepositState: status,
      pdfDepositId: depositId,
    },
    adminUser.id,
  )
}

const pause = duration => new Promise(resolve => setTimeout(resolve, duration))

function retryNcbiApiCall(
  adminUser,
  attempt,
  manuscript,
  depositObj,
  delay = 7500,
) {
  const retries = 3
  return new Promise(async (resolve, reject) => {
    try {
      if (manuscript.pdfDepositState === 'WAITING_FOR_PDF_CONVERSION') {
        const ncbiResponse = await ncbiApiCall('/deposits/', 'POST', depositObj)
        logger.info(ncbiResponse)
        const databaseResponse = await storeIds(
          manuscript,
          ncbiResponse.deposit_id,
          ncbiResponse.state,
          adminUser,
        )
        logger.info(
          `Deposit done. Stored depositId: ${ncbiResponse.deposit_id}`,
        )
        resolve(databaseResponse)
      } else {
        // pdfDepositStates is 'COMMITTED' or 'WAITING_FOR_RESULT'
        const ncbiResponse = await ncbiApiCall(
          '/deposits',
          'GET',
          depositObj,
          `/${manuscript.pdfDepositId}`,
        )
        const response = await processDeposit(
          manuscript,
          ncbiResponse,
          adminUser,
        )
        resolve(response)
      }
    } catch (err) {
      if (attempt <= retries) {
        logger.error(`retry attempt: ${attempt} - ${manuscript.id}`)
        manuscriptModel.update(
          { id: manuscript.id, formState: err, retryAttempt: attempt },
          adminUser.id,
        )
        attempt += 1
        await pause(delay)
        await retryNcbiApiCall(
          adminUser,
          attempt,
          manuscript,
          depositObj,
          delay * 2,
        )
      }
      reject(err)
      // update formState column with error message
      await manuscriptModel.update(
        {
          id: manuscript.id,
          formState: err,
        },
        adminUser.id,
      )
    }
  })
}

async function deposit(manuscript, adminUser) {
  const nxml =
    manuscript.files &&
    manuscript.files.find(file => !file.deleted && file.type === 'PMCfinal')
  const files =
    manuscript.files &&
    manuscript.files.filter(file => !file.deleted && file.type === 'IMGprint')
  const bigfiles = files.filter(file => file.size > 24000000)
  if (bigfiles.length > 0) {
    const err = `Unable to generate PDF. The following TIFF files are too large:\n${bigfiles
      .map(f => f.filename)
      .join('\n')}`
    logger.error(err)
    await manuscriptModel.update(
      {
        id: manuscript.id,
        formState: err,
        pdfDepositState: null,
        status:
          manuscript.status === 'tagging' ? 'xml-triage' : manuscript.status,
      },
      adminUser.id,
    )
  } else if (nxml) {
    logger.info(`Start depositing ${manuscript.id}`)
    files.push(nxml)
    const depositObj = {
      action: 'commit',
      depositor: 'ebi',
      domain: 'ukpmcpa',
    }
    depositObj.items = files.map(file => ({
      item: `${baseUrl}${file.url}`,
      item_size: file.size,
    }))
    // retry method(adminUser, attempt_number, manuscriptObj, depositObj)
    await retryNcbiApiCall(adminUser, 1, manuscript, depositObj)
  } else {
    logger.error(`No files to deposit for manuscript ${manuscript.id}`)
    await manuscriptModel.update(
      {
        id: manuscript.id,
        formState: 'No NXML file to deposit for PDF conversion',
      },
      adminUser.id,
    )
  }
}

function ncbiApiCall(uri, method, body, parameters) {
  let localParams = ''
  if (parameters) localParams = parameters
  const url = `${pdfTransformerApi.url}${uri}${localParams}`
  logger.info(`Calling ${url}`)
  return new Promise((resolve, reject) => {
    fetch(url, {
      method,
      body: body ? JSON.stringify(body) : null,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(data => resolve(data))
      .catch(err => reject(err))
  })
}
