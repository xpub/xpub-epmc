const config = require('config')
const Email = require('@pubsweet/component-send-email')
const lodash = require('lodash')
const fetch = require('node-fetch')
const {
  htmlEmailBase,
  setReviewerTemplate,
  newReviewerTemplate,
  newReviewFinalTemplate,
  setReviewFinalTemplate,
  checkReviewerTemplate,
  rejectSubmissionTemplate,
  removeDupeTemplate,
  alreadyCompleteTemplate,
  newPackageForTaggingTemplate,
  processedTaggingFilesTemplate,
  errorDevTemplate,
  bulkUploadTemplate,
  finalReviewTemplate,
  completedTemplate,
  resetPasswordTemplate,
  grantsAddedTemplate,
  incompleteTemplate,
  stalledTemplate,
  stalledErrorTemplate,
  deletedTemplate,
} = require('./templates')

const tagger = config.ftp_tagger.email
const { sender, url, testAddress, system, dev, listName } = config['epmc-email']

const parsed = string => lodash.unescape(string)

const sendMail = (to, subject, message, from = null, cc = null, bcc = null) => {
  const mailData = {
    from: from || sender,
    to: testAddress || to,
    subject: `[${listName}] ${subject}`,
    html: htmlEmailBase(message, url),
  }
  if (cc) {
    mailData.cc = testAddress || cc
  }
  if (bcc) {
    mailData.bcc = testAddress || bcc
  }
  Email.send(mailData)
}

const userMessage = (
  email,
  subject,
  message,
  from = null,
  cc = null,
  bcc = null,
) => {
  const regex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()[\]{};:'".,<>?«»“”‘’]))/i
  const linkAdded = message.replace(
    regex,
    '<a href="$&" style="color:#20699C">$&</a>',
  )
  const messageDiv = `<div style="white-space: pre-wrap;overflow-wrap: break-word;">${linkAdded}</div>`
  sendMail(email, subject, messageDiv, from, cc, bcc)
}

const taggerErrorEmail = (subject, message) => {
  const html = `<p>This manuscript has been returned to you. Please correct the following tagging errors.</p>${message}`
  sendMail(tagger, subject, html, null, null, system)
}

const taggerEmail = (email, manId, title, link) => {
  const html = newPackageForTaggingTemplate(manId, parsed(title), link)
  const subject = 'New Package Available for Tagging'
  sendMail(email, subject, html, null, null, system)
}

const processedTaggerEmail = (email, manId, title, packageName) => {
  const html = processedTaggingFilesTemplate(manId, parsed(title), packageName)
  const subject = 'Tagging files have been uploaded'
  sendMail(email, subject, html, null, null, system)
}

const errorDevEmail = err => {
  const html = errorDevTemplate(err)
  const subject = 'Something went wrong with the FTP utility functions'
  sendMail(dev, subject, html, null, null, system)
}

const bulkUploaderEmail = (email, message, filename = '') => {
  const html = bulkUploadTemplate(message)
  const subject = `Error while processing package ${filename}`
  sendMail(email, subject, html, null, null, system)
}

const resetPassword = (email, user, token) => {
  const link = `${url}password-reset/${token}`
  const html = resetPasswordTemplate(user, link)
  const subject = `Password reset requested`
  sendMail(email, subject, html, null, null, system)
}

const completedEmail = (email, manInfo) => {
  const link = `https://europepmc.org/articles/${manInfo.pmcid}`
  const html = completedTemplate(parsed(manInfo.title), link, manInfo.pmcid)
  const subject = `${manInfo.id}: Manuscript available in Europe PMC`
  sendMail(email, subject, html, null, null, system)
}

const deletedEmail = (email, manInfo) => {
  const html = deletedTemplate(parsed(manInfo.title))
  const subject = `${manInfo.id}: Submission cancelled`
  sendMail(email, subject, html, null, null, system)
}

const incompleteEmail = (manInfo, submitter) => {
  const { title, givenNames, surname } = submitter
  const { email } = submitter.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/${
    manInfo.status === 'INITIAL' ? 'create' : 'submit'
  }`
  const html = incompleteTemplate(salutation, parsed(manInfo.title), link)
  const subject = `${manInfo.id}: Your submission is incomplete`
  sendMail(email, subject, html, null, null, system)
}

const stalledEmail = (manInfo, reviewer) => {
  const { title, givenNames, surname, email, token } = reviewer
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = token
    ? `${url}dashboard?accept=${token}`
    : `${url}submission/${manInfo.id}/${
        manInfo.status === 'xml-review' ? 'review' : 'submit'
      }`
  const html = stalledTemplate(salutation, parsed(manInfo.title), link)
  const subject = `${manInfo.id}: Please review your submission`
  sendMail(email, subject, html, null, null, system)
}

const stalledErrorEmail = (manInfo, user) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/submit`
  const html = stalledErrorTemplate(salutation, parsed(manInfo.title), link)
  const subject = `${manInfo.id}: Your submission still needs corrections`
  sendMail(email, subject, html, null, null, system)
}

const grantEmail = (pi, grants, manInfo, reviewer) => {
  const html = grantsAddedTemplate(pi, grants, manInfo, reviewer)
  const subject = `Publication linked to your grants`
  sendMail(pi.email, subject, html, null, null, system)
}

const reviewerEmail = ({ reviewer, manInfo, submitter, token }) => {
  const { title, givenNames, surname } = submitter
  const submitterName = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const link = `${url}submission/${manInfo.id}/submit`
  const args = [salutation, parsed(manInfo.title), submitterName, link]
  let html = ''
  if (token === 'correction') {
    html = checkReviewerTemplate(...args)
  } else if (reviewer.id) {
    html = setReviewerTemplate(...args)
  } else {
    args[3] = `${url}dashboard?accept=${token}`
    html = newReviewerTemplate(...args)
  }
  const subject = `Approve submission of ${manInfo.id}`
  sendMail(reviewer.email, subject, html, null, null, system)
}

const finalReviewerEmail = ({ reviewer, manInfo, submitter = null, token }) => {
  const { title, givenNames, surname } = submitter || ''
  const submitterName =
    submitter && `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const finalReview = manInfo.status === 'xml-review'
  const link = `${url}submission/${manInfo.id}/review`
  const args = [
    salutation,
    parsed(manInfo.title),
    submitterName,
    link,
    finalReview,
  ]
  let html = ''
  if (reviewer.id) {
    html = setReviewFinalTemplate(...args)
  } else if (token) {
    args[3] = `${url}dashboard?accept=${token}`
    html = newReviewFinalTemplate(...args)
  }
  if (html) {
    const subject = finalReview
      ? `Please review manuscript ${manInfo.id}`
      : `You've been assigned to review ${manInfo.id}`
    sendMail(reviewer.email, subject, html, null, null, system)
  }
}

const submitterRejectEmail = ({ reviewer, manInfo, submitter, message }) => {
  const { title, givenNames, surname } = submitter
  const { email } = submitter.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer
  const reviewerName = `${t ? `${t} ` : ''}${g} ${s}`
  const link = `${url}submission/${manInfo.id}/submit`
  const html = rejectSubmissionTemplate(
    salutation,
    parsed(manInfo.title),
    reviewerName,
    message,
    link,
  )
  const subject = `${manInfo.id}: Submission rejected`
  sendMail(email, subject, html, null, null, system)
}

const removeDuplicateEmail = (user, badInfo, goodInfo) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const pmcid =
    goodInfo['meta,articleIds'] &&
    goodInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid')
      ? goodInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id
      : null
  const html = removeDupeTemplate(
    salutation,
    badInfo.id,
    parsed(badInfo['meta,title']),
    goodInfo.id,
    pmcid,
  )
  const subject = `${badInfo.id}: Duplicate submission`
  sendMail(email, subject, html, null, null, system)
}

const alreadyCompleteEmail = async (user, manInfo) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const pmcid =
    manInfo['meta,articleIds'] &&
    manInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid') &&
    manInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id
  const epmcURL = `https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=SRC:MED%20EXT_ID:${pmcid}&resulttype=lite&format=json`
  const response = await fetch(epmcURL)
  const json = await response.json()
  const epmc = json.resultList.result[0] && json.resultList.result[0].inEPMC
  const inEPMC = epmc === 'Y'
  const html = alreadyCompleteTemplate(
    salutation,
    manInfo.id,
    manInfo['meta,title'],
    pmcid,
    inEPMC,
  )
  const subject = `${manInfo.id}: Article already in Europe PMC`
  sendMail(email, subject, html, null, null, system)
}

const finalReviewEmail = (reviewer, manInfo) => {
  const { title, givenNames, surname } = reviewer
  const { email } = reviewer.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/review`
  const releaseDelay =
    manInfo.releaseDelay === '0'
      ? 'immediately'
      : `${manInfo.releaseDelay} month${
          manInfo.releaseDelay === '1' ? '' : 's'
        }`
  const html = finalReviewTemplate(
    salutation,
    parsed(manInfo.title),
    link,
    releaseDelay,
  )
  const subject = `${manInfo.id}: Ready for final review`
  sendMail(email, subject, html, null, null, system)
}

module.exports = {
  sendMail,
  userMessage,
  taggerEmail,
  processedTaggerEmail,
  errorDevEmail,
  taggerErrorEmail,
  bulkUploaderEmail,
  reviewerEmail,
  finalReviewerEmail,
  submitterRejectEmail,
  grantEmail,
  removeDuplicateEmail,
  alreadyCompleteEmail,
  finalReviewEmail,
  completedEmail,
  deletedEmail,
  resetPassword,
  incompleteEmail,
  stalledEmail,
  stalledErrorEmail,
}
