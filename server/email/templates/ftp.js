const newPackageForTaggingTemplate = (manId, title, link) => `
  <p>There is a new package available for tagging.<p>
  <p>ID: <b>${manId}</b>
  <p>Title: <b>${title}</b><p>
  <p>You can download it <a style="color:#20699C" href="${link}">here</a>.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC plus system</p>
`
const processedTaggingFilesTemplate = (manId, title, packageName) => `
  <p>The following package has been processed successfully.<p>
  <p>ID: <b>${manId}</b>
  <p>Title: <b>${title}</b><p>
  <p>Package: <b>${packageName}</b>
  <p>Kind regards,</p>
  <p>The Europe PMC plus system</p>
  `
const errorDevTemplate = err => `
  <p>Something went wrong with the FTP utility.<p>
  <p>Error: <b>${err}</b>
  <p>The Europe PMC plus system</p>
  `
const bulkUploadTemplate = message => `
  <p>There was an error while processing the package: <b>${message}</b><p>
  <p>Kind regards,</p>
  <p>The Europe PMC plus system</p>
`
module.exports = {
  newPackageForTaggingTemplate,
  bulkUploadTemplate,
  processedTaggingFilesTemplate,
  errorDevTemplate,
}
