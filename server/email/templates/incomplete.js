const incompleteTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;">Your submission is incomplete</h1>
  <p>Dear ${salutation},</p>
  <p>Your Europe PMC plus submission, <b>${title}</b>, has not been completed.</p>
  <p>Please go to <a style="color:#20699C" href="${link}">${link}</a> to complete and submit your manuscript submission.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const stalledTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;">Please review your submission</h1>
  <p>Dear ${salutation},</p>
  <p>Your Europe PMC plus submission, <b>${title}</b>, is still waiting for your review. This is a necessary step for processing the submission.</p>
  <p>Please <a style="color:#20699C" href="${link}">click the link to review this submission</a>.</p>
  <p>If you do not think you are the correct person to review this submission, and you would like to reject the assignment, we would greatly appreciate it if you would reply to this email and let us know.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const stalledErrorTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;">Please correct and continue your submission</h1>
  <p>Dear ${salutation},</p>
  <p>Errors were reported in your Europe PMC plus submission, <b>${title}</b>. Your submission cannot be processed until these errors are corrected.</p>
  <p>Please go to <a style="color:#20699C" href="${link}">${link}</a> to correct the errors and submit your manuscript again.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = { incompleteTemplate, stalledTemplate, stalledErrorTemplate }
