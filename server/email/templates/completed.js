const completedTemplate = (title, link, pmcid) => `
  <h1 style="font-weight:600;">Manuscript available in Europe PMC</h1>
  <p>Your completed Europe PMC plus submission, <b>${title}</b>, is now freely available in Europe PMC:</p>
  <p><a style="color:#20699C" href="${link}">${link}</a></p>
  <p>You may use ${pmcid} for grant reporting purposes.</p>
  <p>Please let us know if you have any questions.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`
module.exports = { completedTemplate }
