const grantsAddedTemplate = (pi, grants, manInfo, reviewer) => `
  <p>Dear ${pi.title ? `${pi.title} ` : ''}${
  pi.givenNames ? `${pi.givenNames} ` : ''
}${pi.surname},</p>
  <p>This message is to inform you that your grant${
    grants.length > 1 ? 's' : ''
  }, listed below, ha${
  grants.length > 1 ? 've' : 's'
} been linked to a paper deposited with Europe PMC plus:<p>
  <ul>${grants
    .map(g => `<li>${g.fundingSource} ${g.awardId}, <em>${g.title}</em></li>`)
    .join('')}</ul>
  <p>The linked paper is <em>${manInfo.title}</em> (${
  manInfo.id
}), under the review of ${reviewer.givenNames} ${reviewer.surname}.</p>
  <p>If you are satisfied that this link has been made correctly, no further action is required. The paper will be uploaded to Europe PMC ${
    manInfo.releaseDelay === '0'
      ? 'immediately'
      : `${manInfo.releaseDelay} month${
          manInfo.releaseDelay === '1' ? '' : 's'
        }`
  } after publication.</p>
  <p>If you have any questions, or think this link has been made in error, please respond to this message for assistance.</p>
  <p>Sincerely,</p>
  <p>The Europe PMC Helpdesk</p>
`
module.exports = { grantsAddedTemplate }
