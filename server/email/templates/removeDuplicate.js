const removeDupeTemplate = (salutation, badId, title, goodId, pmcid) => `
  <h1 style="font-weight:600;">Duplicate submission</h1>
  <p>Dear ${salutation},</p>
  <p>Your manuscript submission, <b>${badId}: ${title}</b> has been removed as a duplicate of ${goodId}. Any grants attached to your submission have been added to the existing record.</p>
  <p>Please use ${pmcid || goodId} for grant reporting purposes${
  pmcid ? '' : ' until a PMCID becomes available'
}.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const alreadyCompleteTemplate = (salutation, manId, title, pmcid, inEPMC) => `
<p>Dear ${salutation},</p>
<p>The full text of your manuscript submission, <b>${manId}: ${title}</b> has already been provided to Europe PMC. Your submission has been removed as a duplicate of ${pmcid}.</p>
${
  inEPMC
    ? `<p><a style="color:#20699C" href="https://europepmc.org/articles/${pmcid}">View this article on Europe PMC</a></p>`
    : ''
}
<p>Please use ${pmcid} for grant reporting purposes. Any grants attached to your submission will be linked to the existing record.</p>
<p>Kind regards,</p>
<p>The Europe PMC Helpdesk</p>
`

module.exports = {
  alreadyCompleteTemplate,
  removeDupeTemplate,
}
