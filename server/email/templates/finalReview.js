const finalReviewTemplate = (salutation, title, link, releaseDelay) => `
  <h1 style="font-weight:600;">Review web version</h1>
  <p>Dear ${salutation},</p>
  <p>The web version of your Europe PMC plus submission, <b>${title}</b>, is now ready for your review. This is a necessary step to complete your submission to Europe PMC.</p>
  <p>Please proceed to <a style="color:#20699C" href="${link}">${link}</a> to approve or request corrections to the web version of your manuscript.</p>
  <p>Please note that only errors or omissions that impact the scientific accuracy of your article are eligible for correction.</p>
  <p>Once approved, your manuscript will be made available in Europe PMC ${releaseDelay} after the final publication of the article. You will be notified when your article is freely available in Europe PMC.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = { finalReviewTemplate }
