const fs = require('fs')
const logger = require('@pubsweet/logger')
const readline = require('readline')
const path = require('path')
const mime = require('mime-types')
const download = require('download')
const fetch = require('node-fetch')
const dateFormat = require('dateformat')
const rimraf = require('rimraf')

const { minioClient } = require('@europepmc/express-middleware-minio')

module.exports.getManifestFilename = function getManifestFilename(tmpPath) {
  return new Promise((resolve, reject) => {
    fs.readdir(tmpPath, (err, items) => {
      if (err) reject(err)
      let manifestExists = false
      for (let i = 0; i < items.length; i += 1) {
        if (!items[i].startsWith('.', 0) && items[i].indexOf('manifest') > -1) {
          manifestExists = true
          resolve(items[i])
        }
      }
      if (!manifestExists) reject(new Error('There is no manifest file.'))
    })
  })
}

module.exports.getManifestFileData = function getManifestFileData(
  tempFolder,
  manifestFileName,
) {
  return new Promise((resolve, reject) => {
    const files = []
    const manuscriptId = manifestFileName.match(/\d+/g)[0]

    const lineReader = readline.createInterface({
      input: fs.createReadStream(`${tempFolder}/${manifestFileName}`, {
        encoding: 'UTF-8',
      }),
    })
    lineReader
      .on('line', line => {
        if (line) {
          const row = line.split(/\t{1,}/).filter(x => x) // return only non empty tokens
          const fileName = row[row.length - 1]

          if (!fileName || !fs.existsSync(`${tempFolder}/${fileName}`)) {
            reject(new Error(`File ${fileName} does not exist`))
          } else {
            files.push({
              fileURI: `${tempFolder}/${fileName}`,
              filename: fileName,
              type: row[0],
              label: row[1],
              manuscriptId: `EMS${manuscriptId}`,
            })
          }
        }
      })
      .on('close', () => {
        resolve([`EMS${manuscriptId}`, files])
      })
  })
}

module.exports.checkFiles = function checkFiles(files, tmpPath, userId) {
  return new Promise((resolve, reject) => {
    const filesArr = []
    for (let i = 0; i < files.length; i += 1) {
      fs.access(files[i].fileURI, fs.F_OK, err => {
        if (err) {
          reject(new Error(`${files[i]} does not exist.`))
        }
      })
      const fileInfo = files[i]
      const {
        fileURI: file,
        filename,
        type: fileType,
        label: fileLabel,
        manuscriptId,
      } = fileInfo
      const stats = fs.statSync(file)
      const fileSizeInBytes = stats.size
      const extension = path.extname(file)
      const mimeType = mime.contentType(extension)

      filesArr.push({
        manuscriptId,
        filename,
        mimeType,
        extension,
        type: fileType,
        size: fileSizeInBytes,
        url: file,
        label: fileLabel,
        updatedBy: userId,
      })
    }
    resolve(filesArr)
  })
}

module.exports.uploadFileToMinio = uploadFileToMinio

module.exports.renameFile = function renameFile(filepath) {
  return new Promise((resolve, reject) => {
    const datedFolder = dateFormat(new Date(), 'yyyy-mm-dd')
    const dir_path = path.dirname(filepath)
    const filename = path.basename(filepath)

    if (!fs.existsSync(`${dir_path}/${datedFolder}`)) {
      fs.mkdirSync(`${dir_path}/${datedFolder}`)
    }

    const oldPath = filepath
    const newPath = `${dir_path}/${datedFolder}/${filename}_${Date.now()}`

    fs.rename(oldPath, newPath, err => {
      if (err) reject(err)
    })
    logger.info(`File has been moved to ${dir_path}/${datedFolder}`)
    resolve(filepath)
  })
}

module.exports.moveErroneousFile = function moveErroneousFile(
  filepath,
  errorFolder,
) {
  return new Promise((resolve, reject) => {
    const filename = path.basename(filepath)
    const oldPath = filepath
    const newPath = `${errorFolder}/${filename}_${Date.now()}`

    fs.rename(oldPath, newPath, err => {
      if (err) reject(err)
    })
    resolve(filepath)
  })
}

module.exports.downloadFile = function downloadFile(url, tmpPath) {
  return new Promise((resolve, reject) => {
    download(url, tmpPath)
      .then(() => {
        resolve(`${tmpPath}/${fs.readdirSync(tmpPath)[0]}`)
      })
      .catch(err => {
        reject(err)
      })
  })
}

module.exports.getFilename = function getFilename(url) {
  return new Promise((resolve, reject) => {
    if (!url) reject(new Error('Something is wrong with the url'))
    const filename = url.substring(url.lastIndexOf('/') + 1)
    resolve(filename)
  })
}

module.exports.readData = function readData(url) {
  return new Promise((resolve, reject) => {
    fs.readFile(url, 'utf8', (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })
}

module.exports.tidyUp = function tidyUp(tmpPath) {
  return new Promise((resolve, reject) => {
    rimraf(tmpPath, err => {
      if (err) reject(err)
      logger.info(`successfully deleted ${tmpPath}`)
      resolve(true)
    })
  })
}

module.exports.fetchFile = function fetchFile(fileUrl) {
  return new Promise((resolve, reject) => {
    fetch(fileUrl, {
      method: 'GET',
      headers: {
        Accept: 'application/xml',
      },
    })
      .then(data => {
        resolve(data.text())
      })
      .catch(err => {
        reject(err)
      })
  })
}

module.exports.normalizeFilename = function normalizeFilename({
  filename,
  type,
  label,
  manuscriptId,
}) {
  const ext = filename.split('.').pop()
  const normedLabel = label ? `-${label.replace(/[^\w]/g, '_')}` : ''
  return `${manuscriptId}-${type}${normedLabel}.${ext}`
}

function uploadFileToMinio(filename, originalFileName, filePath, mimeType) {
  return new Promise((resolve, reject) => {
    minioClient.uploadFile(
      filename,
      originalFileName,
      mimeType,
      filePath,
      (error, etag) => {
        if (error) {
          fs.unlinkSync(filePath)
          logger.error(error)
          reject(error)
        } else {
          logger.debug(`${originalFileName} file uploaded to minio`)
          fs.unlinkSync(filePath)
          resolve(filePath)
        }
      },
    )
  })
}
