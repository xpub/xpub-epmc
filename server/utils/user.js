const User = require('../xpub-model/entities/user/data-access')

module.exports.getTaggerUser = function getTaggerUser() {
  return new Promise((resolve, reject) => {
    User.findByRole('tagger')
      .then(user => {
        resolve(user)
      })
      .catch(error => reject(error))
  })
}

module.exports.getAdminUser = function getAdminUser() {
  return new Promise((resolve, reject) => {
    User.findByEmail('helpdesk@europepmc.org')
      .then(user => {
        resolve(user)
      })
      .catch(error => reject(error))
  })
}
