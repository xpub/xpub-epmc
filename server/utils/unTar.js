const tar = require('tar')
const tmp = require('tmp')
const fs = require('fs')
const path = require('path')

function walkDir(dir, callback) {
  fs.readdirSync(dir).forEach(f => {
    if (!f.startsWith('.', 0)) {
      const dirPath = path.join(dir, f)
      const isDirectory = fs.statSync(dirPath).isDirectory()
      isDirectory ? walkDir(dirPath, callback) : callback(path.join(dir, f))
    }
  })
}

module.exports.untar = function untar(source, dest) {
  return new Promise((resolve, reject) => {
    tar.x(
      {
        file: source,
        cwd: dest,
      },
      err => {
        if (err) {
          reject(err)
        }
        fs.readdir(dest, err => {
          if (err) reject(err)

          const trail = []
          walkDir(dest, filePath => {
            if (filePath.toLowerCase().includes('manifest')) {
              const dirPath = filePath.substring(0, filePath.lastIndexOf('/'))
              trail.push(dirPath)
            }
          })

          if (trail.length === 0) {
            reject(new Error('No manifest file found.'))
          } else {
            resolve(trail[0])
          }
        })
      },
    )
  })
}

module.exports.createTempDir = function createTempDir() {
  return new Promise((resolve, reject) => {
    tmp.dir({ mode: '0750', prefix: 'xpubTmpDir_' }, (err, tmpPath) => {
      if (err) reject(err)
      resolve(tmpPath)
    })
  })
}

module.exports.createTempDirSync = function createTempDirSync() {
  const tmpobj = tmp.dirSync({ mode: '0750', prefix: 'xpubTmpDir_' })
  return tmpobj.name
}
