require('dotenv').config()

const logger = require('@pubsweet/logger')
const { minioClient } = require('@europepmc/express-middleware-minio')
const AWS = require('aws-sdk')
const { PassThrough } = require('stream')

const s3 = new AWS.S3({
  region: 'eu-west-2',
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
})

minioClient.listFiles((error, list) => {
  if (error) {
    logger.info('error')
  } else {
    let counter = 0
    // eslint-disable-next-line no-restricted-syntax
    for (const file of list) {
      logger.info(file)
      uploadFile(file.name.replace('uploads/', ''))
      counter += 1
      if (counter > 10) {
        break
      }
    }
  }
})

function uploadFile(filePath) {
  minioClient.getFileStream(filePath, (err, inputStream) => {
    if (err) throw err
    inputStream.pipe(uploadFromStream(s3, filePath))
  })
}
// arn:aws:s3:::xpub-epmc-test

function uploadFromStream(s3, filePath) {
  const pass = new PassThrough()

  const params = {
    Bucket: process.env.AWS_BUCKET_NAME, // pass your bucket name
    Key: `uploads/${filePath}`, // file will be saved as
    Body: pass,
  }
  s3.upload(params, (s3Err, data) => {
    if (s3Err) throw s3Err
    logger.info(`File uploaded successfully at ${data.Location}`)
  })
  return pass
}
