exports.up = (knex, Promise) =>
  knex.schema.alterTable('file', t => {
    t.string('label', 200).alter()
  })

exports.down = (knex, Promise) =>
  knex.schema.alterTable('file', t => {
    t.string('label').alter()
  })
