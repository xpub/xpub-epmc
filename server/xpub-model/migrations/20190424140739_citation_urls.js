exports.up = (knex, Promise) =>
  knex.schema.table('manuscript', t => {
    t.string('meta,citeref_url')
    t.string('meta,fulltext_url')
  })

exports.down = (knex, Promise) =>
  knex.schema.table('manuscript', t => {
    t.dropColumn('meta,citeref_url')
    t.dropColumn('meta,fulltext_url')
  })
