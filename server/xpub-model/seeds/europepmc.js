const logger = require('@pubsweet/logger')
const Role = require('../entities/role/data-access')

exports.seed = async (knex, Promise) => {
  // Checking
  const role = await Role.selectByName('admin')
  if (role) {
    logger.info('Good. Already seeded.')
    return
  }

  // Seed organization
  await knex('organization').del()
  await knex('organization').insert([
    {
      name: 'Europe PMC Plus',
    },
  ])

  // Seed roles
  await knex('role').insert([
    { name: 'admin', organization: true },
    { name: 'external-admin', organization: true },
    { name: 'submitter', organization: false },
    { name: 'reviewer', organization: false },
    { name: 'tagger', organization: true },
  ])

  // Seed versions
  await knex('privacy_notice').insert([
    { version: 2, effective_date: new Date('2018-05-24') },
    { version: 3, effective_date: new Date('2018-08-23') },
  ])
}
