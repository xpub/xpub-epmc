const Team = require('./data-access')
const Note = require('../note/data-access')
const Manuscript = require('../manuscript')
const logger = require('@pubsweet/logger')

const TeamManager = {
  find: Team.selectById,
  findByUserIdOrManuscriptId: Team.selectByUserIdOrManuscriptId,
  selectByUserId: async id => Team.selectByUserId(id),
  selectByManuscriptId: async id => Team.selectByManuscriptId(id),
  addNewReviewer: async (noteId, userId) => {
    try {
      const note = await Note.selectById(noteId)
      if (note) {
        const { manuscriptId } = note
        await Team.insert(
          {
            manuscriptId,
            userId,
            roleName: 'reviewer',
          },
          userId,
        )
        await Note.delete(noteId, userId)
        const teams = await Team.selectByManuscriptId(manuscriptId)
        if (
          teams.some(t => t.roleName === 'submitter' && t.userId === userId)
        ) {
          const man = await Manuscript.findById(manuscriptId, userId)
          if (man.status === 'in-review') {
            await Manuscript.update(
              { id: manuscriptId, status: 'submitted' },
              userId,
            )
            return {
              success: false,
              message: `You have already checked and submitted ${manuscriptId}.`,
            }
          }
        }
        return {
          success: true,
          message: `Submission ${manuscriptId} accepted for review.`,
        }
      }
    } catch (error) {
      logger.error('Set reviewer error: ', error)
      return {
        success: false,
        message: 'Submission has already been taken for review.',
      }
    }
  },
  delete: Team.delete,
  save: async (team, userId) => {
    let { id } = team
    if (team.id) {
      const updated = await Team.update(team, userId)
      if (!updated) {
        throw new Error('team not found')
      }
    } else {
      id = await Team.insert(team, userId)
    }

    return { ...team, id }
  },
  modelName: 'EpmcTeam',

  model: Team,
}

module.exports = TeamManager
