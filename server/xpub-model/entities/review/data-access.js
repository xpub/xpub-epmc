const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')

const columnNames = [
  'recommendation',
  'comments',
  'open',
  'user_id',
  'manuscript_id',
]

class Review extends EpmcBaseModel {
  static get tableName() {
    return 'review'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        userId: { type: 'uuid' },
        manuscriptId: { type: 'string' },
        open: { type: 'boolean' },
        comments: { type: 'string' },
        recommendation: { type: 'string' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/data-access')
    const Manuscript = require('../manuscript/data-access')
    const Annotation = require('../annotation/data-access')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'review.userId',
          to: 'users.id',
        },
      },
      manuscript: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'review.manuscriptId',
          to: 'manuscript.id',
        },
      },
      annotations: {
        relation: Model.HasManyRelation,
        modelClass: Annotation,
        join: {
          from: 'review.id',
          to: 'annotation.reviewId',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await Review.query()
      .where('id', id)
      .whereNull('deleted')
    if (!rows.length) {
      throw new Error('Review not found')
    }
    return rowToEntity(rows[0])
  }

  static async getCurrentByManuscriptId(manId) {
    const row = await Review.query()
      .where('review.manuscript_id', manId)
      .whereNull('review.deleted')
      .eager('[user, annotations.file]')
      .modifyEager('annotations', builder => {
        builder.whereNull('deleted')
      })
      .first()
    return rowToEntity(row)
  }

  static async getDeletedByManuscriptId(manId) {
    const rows = await Review.query()
      .where('review.manuscript_id', manId)
      .whereNotNull('review.deleted')
      .eager('[user, annotations.file]')
      .modifyEager('annotations', builder => {
        builder.whereNull('deleted')
      })
    return rows.map(rowToEntity)
  }

  static async selectAll() {
    const rows = await Review.query()
      .from('review')
      .whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async insert(review, userId) {
    const row = entityToRow(review, columnNames)
    row.id = uuid.v4()
    row.updated_by = userId
    const query = buildQuery.insert(row).into('review')
    await runQuery(query)
    return row.id
  }

  static update(review, userId) {
    const row = entityToRow(review, columnNames)
    row.updated_by = userId
    const query = buildQuery
      .update(row)
      .table('review')
      .where('id', review.id)
      .whereNull('deleted')
    return runQuery(query)
  }

  static delete(id, userId) {
    return Review.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }

  static async deleteByManuscriptId(id, userId, trx) {
    await Review.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_id', id)
      .whereNull('deleted')
  }
}
module.exports = Review
