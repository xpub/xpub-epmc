const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')

const columnNames = ['version', 'effectiveDate']

class PrivacyNotice extends EpmcBaseModel {
  static get tableName() {
    return 'privacyNotice'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        effectiveDate: { type: 'timestamp' },
        version: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {}
  }

  static async selectById(id) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('privacyNotice')
        .where({ id }),
    )
    if (!rows.length) {
      throw new Error('Privacy Notice not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectAll() {
    return PrivacyNotice.query()
  }

  static async selectLastVersion() {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('privacyNotice')
        .orderBy('version', 'desc')
        .limit(1),
    )
    if (!rows.length) {
      throw new Error('Privacy Notice not found')
    }
    return rowToEntity(rows[0])
  }
  static async insert(privacyNotice) {
    const row = entityToRow(privacyNotice, columnNames)
    row.id = uuid.v4()
    const query = buildQuery.insert(row).into('privacyNotice')
    await runQuery(query)
    return row.id
  }

  static update(privacyNotice) {
    const row = entityToRow(privacyNotice, columnNames)
    const query = buildQuery
      .update(row)
      .table('privacyNotice')
      .where('id', privacyNotice.id)
    return runQuery(query)
  }

  static delete(id) {
    return runQuery(
      buildQuery
        .delete()
        .from('privacyNotice')
        .where({ id }),
    )
  }
}
module.exports = PrivacyNotice
