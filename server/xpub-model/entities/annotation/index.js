const lodash = require('lodash')
const Annotation = require('./data-access')
const ReviewAccess = require('../review/data-access')
const FileAccess = require('../file/data-access')
const { rowToEntity } = require('../util')

const AnnotationManager = {
  find: Annotation.selectById,
  findByReviewId: Annotation.selectByReviewId,
  findByFileId: Annotation.selectByFileId,
  delete: Annotation.delete,
  deleteByReviewId: Annotation.deleteByReviewId,
  save: async input => {
    const annotation = rowToEntity(input)
    const { fileId, userId } = annotation
    let { reviewId } = annotation
    let annId = annotation.id
    if (!reviewId) {
      try {
        const file = await FileAccess.selectById(fileId)
        const review = await ReviewAccess.getCurrentByManuscriptId(
          file.manuscriptId,
        )
        reviewId = review ? review.id : null
        if (!reviewId) {
          const review = new ReviewAccess({
            manuscriptId: file.manuscriptId,
            userId,
            updatedBy: userId,
          })
          const savedReview = await review.save()
          annotation.reviewId = savedReview.id
        } else {
          annotation.reviewId = reviewId
        }
      } catch (error) {
        throw new Error(error)
      }
    }
    try {
      if (annId) {
        const toUpdate = await Annotation.selectById(annotation.id)
        if (!toUpdate) {
          throw new Error('Annotation not found')
        }
        lodash.assign(toUpdate, annotation)
        await toUpdate.save()
      } else {
        const ann = new Annotation(annotation)
        const saved = await ann.save()
        annId = saved.id
      }
    } catch (error) {
      throw new Error(error)
    }
    return { ...annotation, id: annId }
  },
  modelName: 'Annotation',

  model: Annotation,
}

module.exports = AnnotationManager
