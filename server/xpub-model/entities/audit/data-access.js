const { Model } = require('objection')
const EpmcBaseModel = require('../epmc-base-model')

const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

class Audit extends EpmcBaseModel {
  static get tableName() {
    return 'audit.audit_log'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        userId: { type: 'uuid' },
        action: { type: 'string' },
        objectId: { type: 'uuid' },
        originalData: { type: 'object' },
        changes: { type: 'object' },
        objectType: { type: 'string' },
        manuscriptId: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/data-access')
    const Manuscript = require('../manuscript/data-access')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'audit.audit_log.userId',
          to: 'users.id',
        },
      },
      manuscript: {
        relation: Model.HasOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'audit.audit_log.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  static async getMetrics() {
    // display_mth, submitted, xml_review, xml_review_within_10_days, xml_review_within_10_days_perc,
    // xml_review_within_3_days, xml_review_within_3_days_perc, published, ncbi_ready_median, external_qa
    const metrics = await knex.raw(
      `SELECT display_mth, submitted, xml_review, xml_review_within_10_days,
       CASE WHEN xml_review=0 THEN 0 ELSE xml_review_within_10_days*100/xml_review END xml_review_within_10_days_perc,
       xml_review_within_3_days,
       CASE WHEN xml_review=0 THEN 0 ELSE xml_review_within_3_days*100/xml_review END xml_review_within_3_days_perc,
       published, ncbi_ready_median, COALESCE (external_qa, 0) external_qa, xml_tagging
FROM
(SELECT TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 month', 'YYYYMM') mth,
        TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 month', 'Mon YYYY') display_mth,
       SUM(CASE WHEN TO_CHAR(d.submitted_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) submitted,
       SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) xml_review,
       SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY')
                     THEN CASE WHEN DATE_PART('DAY', xml_review_date-submitted_date)<11 THEN 1
                     ELSE 0
                    END
                ELSE 0
               END) xml_review_within_10_days,
       SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY')
                     THEN CASE WHEN DATE_PART('DAY', xml_review_date-submitted_date)<4 THEN 1
                     ELSE 0
                    END
                ELSE 0
               END) xml_review_within_3_days,
       SUM(CASE WHEN TO_CHAR(d.first_published_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) published,
       percentile_cont(0.5) within group ( order by CASE WHEN TO_CHAR(d.submitted_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY')
                        THEN DATE_PART('DAY', ncbi_ready_date-submitted_date)
                   ELSE NULL END) ncbi_ready_median
FROM   generate_series(1, 12) n, audit.manuscript_process_dates d
GROUP BY TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY'), n.n) AS a
     FULL OUTER JOIN (
     SELECT TO_CHAR(date_trunc('month', a.created AT TIME ZONE 'Europe/London') , 'YYYYMM') mth, count(*) external_qa
      FROM public.team t, audit.audit_log a
      WHERE t.role_name='external-admin'
      AND a.user_id=t.user_id
      AND changes->>'status' in ('xml-review', 'xml-triage')
      AND original_data->>'status' = 'xml-qa'
      AND a.created AT TIME ZONE 'Europe/London' BETWEEN DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London') + -12 * INTERVAL '1 month' and DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London')
      GROUP BY TO_CHAR(date_trunc('month', a.created AT TIME ZONE 'Europe/London') , 'YYYYMM')
    ) qa ON a.mth = qa.mth
-- Sum of count of XMLs uploaded by tagger first time only (no corrected XMLs) + count of re-tagging notes added by admin
-- Helpdesk need to add a note containing the word 're-tag' or 'retag' (case-insensitive) for each re-tagging
      FULL OUTER JOIN (
      SELECT TO_CHAR(date_trunc('month', tag.created AT TIME ZONE 'Europe/London') , 'YYYYMM') mth, count(*) xml_tagging
FROM 
 (SELECT a.manuscript_id, min(a.created) created
   FROM audit.audit_log a, public.team t
   WHERE t.role_name='tagger'
   AND a.user_id=t.user_id
   AND a.created AT TIME ZONE 'Europe/London' BETWEEN DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London') + -12 * INTERVAL '1 month' and DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London')
   AND a.changes->>'type' = 'PMC'
   GROUP BY a.manuscript_id
 UNION
  SELECT a.manuscript_id, a.created
   FROM audit.audit_log a, public.team t
   WHERE t.role_name='admin'
   AND a.user_id=t.user_id
   AND a.created AT TIME ZONE 'Europe/London' BETWEEN DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London') + -12 * INTERVAL '1 month' and DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London')
   AND changes::text ~* '(re-|re)tag'
   AND changes->>'notes_type'='userMessage') tag
GROUP BY TO_CHAR(date_trunc('month', tag.created AT TIME ZONE 'Europe/London') , 'YYYYMM') 
) tagging ON a.mth = tagging.mth
order by a.mth desc`,
    )

    const { rows } = metrics

    rows.forEach((row, index) => {
      row.id = index
      row.created = '2018-05-17'
    })

    return rows
  }
}

module.exports = Audit
