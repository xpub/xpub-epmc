const lodash = require('lodash')
const Organization = require('./data-access')
const config = require('config')

const europepmc_plus_name = config.organization.europepmc_plus.name

const empty = {
  name: '',
}

let organizations

const OrganizationManager = {
  find: Organization.selectById,
  findAll: async refresh => {
    if (organizations && organizations.length > 0 && !refresh) {
      return organizations
    }

    organizations = await Organization.selectAll()
    return organizations
  },
  getEuropePMCPlusID: async () => {
    const organizations = await OrganizationManager.findAll()
    if (!organizations) {
      throw new Error('Organization Europe PMC Plus cannot be found')
    }

    const id = organizations
      .filter(org => org.name === europepmc_plus_name)
      .reduce((a, b) => b.id, undefined)
    if (!id) {
      throw new Error('Organization Europe PMC Plus undefined')
    }

    return id
  },
  delete: Organization.delete,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async organization => {
    let { id } = organization
    if (organization.id) {
      const updated = await Organization.update(organization)
      if (!updated) {
        throw new Error('Organization not found')
      }
    } else {
      id = await Organization.insert(organization)
    }

    return { ...organization, id }
  },
}

module.exports = OrganizationManager
