const rfr = require('rfr')

const { ManuscriptManager, AuditManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async epmc_queryActivitiesByManuscriptId(_, { id }, { user }) {
      // if (!user) {
      //   throw new Error('You are not authenticated!')
      // }

      const activity = await ManuscriptManager.selectActivityById(id)
      // logger.debug('Activity: ', JSON.stringify(activity, null, 2))
      return activity
    },
    async getMetrics(_, vars, { user }) {
      // if (!user) {
      //   throw new Error('You are not authenticated!')
      // }
      const metrics = await AuditManager.getMetrics()
      return metrics
    },
  },
}

module.exports = resolvers
