const rfr = require('rfr')

const { ReviewManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async currentReview(_, { manuscriptId }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ReviewManager.getCurrent(manuscriptId)
    },
    async deletedReviews(_, { manuscriptId }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ReviewManager.getDeleted(manuscriptId)
    },
  },
}

module.exports = resolvers
