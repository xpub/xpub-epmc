const rfr = require('rfr')
const logger = require('@pubsweet/logger')
const authentication = require('../user/authentication')

const { userMessage, resetPassword, grantEmail } = rfr('server/email')
const { ManuscriptManager, UserManager, NoteManager, IdentityManager } = rfr(
  'server/xpub-model',
)

const resolvers = {
  Mutation: {
    async epmc_email(
      _,
      { manuscriptId, to, subject, message, cc, bcc },
      { user },
    ) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const uniqueTo = [...new Set(to)]
      const uniqueCC = cc
        ? [...new Set(cc.split(',').map(s => s.trim()))]
        : null
      // Get email addresses
      const sendTo = await Promise.all(
        uniqueTo.map(async userId =>
          userId === 'helpdesk'
            ? 'helpdesk@europepmc.org'
            : UserManager.findEmail(userId),
        ),
      )
      const content = { to, uniqueCC, subject, message }
      const note = {
        manuscriptId,
        notesType: 'userMessage',
        content: JSON.stringify(content),
      }
      const sender = await UserManager.findEmail(user)
      // Send email
      await userMessage(
        sendTo,
        `${manuscriptId}: ${subject}`,
        message,
        null,
        uniqueCC,
        bcc ? sender : null,
      )
      // Create a note
      await NoteManager.create(note, user)
      return true
    },
    async epmc_emailGrantPis(_, { manuscriptId, newGrants = null }, { user }) {
      const manuscript = await ManuscriptManager.findById(manuscriptId, user)
      const { meta, teams } = manuscript
      const { fundingGroup, releaseDelay, title } = meta
      const submitter = teams.find(team => team.role === 'submitter')
      const sEmail =
        submitter &&
        submitter.teamMembers[0] &&
        UserManager.findEmail(submitter.teamMembers[0].user.id)
      const reviewer = teams.find(team => team.role === 'reviewer')
      const rEmail =
        reviewer &&
        reviewer.teamMembers[0] &&
        UserManager.findEmail(reviewer.teamMembers[0].user.id)
      if (
        sEmail &&
        rEmail &&
        fundingGroup.some(g => [rEmail, sEmail].includes(g.pi.email))
      ) {
        return true
      }
      const grantList = newGrants || fundingGroup
      const piList = grantList.reduce((pis, curr) => {
        const exists = pis.findIndex(p => p.pi.email === curr.pi.email)
        const award = {
          awardId: curr.awardId,
          fundingSource: curr.fundingSource,
          title: curr.title,
        }
        if (exists > -1) {
          pis[exists].awards.push(award)
        } else {
          pis.push({
            pi: curr.pi,
            awards: [award],
          })
        }
        return pis
      }, [])
      const manInfo = {
        id: manuscriptId,
        title,
        releaseDelay,
      }
      const reviewerName =
        reviewer &&
        reviewer.teamMembers[0] &&
        reviewer.teamMembers[0].alias.name
      await Promise.all(
        piList.map(async li =>
          grantEmail(li.pi, li.awards, manInfo, reviewerName),
        ),
      )
      return true
    },
    async epmc_emailPasswordResetLink(_, { email }, ctx) {
      const user = await UserManager.findByEmail(email)
      if (!user) {
        throw new Error('Email not registered')
      }
      const token = authentication.token.create({
        email,
        id: user.id,
      })

      // Update password reset token
      const identity = user.identities.reduce((id, identity) => {
        if (identity.email.toLowerCase() === email.toLowerCase()) {
          return identity
        }
        return id
      }, undefined)

      if (!identity) {
        throw new Error(`No user is associated with email ${email}`)
      }

      let numUpdated = 0
      try {
        // identity.passwordResetToken = token
        // await IdentityManager.save(identity)
        numUpdated = await IdentityManager.model.updatePasswordResetToken(
          identity.id,
          token,
        )
      } catch (error) {
        logger.error('Error updating password reset token')
        throw error
      }
      if (numUpdated !== 1) {
        logger.error('Failed updating password reset token')
        return false
      }

      await resetPassword(email, user, token)
      return true
    },
  },
}

module.exports = resolvers
