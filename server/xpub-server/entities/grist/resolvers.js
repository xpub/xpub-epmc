const superagent = require('superagent')
const config = require('config')
const GraphQLJSON = require('graphql-type-json')

const resolvers = {
  JSON: GraphQLJSON,

  Query: {
    async epmc_grist(_, { query, page }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }

      const url = `https://www.ebi.ac.uk/europepmc/GristAPI/rest/get/secure/query=${query}&page=${page}&showExtraInformation=true&format=json`

      let response
      try {
        response = await superagent
          .get(url)
          .auth(
            config.get('grist-api').username,
            config.get('grist-api').password,
          )
      } catch (err) {
        return { error: { status: err.status } }
      }

      return response.body
    },
  },
}

module.exports = resolvers
