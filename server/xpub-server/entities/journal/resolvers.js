const rfr = require('rfr')

const { JournalManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async journal(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return JournalManager.find(id)
    },
    async selectWithNLM(_, { nlmId }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return JournalManager.selectWithNLM(nlmId)
    },
    async alphaJournals(_, { query }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return JournalManager.searchByTitle(query)
    },
  },
}

module.exports = resolvers
