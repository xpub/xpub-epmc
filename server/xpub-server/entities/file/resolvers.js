const logger = require('@pubsweet/logger')
const rfr = require('rfr')
const lodash = require('lodash')
const uploadFiles = require('./uploadFiles')
const uploadCitation = require('./uploadCitation')

const FileManager = rfr('server/xpub-model/entities/file')
const { pushXML } = rfr('server/xml-validation')

const resolvers = {
  Mutation: {
    uploadFiles,
    uploadCitation,
    async deleteCitation(_, { manuscriptId }, { user }) {
      const deleted = await FileManager.deleteByManIdAndType(
        manuscriptId,
        'citation',
        user,
      )
      if (deleted) {
        logger.info('deleted citation: ', deleted)
        return true
      }
      return false
    },
    async updateFile(_, props, { user }) {
      if (!props.id) {
        return false
      }
      const existing = await FileManager.find(props.id)
      if (lodash.isMatch(existing, props)) {
        return false
      }

      const updatedFile = await FileManager.save(props, user)
      logger.debug('updated: ', updatedFile)
      return true
    },
    async copyFile(_, props, { user }) {
      if (!props.id) {
        return false
      }
      const existing = await FileManager.find(props.id)
      lodash.assign(existing, props)
      delete existing.id
      const updatedFile = await FileManager.save(existing, user)
      logger.debug('updated: ', updatedFile)
      return true
    },
    async deleteFile(_, { id }, { user }) {
      const deletedFile = await FileManager.deleteById(id, user)
      logger.info('deletedFile: ', deletedFile)
      return true
    },
    async replaceManuscriptFile(_, { id, fileId, files }, { user }) {
      try {
        const file = await FileManager.find(fileId)
        const { type, label } = file
        if (type === 'manuscript') {
          const deletedSource = await FileManager.deleteByManIdAndType(
            id,
            'source',
            user,
          )
          if (deletedSource) {
            logger.info('deleted source: ', deletedSource)
          }
          const deletedPDF = await FileManager.deleteByManIdAndType(
            id,
            'pdf4load',
            user,
          )
          if (deletedPDF) {
            logger.info('deleted PDF for load: ', deletedPDF)
          }
        }
        const deletedFile = await FileManager.deleteById(fileId, user)
        logger.info('deletedFile: ', deletedFile)
        return uploadFiles(_, { id, files, type, label }, { user })
      } catch (err) {
        logger.error('Error replacing file', err)
        return false
      }
    },
    async convertXML(_, { id }, { user }) {
      try {
        const xmlFile = await FileManager.find(id)
        await pushXML(xmlFile.url, xmlFile.manuscriptId, user)
      } catch (error) {
        logger.error(error)
        return false
      }
      return true
    },
  },
}

module.exports = resolvers
