const uuidv1 = require('uuid/v1')
const config = require('config')
const logger = require('@pubsweet/logger')
const { minioClient } = require('@europepmc/express-middleware-minio')
const { Readable } = require('stream')
const rfr = require('rfr')

const { ManuscriptManager, FileManager } = rfr('server/xpub-model')

async function uploadCitation(_, { manuscriptId }, { user }) {
  const manuscript = await ManuscriptManager.findById(manuscriptId, user)
  const { meta } = manuscript
  const {
    articleIds,
    volume,
    issue,
    location,
    publicationDates,
    citerefUrl,
    fulltextUrl,
  } = meta
  const { fpage, lpage, elocationId } = location
  const contents = `<citation>${volume &&
    `
  <volume>${volume}</volume>`}${issue &&
    `
  <issue>${issue}</issue>`}${fpage &&
    `
  <fpage>${fpage}</fpage>`}${lpage &&
    `
  <lpage>${lpage}</lpage>`}${elocationId &&
    `
  <elocation-id>${elocationId}</elocation-id>`}${
    articleIds.length > 0
      ? articleIds
          .map(
            aid => `\n  <article-id pub-id-type="${aid.pubIdType}">${
              aid.id
            }</article-id>
  `,
          )
          .join('')
      : ''
  }${
    publicationDates.length > 0
      ? publicationDates
          .map(pDate =>
            pDate.jatsDate
              ? `<pub-date pub-type="${pDate.type}">\n${Object.keys(
                  pDate.jatsDate,
                )
                  .sort()
                  .map(el =>
                    pDate.jatsDate[el]
                      ? `    <${el}>${pDate.jatsDate[el]}</${el}>\n`
                      : '',
                  )
                  .join('')}  </pub-date>`
              : '',
          )
          .join('\n  ')
      : ''
  }${
    citerefUrl
      ? `
  <citeref-url>${citerefUrl}</citeref-url>`
      : ''
  }${
    fulltextUrl
      ? `
  <fulltext-url>${fulltextUrl}</fulltext-url>`
      : ''
  }
</citation>`
  const filename = `${manuscriptId}.cit`
  const mimeType = 'text/xml'
  const minioLoc = `${uuidv1()}.cit`
  try {
    const stream = new Readable()
    stream._read = () => {}
    stream.push(contents)
    stream.push(null)
    const file = [
      minioLoc, // filename
      filename, // oriFilename
      mimeType, // fileType
      stream, // stream
    ]
    await minioClient.uploadFileSteam(...file)
  } catch (err) {
    logger.error('Minio error: ', err)
    return false
  }
  try {
    const { size } = await minioClient.getFileStat(minioLoc)
    const files = await FileManager.findByManuscriptId(manuscriptId)
    const exists = files.find(f => f.type === 'citation')
    const savedFile = await FileManager.save(
      {
        id: (exists && exists.id) || null,
        manuscriptId,
        filename,
        mimeType,
        type: 'citation',
        url: `${config.file.url.download}/${minioLoc}`,
        size,
      },
      user,
    )
    return !!savedFile
  } catch (err) {
    logger.error('Create file error: ', err)
    return false
  }
}

module.exports = uploadCitation
