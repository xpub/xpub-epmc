const uuidv1 = require('uuid/v1')
const logger = require('@pubsweet/logger')
const config = require('config')
const rfr = require('rfr')
const { minioClient } = require('@europepmc/express-middleware-minio')
const { timer } = require('benmark')
const fs = require('fs')
const http = require('http')

const FileModel = rfr('server/xpub-model/entities/file/data-access')
const tar = rfr('server/utils/unTar')
const fileUtil = rfr('server/utils/files')
const { xsweetConvert } = rfr('server/xsweet-conversion')
const { internalBaseUrl } = config.get('pubsweet-server')

const streamPromise = stream =>
  new Promise((resolve, reject) => {
    stream.on('finish', () => {
      resolve('finish')
    })
    stream.on('error', error => {
      reject(error)
    })
  })

const handleFile = async (file, manuscriptId, type, user) => {
  const { stream, filename, mimetype } = await file
  let newFilename = uuidv1()
  let extension = ''

  if (filename) {
    extension = filename.split('.').pop()
    if (extension) {
      newFilename += `.${extension}`
    }
  }

  try {
    await timer(`S3 uploading file ${filename}`)(minioClient.uploadFileSteam)(
      newFilename,
      filename,
      mimetype,
      stream,
    )

    const { size } = await minioClient.getFileStat(newFilename)

    // case when manuscript is a docx file
    if (
      type === 'manuscript' &&
      mimetype ===
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ) {
      try {
        logger.info(`Starting XSweet conversion of ${filename}`)
        const tmpPath = await tar.createTempDir()
        const wstreamTmp = fs.createWriteStream(`${tmpPath}/${newFilename}`)
        const fileUrl = `${internalBaseUrl}/download/${newFilename}`
        http.get(fileUrl, response => response.pipe(wstreamTmp))
        await streamPromise(wstreamTmp)
        await xsweetConvert(`${tmpPath}/${newFilename}`, manuscriptId, user)
        logger.info(
          `XSweet conversion of ${filename} for ${manuscriptId} is done`,
        )
        fileUtil.tidyUp(tmpPath)
      } catch (err) {
        return { err }
      }
    }

    return { filename, url: newFilename, mimetype, size }
  } catch (err) {
    return { err }
  }
}

async function uploadFiles(
  _,
  { id, files, type = '', label = null },
  { user },
) {
  const resolvedFiles = await files
  const savedFiles = []
  await Promise.all(
    resolvedFiles.map(async resolvedFile => {
      const { err, filename, url, mimetype, size } = await handleFile(
        resolvedFile,
        id,
        type,
        user,
      )
      if (err || !filename) {
        logger.error('Minio error: ', err)
        return
      }
      logger.info(`Minio filename: ${url} (original filename: ${filename})`)
      // Update database
      const file = new FileModel({
        manuscriptId: id,
        filename,
        mimeType: mimetype,
        url: `${config.file.url.download}/${url}`,
        type,
        size,
        label,
        updatedBy: user,
      })
      const savedFile = await file.save()
      logger.debug('savedFile: ', savedFile)
      savedFiles.push(savedFile)
    }),
  )

  const manuscript = {
    id,
    files: savedFiles,
  }
  logger.info('Manuscript updated: ', manuscript)
  return savedFiles.length > 0
}

module.exports = uploadFiles
