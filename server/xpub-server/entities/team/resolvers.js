const rfr = require('rfr')

const { TeamManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async userTeams(_, { id }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      const teamList = await TeamManager.selectByUserId(id)
      return teamList
    },
  },
  Mutation: {
    async addReviewer(_, { noteId }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      return TeamManager.addNewReviewer(noteId, ctx.user)
    },
  },
}

module.exports = resolvers
