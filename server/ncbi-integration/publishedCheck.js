const logger = require('@pubsweet/logger')
const fetch = require('node-fetch')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const rfr = require('rfr')

const { completedEmail } = rfr('server/email')

;(async () => {
  const beforeUpdate = Date.now()
  await publishedCheck()
  Manuscript.knex().destroy()
  logger.info(`Published check was finished in ${Date.now() - beforeUpdate} ms`)
})()

// module.exports = async publishedCheck => {
async function publishedCheck() {
  const manuscripts = await Manuscript.query()
    .distinct('manuscript.id')
    .select('manuscript.*')
    .eager('users.[roles, identities]')
    .where('manuscript.status', 'ncbi-ready')
    .where('manuscript.ncbiState', 'success')
    .whereNull('manuscript.deleted')

  await Promise.all(
    manuscripts.map(async manuscript => {
      if (await checkEuropepmc(manuscript)) {
        manuscript.status = 'published'
        manuscript.ncbiState = null
        const manuscript4Db = new Manuscript(manuscript)
        delete manuscript4Db.users
        await manuscript4Db.save()
        logger.info(
          `Status of manuscript ${manuscript.id} was changed to 'published'`,
        )
        sendEmails(manuscript)
      }
    }),
  )
}

function checkEuropepmc(manuscript) {
  return new Promise((resolve, reject) => {
    try {
      fetch(
        `https://www.ebi.ac.uk/europepmc/webservices/rest/search?resulttype=idlist&format=json&query=AUTH_MAN_ID:${
          manuscript.id
        } AND IN_EPMC:y`,
        {
          method: 'GET',
        },
      )
        .then(response => response.json())
        .then(data => resolve(data.hitCount !== 0))
        .catch(err => {
          logger.error(err)
          resolve(false)
        })
    } catch (ignored) {
      resolve(false)
    }
  })
}

function sendEmails(manuscript) {
  const uniqueUsers = manuscript.users.reduce((array, curr) => {
    if (
      !array.some(u => u.id === curr.id) &&
      !curr.deleted &&
      curr.roles.some(
        role => role.name === 'reviewer' || role.name === 'submitter',
      )
    ) {
      array.push(curr)
    }
    return array
  }, [])
  const getEmails = uniqueUsers.reduce((array, curr) => {
    const emails = curr.identities.filter(i => !i.deleted).map(i => i.email)
    array.concat(emails)
    return array
  }, [])
  const manInfo = {
    id: manuscript.id,
    pmcid: manuscript['meta,articleIds'].filter(
      id => id.pubIdType === 'pmcid',
    )[0].id,
    title: manuscript['meta,title'],
  }
  completedEmail(getEmails, manInfo)
}
