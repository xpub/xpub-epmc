const config = require('config')
const logger = require('@pubsweet/logger')
const fetch = require('node-fetch')
const moment = require('moment')
const Client = require('ftp')
const { Readable } = require('stream')

module.exports.createGrantLinks = async function createGrantLinks(
  manuscripts,
  removeFundingGroup,
) {
  try {
    const filename = `grants${
      manuscripts.length === 1 ? `.${manuscripts[0].id}` : ''
    }.${moment().format('YYYY_MM_DD-HH_mm_SS')}.xml`
    const sendManuscripts = manuscripts.reduce((list, manuscript) => {
      const articleIds =
        manuscript.meta.articleIds &&
        manuscript.meta.articleIds
          .filter(a => ['pmcid', 'pmid'].includes(a.pubIdType))
          .sort((a, b) => (a.pubIdType > b.pubIdType ? 1 : -1))
      const fundingGroup =
        (manuscript.meta.fundingGroup &&
          manuscript.meta.fundingGroup.length > 0 &&
          manuscript.meta.fundingGroup) ||
        undefined
      if (
        articleIds &&
        articleIds.length > 0 &&
        (fundingGroup || removeFundingGroup)
      ) {
        list.push({
          id: manuscript.id,
          articleIds,
          fundingGroup,
          removeFundingGroup,
        })
      } else {
        logger.info(`${manuscript.id} No grants to link or no ID to link to`)
      }
      return list
    }, [])
    if (sendManuscripts.length > 0) {
      let contents = `<?xml version="1.0" encoding="UTF-8"?>
<grants file="${filename}">`
      const grantsLists = await Promise.all(
        sendManuscripts.map(async each => {
          const grantList = await createGrantXML(
            each.articleIds,
            each.fundingGroup,
            each.removeFundingGroup,
          )
          logger.info(`${each.id} Grant list created`)
          return grantList
        }),
      )
      contents += grantsLists.join('\n')
      contents += '</grants>'
      await sendXML(contents, filename)
      logger.info(`Grant list sent to NCBI`)
      return { success: true, message: 'Grant list sent to NCBI' }
    }
    return { success: true, message: 'No grants to link or no ID to link to' }
  } catch (err) {
    logger.error(`Unable to link grants: ${err}`)
    return { success: false, message: `Unable to link grants: ${err}` }
  }
}

async function createGrantXML(
  articleIds = [],
  fundingGroup = [],
  removeFundingGroup = [],
) {
  const response = await fetch(
    'https://www.ebi.ac.uk/europepmc/GristAPI/rest/api/get/funders',
    {
      method: 'GET',
    },
  )
  const json = await response.json()
  const { funderInfoList } = json
  const listIDs = articleIds
    .map(a => `${a.pubIdType}="${a.id.replace('PMC', '')}"`)
    .join(' ')
  const xml = `${removeFundingGroup
    .map(
      g =>
        `  <grant cmd="remove" ${listIDs} grant-name="${funderInfoList.find(
          f => f.name === g.fundingSource,
        ) && funderInfoList.find(f => f.name === g.fundingSource).grantPrefix}${
          g.awardId
        }" authority="WT"/>`,
    )
    .join('\n')}
${fundingGroup
  .map(
    g =>
      `  <grant cmd="add" ${listIDs} grant-name="${funderInfoList.find(
        f => f.name === g.fundingSource,
      ) && funderInfoList.find(f => f.name === g.fundingSource).grantPrefix}${
        g.awardId
      }" authority="WT"/>`,
  )
  .join('\n')}`
  return xml
}

function sendXML(contents, filename) {
  return new Promise((resolve, reject) => {
    let ftp = new Client()
    const stream = new Readable()
    stream._read = () => {}
    stream.push(contents)
    stream.push(null)
    ftp.on('ready', () => {
      ftp.put(
        stream,
        `/${config.get('ncbi-ftp')['send-folder']}/${filename}`,
        err => {
          ftp.end()
          ftp = null
          if (err) {
            reject(err)
          }
          resolve(true)
        },
      )
    })
    const { host, user, password } = config.get('ncbi-ftp')
    ftp.connect({ host, user, password })
  })
}
