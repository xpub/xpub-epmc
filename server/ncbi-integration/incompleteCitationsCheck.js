const moment = require('moment')
const logger = require('@pubsweet/logger')
const fetch = require('node-fetch')
const JournalManager = require('../xpub-model/entities/journal/')
const FileManager = require('../xpub-model/entities/file/')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const ManuscriptManager = require('../xpub-model/entities/manuscript/')
const getUser = require('../utils/user.js')
const config = require('config')

let eutilsApiKey = null
try {
  eutilsApiKey = config.get('eutils-api-key')
} catch (e) {
  // console.log("eutils-api-key is not defined")
}

let user = null
;(async () => {
  const beforeUpdate = Date.now()
  logger.info(`Running incomplete citations check...`)
  user = await getUser.getAdminUser()
  await incompleteCitationsCheck()
  await Manuscript.knex().destroy()
  logger.info(
    `Incomplete citations check was finished in ${Date.now() -
      beforeUpdate} ms`,
  )
  process.exit(0)
})()

// module.exports = async publishedCheck => {
async function incompleteCitationsCheck() {
  const manuscripts = await Manuscript.query()
    .where('status', 'xml-complete')
    // .whereNull('meta,volume')
    .whereJsonSupersetOf('meta,article_ids', [{ pubIdType: 'pmid' }])
    .whereNull('manuscript.deleted')

  const pmids = manuscripts
    .map(
      manuscript =>
        manuscript['meta,articleIds'].filter(id => id.pubIdType === 'pmid')[0]
          .id,
    )
    .reduce((acc, pmid) => `${acc + pmid},`, '')

  await checkPubmed(manuscripts, pmids)
}

function isDate(d) {
  let date = moment.utc(d, 'YYYY MMM DD')
  if (!date.isValid()) {
    date = moment.utc(d, 'YYYY MMM')
    if (!date.isValid()) {
      return false
    }
  }
  return date.format('YYYY-MM-DD')
}

async function selectResult(results, manuscripts) {
  /* eslint-disable no-await-in-loop */
  /* eslint-disable no-restricted-syntax */
  for (const thisUid of results.result.uids) {
    const result = results.result[thisUid]
    const journal = await JournalManager.selectWithNLM(result.nlmuniqueid)
    const publicationDates = []
    if (result.pubdate) {
      const ppub = { type: 'ppub' }
      if (isDate(result.pubdate)) {
        ppub.date = isDate(result.pubdate)
      } else {
        ppub.date = moment.utc(result.pubdate.substring(0, 4), 'YYYY')
        ppub.jatsDate = {
          year: result.pubdate.substring(0, 4),
          season: result.pubdate.substring(5),
        }
      }
      publicationDates.push(ppub)
    }
    if (result.epubdate) {
      const epub = { type: 'epub' }
      if (isDate(result.epubdate)) {
        epub.date = isDate(result.epubdate)
      } else {
        epub.date = moment.utc(result.pubdate.substring(0, 4), 'YYYY')
        epub.jatsDate = {
          year: result.pubdate.substring(0, 4),
          season: result.epubdate.substring(5),
        }
      }
      publicationDates.push(epub)
    }
    const citationData = {
      journalId: journal.id,
      meta: {
        unmatchedJournal: null,
        title: result.title,
        volume: result.volume,
        issue: result.issue,
        location: {
          fpage: result.pages.split('-')[0] || null,
          lpage: result.pages.split('-').pop() || null,
          elocationId: result.elocationid,
        },
        publicationDates,
        articleIds: [
          {
            pubIdType: 'pmid',
            id: result.uid,
          },
        ],
      },
    }
    if (result.articleids && result.articleids.find(i => i.idtype === 'doi')) {
      citationData.meta.articleIds.push({
        pubIdType: 'doi',
        id: result.articleids.find(i => i.idtype === 'doi').value,
      })
    }

    manuscripts.forEach(manuscript => {
      const arIds = manuscript['meta,articleIds']
      arIds.forEach(arId => {
        if (arId.id === thisUid && arId.pubIdType === 'pmid') {
          citationData.id = manuscript.id
          if (
            manuscript.status === 'xml-complete' &&
            citationData.meta &&
            (citationData.meta.volume || citationData.meta.issue)
          ) {
            citationData.status = 'ncbi-ready'
          }
        }
      })
    })

    await FileManager.deleteByManIdAndType(citationData.id, 'citation', user.id)

    logger.info(`Removed unneeded ${citationData.id} citation file`)

    await ManuscriptManager.update(citationData, user.id)

    logger.info(`Updated incomplete citation for manuscript ${citationData.id}`)
  }
}

function checkPubmed(manuscripts, pmids) {
  return new Promise((resolve, reject) => {
    try {
      fetch(
        `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=${pmids}&retmode=json${
          eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
        }`,
        {
          method: 'GET',
        },
      )
        .then(response => response.json())
        .then(async data => {
          await selectResult(data, manuscripts)
          resolve(true)
        })
        .catch(err => {
          logger.error(err)
          resolve(false)
        })
    } catch (ignored) {
      resolve(false)
    }
  })
}
