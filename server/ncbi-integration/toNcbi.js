const moment = require('moment')
const logger = require('@pubsweet/logger')
const path = require('path')
const { exec } = require('child_process')
const http = require('http')
const fs = require('fs')
const os = require('os')
const config = require('config')
const md5File = require('md5-file')
const Client = require('ftp')
const lodash = require('lodash')
const fileUtils = require('../utils/files.js')

// Pubsweet server is running locally from toNcbi's perspective
const pubsweetServer = config.get('pubsweet-server.internalBaseUrl')

// const ftpLocation = '/home/mselim/test'
const ncbiFileTypes = config.get('ncbiFileTypes')

// const ManuscriptManager = require('../xpub-model/entities/manuscript')
const ManuscriptAccess = require('../xpub-model/entities/manuscript/data-access')
const {
  dManuscriptUpdate,
  gManuscript,
} = require('../xpub-model/entities/manuscript/helpers/transform')

const NCBI_PACKAGE_EXT = '.ld'

;(async () => {
  const beforeUpdate = Date.now()
  await toNcbi()
  logger.info(`ncbi check in ${Date.now() - beforeUpdate} ms`)
})()

async function toNcbi() {
  ManuscriptAccess.findNcbiReady()
    .then(async manuscripts => {
      await manuscripts.reduce(
        (p, manuscript) =>
          p.then(() => {
            logger.info(
              `Sending package ${manuscript.id} to ncbi FTP location.`,
            )
            return createTempDir(manuscript).then(tmpPath =>
              getFiles(tmpPath, manuscript).then(() =>
                createManifest(tmpPath, manuscript).then(() =>
                  compress(tmpPath, manuscript).then(destination =>
                    send(destination, manuscript).then(() =>
                      tidyUp(tmpPath, manuscript).then(manuscript =>
                        updateManuscriptNcbiStatus(manuscript).then(
                          updatedManuscript => {
                            logger.info(`Done updating ${manuscript.id}.`)
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          }),
        Promise.resolve(),
      )
    })
    .catch(e => logger.error(e))
    .finally(() => {
      logger.info('About to close database connection')
      teardownDBConnectionPool()
    })
}

process
  .on('uncaughtException', err => {
    logger.error('Uncaught Exception thrown: ', err)
    teardownDBConnectionPool()
    process.exit(1)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection at Promise: ', promise)
    logger.error('Reason: ', reason)
    teardownDBConnectionPool()
    process.exit(1)
  })

function teardownDBConnectionPool() {
  logger.info('ncbi-integration/toNcbi: closing connection to the database...')
  ManuscriptAccess.knex().destroy()
}

function createTempDir(manuscript) {
  return new Promise((resolve, reject) => {
    // noinspection JSCheckFunctionSignatures
    const directory = `${os.tmpdir()}/${getFilesPrefix(manuscript)}`
    // if temp directory exists, remove its contents:
    fs.access(directory, fs.constants.F_OK, async err => {
      if (!err) {
        await tidyUp(directory)
      }
      fs.mkdir(directory, err => {
        if (err) {
          reject(err)
        }
        resolve(directory)
      })
    })
  })
}

function getFiles(tmpPath, manuscript) {
  const promises = [] // array of promises
  // list of files needed by ncbi
  const files = getNcbiFiles(manuscript)
  files.forEach(file => {
    const promise = new Promise((resolve, reject) => {
      const fileOnDisk = fs.createWriteStream(`${tmpPath}/${file.filename}`)
      http
        .get(`${pubsweetServer}${file.url}`, response => {
          let stream = response.pipe(fileOnDisk)
          stream.on('finish', () => {
            resolve(true)
            stream = null
          })
        })
        .on('error', err => {
          // Handle errors
          reject(err)
        })
    })
    promises.push(promise)
  })
  return Promise.all(promises)
}

function createManifest(tmpPath, manuscript) {
  const articleIds = manuscript['meta,articleIds']
  const pmcId =
    articleIds && articleIds.find(id => id.pubIdType === 'pmcid')
      ? articleIds.find(id => id.pubIdType === 'pmcid').id.substring(3)
      : 0
  const pmId =
    articleIds && articleIds.find(id => id.pubIdType === 'pmid')
      ? articleIds.find(id => id.pubIdType === 'pmid').id
      : 0

  const submitter = manuscript.teams.find(t => t.roleName === 'submitter')
  const subUser = submitter && submitter.users[0] && submitter.users[0]
  const identity = subUser && subUser.identities[0]
  const isPublisher = identity.meta && identity.meta.publisher
  const epubPublisher = isPublisher && isPublisher === 'NPG'

  const createdDate = moment(manuscript.created).format('YYYY-MM-DD')
  const pubDates = manuscript['meta,publicationDates']
  const releaseDelay = manuscript['meta,releaseDelay']
    ? parseInt(manuscript['meta,releaseDelay'], 10)
    : 0
  const epubDate =
    pubDates.find(date => date.type === 'epub') &&
    pubDates.find(date => date.type === 'epub').date
  const ppubDate =
    pubDates.find(date => date.type === 'ppub') &&
    pubDates.find(date => date.type === 'ppub').date
  const startDate =
    (epubPublisher && epubDate && epubDate) ||
    (ppubDate && ppubDate) ||
    (epubDate && epubDate)
  const publishDate = moment(startDate)
    .add(releaseDelay, 'M')
    .format('YYYY-MM-DD')

  const journalNlmId = manuscript.journal['meta,nlmuniqueid']
  const files = getNcbiFiles(manuscript)
  const hasPubPdf = files.find(file => file.type === 'pdf4load') ? '1' : '0'
  let text = `<?xml version="1.0"?>\n<!DOCTYPE pnihms-xdata PUBLIC "-//PNIHMS-EXCHANGE//DTD pNIHMS Exchange DTD//EN" "pnihms_exchange.dtd">\n
  <pnihms-xdata mid="${getFilesPrefix(
    manuscript,
  )}" pubmed-id="${pmId}" pmc-id="${pmcId}" create-date="${createdDate}" publish-date="${publishDate}" pub-pdf="${hasPubPdf}" nlm-journal-id="${journalNlmId}"`
  if (isPublisher) {
    text += ` submitter-name="${subUser.givenNames} ${
      subUser.surname
    }" submitter-login="${isPublisher}" submitter-authority="publisher"`
  }
  text += `> \n<blobs>\n${files
    .map(file => {
      const { numeric } = ncbiFileTypes.find(type => type.value === file.type)
      const fileMd5 = getFileMd5(`${tmpPath}/${file.filename}`)
      return `  <blob md5="${fileMd5}" name="${
        file.filename
      }" type="${numeric}" />`
    })
    .join('\n')}</blobs>\n
  </pnihms-xdata>`

  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/${getFilesPrefix(manuscript)}.mxml`, text, err => {
      if (err) {
        reject(err)
      }
      resolve(tmpPath)
    })
  })
}

function compress(tmpPath, manuscript) {
  return new Promise((resolve, reject) => {
    const currentDate = new Date()
    // const date = currentDate.toISOString().split("T")[0]
    const timestamp = currentDate
      .toISOString()
      .replace(/-/g, '_')
      .replace('T', '-')
      .replace(/:/g, '_')
      .split('.')[0]
    const dest = `${tmpPath}/${getFilesPrefix(
      manuscript,
    )}.${timestamp}${NCBI_PACKAGE_EXT}.zip`
    const cmd = `cd ${tmpPath} && zip -r ${dest} .`
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        throw new Error('cannot compress files')
      }
      global.destination = dest
      resolve(dest)
    })
  })
}

function send(dest, manuscript) {
  return new Promise((resolve, reject) => {
    let c = new Client()
    c.on('ready', () => {
      c.put(
        dest,
        `/${config.get('ncbi-ftp')['send-folder']}/${path.basename(dest)}`,
        err => {
          if (err) {
            reject(err)
          }
          c.end()
          c = null
          resolve(true)
        },
      )
    })
    const { host, user, password } = config.get('ncbi-ftp')
    c.connect({ host, user, password })
  })
}

function tidyUp(tmpPath, manuscript) {
  try {
    const cmd = `rm -rf ${tmpPath}*`
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err)
        }
        // logger.info(`Finished process`)
        resolve(manuscript)
      })
    })
  } catch (e) {
    logger.error('Error while tidying up after another error!')
  }
}

function getFilesPrefix(manuscript) {
  return manuscript.id.toLowerCase()
}

function getNcbiFiles(manuscript) {
  return manuscript.files.reduce((list, file) => {
    if (!file.deleted && ncbiFileTypes.some(type => type.value === file.type)) {
      if (['supplement', 'pdf4load'].includes(file.type)) {
        file.filename = fileUtils.normalizeFilename(file)
      }
      list.push(file)
    }
    return list
  }, [])
}

function getFileMd5(filePath) {
  return md5File.sync(filePath)
}

async function updateManuscriptNcbiStatus(manuscript) {
  await updateManuscript(
    {
      id: manuscript.id,
      ncbiState: 'sent',
    },
    manuscript.updatedBy,
  )
}

async function updateManuscript(input) {
  const originalMan = await ManuscriptAccess.selectById(input.id)
  if (!originalMan) {
    throw new Error('Manuscript not found')
  }
  const manuscriptUpdate = dManuscriptUpdate(input)
  lodash.assign(originalMan, manuscriptUpdate)
  await originalMan.save()
  const updatedMan = await ManuscriptAccess.selectById(input.id, true)
  return gManuscript(updatedMan)
}
