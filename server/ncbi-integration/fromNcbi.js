const Client = require('ftp')
const config = require('config')
const fs = require('fs')
const os = require('os')
const Dom = require('xmldom').DOMParser
const logger = require('@pubsweet/logger')
const lodash = require('lodash')
const ManuscriptAccess = require('../xpub-model/entities/manuscript/data-access')
const {
  dManuscriptUpdate,
  gManuscript,
} = require('../xpub-model/entities/manuscript/helpers/transform')
const { createGrantLinks } = require('./linkGrants')

const NCBI_RESPONSE_EXT = new RegExp(/\S+.ld.response.xml$/i)
let c = new Client()

;(async () => {
  logger.info('running from NCBI check')
  const beforeUpdate = Date.now()
  const updatedManuscripts = await fromNcbi()
  close(c)
  logger.info(`from NCBI check was finished in ${Date.now() - beforeUpdate} ms`)
  const successManuscripts = updatedManuscripts.filter(
    m => m.ncbiState === 'success',
  )
  if (successManuscripts.length > 0) {
    logger.info('Sending grant data')
    await createGrantLinks(successManuscripts)
  }
  process.exit()
})()

function close(c) {
  c.end()
  c = null
  logger.info('connection terminated')
}

function fromNcbi() {
  return new Promise((resolve, reject) => {
    const updatedManuscripts = []
    c.on('ready', () => {
      c.list(
        `/${config.get('ncbi-ftp')['receive-folder']}`,
        false,
        (err, list) => {
          if (err) {
            close(c)
            c = null
            reject(err)
          }
          const responseFiles = list.filter(file =>
            NCBI_RESPONSE_EXT.test(file.name),
          )
          if (!responseFiles.length) {
            close(c)
            logger.info('No files to load')
            reject()
          }
          let counter = 0
          responseFiles.forEach((file, index, array) => {
            const remoteFilePath = `/${
              config.get('ncbi-ftp')['receive-folder']
            }/${file.name}`
            c.get(remoteFilePath, (err, stream) => {
              if (err) return
              const path = `${os.tmpdir()}/${file.name}`
              const writeStream = stream.pipe(fs.createWriteStream(path))
              writeStream.on('finish', () => {
                processFile(path).then(response => {
                  updateManuscriptNcbiStatus(file.name, response).then(
                    updatedManuscript => {
                      if (!updatedManuscript) {
                        counter += 1
                        if (counter === array.length) {
                          resolve(updatedManuscripts)
                        }
                      } else {
                        updatedManuscripts.push(updatedManuscript)
                        c.rename(
                          remoteFilePath,
                          `${remoteFilePath}.processed`,
                          err => {
                            if (err) {
                              logger.info(err)
                            }
                            counter += 1
                            if (counter === array.length) {
                              resolve(updatedManuscripts)
                            }
                          },
                        )
                      }
                    },
                  )
                })
              })
            })
          })
        },
      )
    })
    const { host, user, password } = config.get('ncbi-ftp')
    c.connect({ host, user, password })
  })
}

process
  .on('uncaughtException', err => {
    logger.error('Uncaught Exception thrown: ', err)
    close(c)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection at Promise: ', promise)
    logger.error('Reason: ', reason)
    close(c)
  })

function processFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      const doc = new Dom().parseFromString(data)
      if (doc) {
        const receipt = doc.getElementsByTagName('receipt')[0]
        resolve({
          status: parseInt(receipt.getAttribute('status'), 10),
          pmcid: receipt.getAttribute('pmcid'),
          formState: data.match(
            /==== BEGIN: STDERR ====(.*)==== END: STDERR ====/s,
          )
            ? data.match(/==== BEGIN: STDERR ====(.*)==== END: STDERR ====/s)[0]
            : data,
        })
      } else {
        reject(new Error('xml parse error'))
      }
    })
  })
}

async function updateManuscriptNcbiStatus(fileName, response) {
  // filename example: ems76395.2019_01_01-08_30_06.ld.response.xml
  const manuscriptId = fileName.split('.')[0].toUpperCase()
  const manuscript = await ManuscriptAccess.selectById(manuscriptId)
  if (!manuscript) {
    logger.info(`manuscript ${manuscriptId} not found in db`)
    return null
  }
  const manInput = {
    id: manuscriptId,
    ncbiState: response.status ? 'failure' : 'success',
  }
  const newIds = manuscript['meta,articleIds'].filter(
    id => id.pubIdType !== 'pmcid',
  )
  if (response.status) {
    manInput.formState = `Failed NCBI loading\n\n${response.formState}`
    manInput.status = 'ncbi-triage'
  } else if (response.pmcid) {
    const newPMCID = response.pmcid.startsWith('PMC')
      ? response.pmcid
      : `PMC${response.pmcid}`
    newIds.push({ pubIdType: 'pmcid', id: newPMCID })
  }
  manInput['meta,articleIds'] = response.pmcid
    ? newIds
    : manuscript['meta,articleIds']
  const newManuscript = await updateManuscript(manInput)
  logger.info(`manuscript ${manuscriptId} updated`)
  return newManuscript
}

async function updateManuscript(input) {
  const originalMan = await ManuscriptAccess.selectById(input.id)
  if (!originalMan) {
    throw new Error('Manuscript not found')
  }
  const manuscriptUpdate = dManuscriptUpdate(input)
  lodash.assign(originalMan, manuscriptUpdate)
  await originalMan.save()
  const updatedMan = await ManuscriptAccess.selectById(input.id, true)
  return gManuscript(updatedMan)
}
