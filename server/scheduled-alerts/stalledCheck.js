const logger = require('@pubsweet/logger')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const rfr = require('rfr')

const { incompleteEmail, stalledEmail, stalledErrorEmail } = rfr('server/email')

;(async () => {
  const updateTime = Date.now()
  logger.info(`Running stalled manuscripts check`)
  await stalledCheck()
  logger.info(
    `Stalled manuscripts check was finished in ${Date.now() - updateTime} ms`,
  )
  Manuscript.knex().destroy()
})()

async function stalledCheck() {
  await Promise.all([alertIncomplete(), alertStalled(), alertNeedFix()])
  return true
}

async function alertIncomplete() {
  const incomplete = await Manuscript.query()
    .whereIn('status', ['INITIAL', 'READY'])
    .whereRaw(
      '("updated"::date = current_date - interval \'1 day\' or "updated"::date = current_date - interval \'7 days\')',
    )
    .whereNull('manuscript.deleted')
    .eager('[teams.users.identities]')
    .modifyEager('teams', builder => {
      builder.whereNull('deleted')
    })

  if (incomplete && incomplete.length > 0) {
    await Promise.all(
      incomplete.map(async m => {
        try {
          const submitter =
            m.teams.find(t => t.roleName === 'submitter') &&
            m.teams.find(t => t.roleName === 'submitter').users[0]
          if (submitter) {
            const manInfo = {
              id: m.id,
              title: m['meta,title'],
              status: m.status,
            }
            logger.info(`Sending email for incomplete manuscript ${m.id}`)
            await incompleteEmail(manInfo, submitter)
            return true
          }
          throw new Error('No submitter for manuscript')
        } catch (e) {
          logger.error(
            `Unable to send email for incomplete manuscript ${m.id}: ${e}`,
          )
          return true
        }
      }),
    )
    return true
  }
  logger.info('No incomplete manuscripts.')
  return true
}

async function alertStalled() {
  const stalled = await Manuscript.query()
    .whereIn('status', ['in-review', 'xml-review'])
    .whereRaw(
      '("updated"::date = current_date - interval \'1 week\' or "updated"::date = current_date - interval \'2 weeks\')',
    )
    .whereNull('manuscript.deleted')
    .eager('[notes, teams.users.identities]')
    .modifyEager('teams', builder => {
      builder.whereNull('deleted')
    })
    .modifyEager('notes', builder => {
      builder.whereNull('deleted')
    })

  if (stalled && stalled.length > 0) {
    await Promise.all(
      stalled.map(async m => {
        try {
          const reviewer = {}
          const reviewerNote = m.notes.find(
            n => n.notesType === 'selectedReviewer',
          )
          if (reviewerNote) {
            const reviewerInfo = JSON.parse(reviewerNote.content)
            reviewer.token = reviewerNote.id
            reviewer.title = reviewerInfo.name.title
            reviewer.givenNames = reviewerInfo.name.givenNames
            reviewer.surname = reviewerInfo.name.surname
            reviewer.email = reviewerInfo.email
          } else {
            const reviewerInfo =
              m.teams.find(t => t.roleName === 'reviewer') &&
              m.teams.find(t => t.roleName === 'reviewer').users[0]
            if (reviewerInfo) {
              reviewer.title = reviewerInfo.title
              reviewer.givenNames = reviewerInfo.givenNames
              reviewer.surname = reviewerInfo.surname
              reviewer.email = reviewerInfo.identities[0].email
            } else {
              throw new Error('No reviewer for manuscript')
            }
          }
          if (reviewer.email) {
            const manInfo = {
              id: m.id,
              title: m['meta,title'],
              status: m.status,
            }
            logger.info(`Sending email for stalled review manuscript ${m.id}`)
            await stalledEmail(manInfo, reviewer)
            return true
          }
          throw new Error('No email for manuscript reviewer')
        } catch (e) {
          logger.error(
            `Unable to send email for stalled review manuscript ${m.id}: ${e}`,
          )
          return true
        }
      }),
    )
    return true
  }
  logger.info('No stalled review manuscripts.')
  return true
}

async function alertNeedFix() {
  const needFix = await Manuscript.query()
    .where('status', 'submission-error')
    .whereRaw(
      '("updated"::date = current_date - interval \'1 week\' or "updated"::date = current_date - interval \'2 weeks\')',
    )
    .whereNull('manuscript.deleted')
    .eager('[notes, teams.users.identities]')
    .modifyEager('teams', builder => {
      builder.whereNull('deleted')
    })
    .modifyEager('notes', builder => {
      builder.whereNull('deleted')
    })

  if (needFix && needFix.length > 0) {
    await Promise.all(
      needFix.map(async m => {
        try {
          const submitter =
            m.teams.find(t => t.roleName === 'submitter') &&
            m.teams.find(t => t.roleName === 'submitter').users[0]
          const reviewer =
            m.teams.find(t => t.roleName === 'reviewer') &&
            m.teams.find(t => t.roleName === 'reviewer').users[0]
          const users = submitter ? [submitter] : []
          if (
            !m.teams.some(t => t.userId === m.updatedBy) &&
            reviewer &&
            reviewer.id !== submitter.id
          ) {
            users.push(reviewer)
          }
          if (users.length > 0) {
            const manInfo = {
              id: m.id,
              title: m['meta,title'],
            }
            logger.info(`Sending email for stalled error manuscript ${m.id}`)
            await Promise.all(
              users.map(async u => stalledErrorEmail(manInfo, u)),
            )
          } else {
            throw new Error('No users to email')
          }
        } catch (e) {
          logger.error(
            `Unable to send email for stalled error manuscript ${m.id}: ${e}`,
          )
          return true
        }
      }),
    )
    return true
  }
  logger.info('No stalled error manuscripts.')
  return true
}
