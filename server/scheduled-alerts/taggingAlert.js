const logger = require('@pubsweet/logger')
const Audit = require('../xpub-model/entities/audit/data-access')
const config = require('config')
const rfr = require('rfr')

const { userMessage } = rfr('server/email')
const { externalQA, system } = config['epmc-email']

;(async () => {
  const updateTime = Date.now()
  logger.info(`Counting tagged manuscripts`)
  await taggingAlert()
  logger.info(
    `External QA alert was completed in ${Date.now() - updateTime} ms`,
  )
  Audit.knex().destroy()
})()

async function taggingAlert() {
  try {
    const tagging = await Audit.query()
      .aliasFor(Audit, 'a')
      .where('a.objectType', 'manuscript')
      .whereRaw(
        "a.\"created\"::date = current_date - interval '1 day' and a.changes->>'status' = 'tagging'",
      )
      .joinRelation('manuscript')
      .whereNull('manuscript.deleted')
      .distinct('a.manuscriptId')
    logger.info(`${tagging.length} manuscripts sent to tagging yesterday.`)
    if (tagging.length > 0) {
      logger.info('Sending tagging count email.')
      await sendMessage(tagging)
      return true
    }
  } catch (e) {
    logger.error(`Unable to get or send tagging count: ${e}`)
  }
  return true
}

async function sendMessage(list) {
  const message = `${list.length} manuscript${
    list.length === 1 ? ' was' : 's were'
  } sent for tagging yesterday.\n
${list.map(m => m.manuscriptId).join('\n')}`
  const subject = `${list.length} submissions sent for tagging`
  await userMessage(externalQA, subject, message, null, null, system)
  return true
}
