module.exports = {
  transport: {
    host: 'outgoing.ebi.ac.uk',
    port: 587,
    requireTLS: true,
    auth: {
      user: process.env.EMAIL_USR,
      pass: process.env.EMAIL_PWD,
    },
  },
}
