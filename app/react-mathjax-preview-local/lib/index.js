/* global MathJax */
exports.__esModule = true
exports.default = undefined

let _class, _temp

const _react = require('react')

const _react2 = _interopRequireDefault(_react)

const _propTypes = require('prop-types')

const _propTypes2 = _interopRequireDefault(_propTypes)

const _loadScript = require('load-script')

const _loadScript2 = _interopRequireDefault(_loadScript)

const _dompurify = require('dompurify')

const _dompurify2 = _interopRequireDefault(_dompurify)

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj }
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function')
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called",
    )
  }
  return call && (typeof call === 'object' || typeof call === 'function')
    ? call
    : self
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== 'function' && superClass !== null) {
    throw new TypeError(
      `Super expression must either be null or a function, not ${typeof superClass}`,
    )
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  })
  if (superClass)
    Object.setPrototypeOf && Object.setPrototypeOf(subClass, superClass)
}

const SCRIPT =
  'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_HTMLorMML'

const _default = (((_class = (_Component => {
  _inherits(_default, _Component)

  function _default(props) {
    _classCallCheck(this, _default)

    const _this = _possibleConstructorReturn(this, _Component.call(this, props))

    _this.onLoad = err => {
      _this.setState({
        loaded: true,
      })
      if (err) return err
      MathJax.Hub.Config({
        showMathMenu: true,
        tex2jax: { inlineMath: [['$', '$'], ['\\(', '\\)']] },
      })
      window.MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, _this.preview])
    }

    _this.state = {
      loaded: false,
      oldMath: props.math,
    }
    return _this
  }

  _default.prototype.componentDidMount = function componentDidMount() {
    this.preview.innerHTML = _dompurify2.default.sanitize(this.props.math)
    this.state.loaded
      ? MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview])
      : (0, _loadScript2.default)(SCRIPT, this.onLoad)
  }

  _default.prototype.shouldComponentUpdate = function shouldComponentUpdate(
    nextProps,
    nextState,
  ) {
    if (!nextProps.math) return false
    return nextProps.math !== this.state.oldMath
  }

  _default.prototype.componentDidUpdate = function componentDidUpdate(
    prevProps,
    prevState,
  ) {
    this.preview.innerHTML = this.props.math
    MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview])
  }

  _default.prototype.componentWillReceiveProps = function componentWillReceiveProps(
    nextProps,
  ) {
    this.setState({ oldMath: nextProps.math })
  }

  _default.prototype.render = function render() {
    const _this2 = this

    return _react2.default.createElement(
      'span',
      {
        className: `${this.props.className} react-mathjax-preview`,
        style: this.props.style,
      },
      _react2.default.createElement('span', {
        className: 'react-mathjax-preview-result',
        ref: function ref(node) {
          _this2.preview = node
        },
      }),
    )
  }

  return _default
})(_react.Component)),
(_temp = _class)),
(_class.propTypes = {
  className: _propTypes2.default.string,
  style: _propTypes2.default.string,
  math: _propTypes2.default.string,
}),
(_class.defaultProps = {
  math: '',
}),
_temp)

exports.default = _default
module.exports = exports.default
