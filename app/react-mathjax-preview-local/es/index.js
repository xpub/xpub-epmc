/* global MathJax */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import loadScript from 'load-script'
import DOMPurify from 'dompurify'

let _class, _temp

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function')
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called",
    )
  }
  return call && (typeof call === 'object' || typeof call === 'function')
    ? call
    : self
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== 'function' && superClass !== null) {
    throw new TypeError(
      `Super expression must either be null or a function, not ${typeof superClass}`,
    )
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  })
  if (superClass)
    Object.setPrototypeOf && Object.setPrototypeOf(subClass, superClass)
}

const SCRIPT =
  'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_HTMLorMML'

const _default = (((_class = (_Component => {
  _inherits(_default, _Component)

  function _default(props) {
    _classCallCheck(this, _default)

    const _this = _possibleConstructorReturn(this, _Component.call(this, props))

    _this.onLoad = err => {
      _this.setState({
        loaded: true,
      })
      if (err) return err

      MathJax.Hub.Config({
        showMathMenu: true,
        tex2jax: { inlineMath: [['$', '$'], ['\\(', '\\)']] },
      })
      window.MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, _this.preview])
    }

    _this.state = {
      loaded: false,
      oldMath: props.math,
    }
    return _this
  }

  _default.prototype.componentDidMount = function componentDidMount() {
    this.preview.innerHTML = DOMPurify.sanitize(this.props.math)
    this.state.loaded
      ? MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview])
      : loadScript(SCRIPT, this.onLoad)
  }

  _default.prototype.shouldComponentUpdate = function shouldComponentUpdate(
    nextProps,
    nextState,
  ) {
    if (!nextProps.math) return false
    return nextProps.math !== this.state.oldMath
  }

  _default.prototype.componentDidUpdate = function componentDidUpdate(
    prevProps,
    prevState,
  ) {
    this.preview.innerHTML = this.props.math
    MathJax.Hub.Queue(['Typeset', window.MathJax.Hub, this.preview])
  }

  _default.prototype.componentWillReceiveProps = function componentWillReceiveProps(
    nextProps,
  ) {
    this.setState({ oldMath: nextProps.math })
  }

  _default.prototype.render = function render() {
    const _this2 = this

    return React.createElement(
      'span',
      {
        className: `${this.props.className} react-mathjax-preview`,
        style: this.props.style,
      },
      React.createElement('span', {
        className: 'react-mathjax-preview-result',
        ref: function ref(node) {
          _this2.preview = node
        },
      }),
    )
  }

  return _default
})(Component)),
(_temp = _class)),
(_class.propTypes = {
  className: PropTypes.string,
  style: PropTypes.string,
  math: PropTypes.string,
}),
(_class.defaultProps = {
  math: '',
}),
_temp)

export { _default as default }
