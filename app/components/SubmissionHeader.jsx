import React from 'react'
import styled from 'styled-components'
import { Action, Link, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import {
  HTMLString,
  FlexBar,
  LeftSide,
  RightSide,
  Loading,
  LoadingIcon,
  Notification,
} from './ui'
import Mailer from './mailer'
import { States } from './dashboard'

const ManuscriptHead = styled(FlexBar)`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  * {
    font-weight: 600;
  }
`
const Left = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`
const FlexDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
const Progress = styled.span`
  margin-left: calc(${th('gridUnit')} * 3);
  font-weight: normal;
  font-style: italic;
  color: ${th('colorTextPlaceholder')};
  &.saved {
    color: ${th('colorText')};
  }
`
class MailLink extends React.Component {
  state = { show: false }
  render() {
    return (
      <React.Fragment>
        <Action onClick={() => this.setState({ show: true })}>
          <Icon color="currentColor" size={2.5}>
            mail
          </Icon>
          Helpdesk
        </Action>
        {this.state.show && (
          <Mailer
            close={() => this.setState({ show: false })}
            currentUser={this.props.currentUser}
            manuscript={this.props.manuscript}
            recipients={['helpdesk']}
          />
        )}
      </React.Fragment>
    )
  }
}

let savedTimer
const addSaved = () => {
  document.getElementById('progress').classList.add('saved')
}
const removeSaved = () => {
  document.getElementById('progress').classList.remove('saved')
}

const SubmissionHeader = BaseComponent => ({
  currentUser,
  manuscript,
  children,
  saved,
  ...props
}) => {
  if (manuscript) {
    if (saved && document.getElementById('progress')) {
      addSaved()
    }
    if (savedTimer) {
      clearTimeout(savedTimer)
    }
    savedTimer = setTimeout(() => {
      if (document.getElementById('progress')) {
        removeSaved()
      }
    }, 2000)
    if (manuscript.deleted && !currentUser.admin) {
      props.history.push('/')
      return null
    }
    return (
      <React.Fragment>
        <ManuscriptHead>
          <LeftSide>
            <Left>
              {manuscript.id}: <HTMLString string={manuscript.meta.title} />
            </Left>
          </LeftSide>
          <RightSide>
            <FlexDiv>
              {currentUser.admin ? (
                <React.Fragment>
                  {window.location.pathname ===
                  `/submission/${manuscript.id}/activity` ? (
                    <React.Fragment>
                      {States.admin[manuscript.status].url !== 'activity' && (
                        <Link
                          to={`/submission/${manuscript.id}/${
                            States.admin[manuscript.status].url
                          }`}
                        >
                          <Icon color="currentColor" size={2.5}>
                            x-square
                          </Icon>
                          Exit activity
                        </Link>
                      )}
                    </React.Fragment>
                  ) : (
                    <Link to={`/submission/${manuscript.id}/activity`}>
                      <Icon color="currentColor" size={2.5}>
                        check-square
                      </Icon>
                      Activity
                    </Link>
                  )}
                </React.Fragment>
              ) : (
                <MailLink currentUser={currentUser} manuscript={manuscript} />
              )}
              <Progress className="saved" id="progress">
                Progress saved
              </Progress>
            </FlexDiv>
          </RightSide>
        </ManuscriptHead>
        {manuscript.deleted && (
          <Notification fullscreen type="error">
            This submission has been deleted.
          </Notification>
        )}
        <BaseComponent
          currentUser={currentUser}
          manuscript={manuscript}
          {...props}
        >
          {children}
        </BaseComponent>
      </React.Fragment>
    )
  }
  return (
    <Loading>
      <LoadingIcon />
    </Loading>
  )
}

export default SubmissionHeader
