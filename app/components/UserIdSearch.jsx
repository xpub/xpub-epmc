import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Link } from '@pubsweet/ui'
import { withApollo } from 'react-apollo'
import { SearchForm as Search, Notification } from './ui'
import { GET_USER_ID } from './operations'

const Result = styled.div`
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  background-color: ${th('colorBackgroundHue')};
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  & > * {
    margin: ${th('gridUnit')};
  }
  a {
    min-width: 310px;
  }
  button:first-child {
    margin-right: ${th('gridUnit')};
  }
  @media screen and (max-width: 733px) {
    span {
      display: block;
    }
  }
`
const SearchForm = styled(Search)`
  max-width: 400px;
`
class UserIdSearch extends React.Component {
  state = {
    query: '',
    result: null,
    error: null,
  }
  onSearch(searchForm, e) {
    e.preventDefault()
    this.setState({
      result: null,
      error: null,
    })
    const options = {
      query: GET_USER_ID,
      variables: { id: this.state.query },
    }
    this.props.client
      .query(options)
      .then(response => this.setState({ result: response.data.epmc_user }))
      .catch(e => {
        const error = e.graphQLErrors
          ? 'Please enter a valid existing ID'
          : 'Error occured. please try again'
        this.setState({ error })
      })
  }
  render() {
    const { result, query } = this.state
    const { name, email } = (result && result.identities[0]) || ''
    const { title, givenNames, surname } = name || ''
    return (
      <React.Fragment>
        {this.state.error && (
          <Notification type="error">{this.state.error}</Notification>
        )}
        {!result && (
          <SearchForm
            name="searchById"
            noButton="true"
            onChange={e => this.setState({ query: e.target.value.trim() })}
            onSubmit={e => this.onSearch('usersById', e)}
            placeholder="Enter account ID"
            value={query}
          />
        )}
        {result && (
          <Result>
            <Link to={`/manage-account/${result.id}`}>
              <span>{result.id}</span>
            </Link>
            <span>{`${title ? `${title} ` : ''}${givenNames} ${surname}`}</span>
            <span>{email}</span>
            <div>
              <Button onClick={() => this.props.success(result)} primary>
                {this.props.successLabel}
              </Button>
              <Button onClick={() => this.setState({ result: null })}>
                Cancel
              </Button>
            </div>
          </Result>
        )}
      </React.Fragment>
    )
  }
}
export default withApollo(UserIdSearch)
