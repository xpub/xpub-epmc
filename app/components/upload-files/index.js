export { default } from './UploadFiles'
export {
  SubmissionTypes,
  ManuscriptTypes,
  FileTypes,
  XMLTypes,
  ReviewTypes,
  AllTypes,
} from './uploadtypes'
