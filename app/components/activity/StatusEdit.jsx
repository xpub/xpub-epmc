import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H2, Button, Icon, RadioGroup } from '@pubsweet/ui'
import { states } from 'config'
import { Notification } from '../ui'
import SubmissionCancel from '../SubmissionCancel'
import { QUERY_ACTIVITY_INFO } from './operations'
import { Exit } from './MetaEdit'
import { EditIcon } from './MetaSec'

const FlexP = styled.p`
  display: flex;
  align-items: center;
  & > button {
    flex: 0 0 auto;
  }
  & > span {
    flex: 1 1 50%;
    margin-left: ${th('gridUnit')};
  }
`
const RouteButton = styled(Button)`
  align-items: center;
  display: inline-flex;
`
const StatusDesc = [
  'not yet submitted',
  'not yet submitted',
  'submission error',
  'needs reviewer approval',
  'needs submission QA',
  'tagging',
  'needs XML QA',
  'has XML errors',
  'needs final review',
  'needs citation',
  'approved for Europe PMC',
  'failed NCBI loading',
  'available in Europe PMC',
  'being withdrawn',
]

const DeleteButton = ({ deleteMan }) => (
  <Action onClick={() => deleteMan()}>Confirm deletion</Action>
)

const RecoverButton = ({ callback, recoverMan }) => (
  <RouteButton onClick={() => recoverMan(callback())}>
    <Icon color="currentColor" size={2.5}>
      refresh-cw
    </Icon>
    Recover manuscript
  </RouteButton>
)

class StatusEdit extends React.Component {
  state = {
    edit: !this.props.lastStatus,
    selected: this.props.lastStatus,
  }
  render() {
    const { manuscript, lastStatus, setStatus, close } = this.props
    const { edit, selected } = this.state
    const done = states.indexOf('ncbi-ready')
    const curr = states.indexOf(manuscript.status)
    const options = states.map((s, i) => ({
      value: s,
      label: `"${s}" – ${StatusDesc[i]}${
        s === lastStatus ? ' [most recent]' : ''
      }${s === manuscript.status ? ' [current]' : ''}`,
      disabled: s === manuscript.status,
    }))
    return (
      <React.Fragment>
        <H2>Send/Remove</H2>
        {manuscript.deleted ? (
          <React.Fragment>
            <Notification type="info">
              {`Manuscript succesfully deleted. If not already sent, please send a message to the user(s) explaining the deletion.`}
            </Notification>
            <FlexP>
              <SubmissionCancel
                callback={close}
                manuscriptId={manuscript.id}
                refetch={[
                  {
                    query: QUERY_ACTIVITY_INFO,
                    variables: { id: manuscript.id },
                  },
                ]}
                showComponent={RecoverButton}
              />
            </FlexP>
          </React.Fragment>
        ) : (
          <React.Fragment>
            {manuscript.status === 'submission-error' && (
              <FlexP>
                <RouteButton
                  onClick={() => setStatus(lastStatus || 'submitted')}
                >
                  <Icon color="currentColor" size={2.5}>
                    check-circle
                  </Icon>
                  Cancel submission error
                </RouteButton>
                <span>
                  Cancel submission error request and send back to
                  {(lastStatus &&
                    lastStatus === 'xml-triage' &&
                    ` XML errors state ('xml-triage')`) ||
                    (lastStatus &&
                      lastStatus === 'in-review' &&
                      ` Initial review state (in-review)`) ||
                    ` QA state ('submitted')`}
                </span>
              </FlexP>
            )}
            <React.Fragment>
              <FlexP>
                <RouteButton
                  onClick={() => {
                    setStatus(selected)
                    close()
                  }}
                >
                  <Icon color="currentColor" size={2.5}>
                    send
                  </Icon>
                  Change status
                </RouteButton>
                <span>
                  send{selected === lastStatus && ' back'} to <b>{selected}</b>
                  <Action onClick={() => this.setState({ edit: true })}>
                    <EditIcon />
                  </Action>
                  <em style={{ marginLeft: '8px' }}>
                    {selected === lastStatus && '(most recent status)'}
                  </em>
                  <br />
                  Please note that no emails will be automatically sent!
                </span>
              </FlexP>
              {edit && (
                <RadioGroup
                  name="Statuses"
                  onChange={v => this.setState({ selected: v })}
                  options={options}
                  value={selected}
                />
              )}
              <FlexP>
                <RouteButton
                  disabled={curr >= done}
                  onClick={e =>
                    e.currentTarget.nextElementSibling.classList.remove(
                      'hidden',
                    )
                  }
                >
                  <Icon color="currentColor" size={2.5}>
                    trash-2
                  </Icon>
                  Delete manuscript
                </RouteButton>
                <span className="hidden">
                  {` Are you certain? `}
                  <SubmissionCancel
                    manuscriptId={manuscript.id}
                    refetch={[
                      {
                        query: QUERY_ACTIVITY_INFO,
                        variables: { id: manuscript.id },
                      },
                    ]}
                    showComponent={DeleteButton}
                  />
                </span>
                {curr >= done && (
                  <span>
                    {` Manuscript has been sent to PMC and must be withdrawn.`}
                  </span>
                )}
              </FlexP>
            </React.Fragment>
          </React.Fragment>
        )}
        <Exit close={close} />
      </React.Fragment>
    )
  }
}

export default StatusEdit
