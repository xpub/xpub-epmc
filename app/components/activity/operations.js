import gql from 'graphql-tag'
import { ManuscriptFragment } from '../operations'

export const GET_USER = gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
      identities {
        ... on Local {
          name {
            givenNames
            surname
          }
        }
      }
    }
  }
`

export const GET_JOURNAL = gql`
  query($id: ID!) {
    journal(id: $id) {
      id
      journalTitle
    }
  }
`

export const UPLOAD_CITATION = gql`
  mutation UploadCitation($manuscriptId: ID!) {
    uploadCitation(manuscriptId: $manuscriptId)
  }
`

export const DELETE_CITATION = gql`
  mutation DeleteCitation($manuscriptId: ID!) {
    deleteCitation(manuscriptId: $manuscriptId)
  }
`

export const LINK_AND_DELETE = gql`
  mutation LinkAndDelete($id: ID!) {
    linkDeleteManuscript(id: $id) {
      success
      message
    }
  }
`

export const INVITE_REVIEWER = gql`
  mutation SetNewReviewer($id: ID!) {
    setManuscriptReviewer(id: $id)
  }
`

export const QUERY_ACTIVITY_INFO = gql`
  query QueryActivitiesByManuscriptId($id: ID!) {
    activities: epmc_queryActivitiesByManuscriptId(id: $id) {
      ...ManuscriptFragment
      deleted
      meta {
        volume
        issue
        location {
          fpage
          lpage
          elocationId
        }
        citerefUrl
        fulltextUrl
      }
      claiming {
        id
        givenNames
        surname
      }
      audits {
        id
        created
        updated
        user {
          id
          title
          givenNames
          surname
          identities {
            email
          }
          deleted
        }
        action
        originalData
        changes
        objectType
        objectId
        manuscriptId
      }
    }
  }
  ${ManuscriptFragment}
`
