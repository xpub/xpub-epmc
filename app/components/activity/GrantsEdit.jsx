import React from 'react'
import { Mutation } from 'react-apollo'
import { Button } from '@pubsweet/ui'
import { Buttons } from '../ui'
import GrantSearch from '../GrantSearch'
import { ALERT_PIS } from '../operations'

class GrantsEdit extends React.Component {
  state = { originalData: null }
  componentDidMount() {
    this.setState({ originalData: this.props.fundingGroup })
  }
  render() {
    const { close, updateGrants, fundingGroup, manuscriptId } = this.props
    return (
      <React.Fragment>
        <GrantSearch
          changedGrants={updateGrants}
          hideEmbargo
          selectedGrants={fundingGroup}
        />
        <div style={{ height: '200px' }} />
        <Mutation mutation={ALERT_PIS}>
          {(epmc_emailGrantPis, { data }) => {
            const sendAlert = async () => {
              const { originalData } = this.state
              const newGrants = fundingGroup.filter(
                p =>
                  !originalData.some(
                    o =>
                      o.awardId === p.awardId &&
                      o.fundingSource === p.fundingSource &&
                      o.pi.surname === p.pi.surname,
                  ),
              )
              await epmc_emailGrantPis({
                variables: { manuscriptId, newGrants },
              })
              close()
            }
            return (
              <Buttons right>
                <Button onClick={() => sendAlert()} primary>
                  Save &amp; send PI emails
                </Button>
              </Buttons>
            )
          }}
        </Mutation>
      </React.Fragment>
    )
  }
}

export default GrantsEdit
