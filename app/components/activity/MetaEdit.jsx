import React from 'react'
import { omit } from 'lodash'
import { withTheme } from 'styled-components'
import { Button } from '@pubsweet/ui'
import { Portal, Buttons, CloseModal } from '../ui'
import { ManuscriptMutations, NoteMutations } from '../SubmissionMutations'
import ResolveDuplicates from '../ResolveDuplicates'
import ReviewerEdit from './ReviewerEdit'
import CitationEdit from './CitationEdit'
import GrantsEdit from './GrantsEdit'
import EmbargoEdit from './EmbargoEdit'
import StatusEdit from './StatusEdit'
import { QUERY_ACTIVITY_INFO } from './operations'

export const Exit = ({ close }) => (
  <Buttons right>
    <Button onClick={close}>Exit</Button>
  </Buttons>
)

const DuplicatesWithMutations = NoteMutations(ResolveDuplicates)
const ReviewerWithMutations = NoteMutations(ReviewerEdit)

const MetaEdit = withTheme(
  ({
    theme,
    close,
    currentUser,
    duplicates,
    lastStatus,
    linkAndDelete,
    manuscript,
    toEdit,
    ...props
  }) => {
    const { meta } = manuscript
    const { fundingGroup: grants, releaseDelay, notes } = meta
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []

    if (toEdit === 'dupes' && duplicates) {
      return (
        <DuplicatesWithMutations
          close={close}
          duplicates={duplicates}
          manuscript={manuscript}
          note={notes ? notes.find(n => n.notesType === 'notDuplicates') : null}
          refetch={[
            {
              query: QUERY_ACTIVITY_INFO,
              variables: { id: manuscript.id },
            },
          ]}
        />
      )
    }
    return (
      <Portal style={{ backgroundColor: theme.colorBackground }} transparent>
        <CloseModal onClick={close} />
        <React.Fragment>
          {(() => {
            switch (toEdit) {
              case 'grants':
                return (
                  <GrantsEdit
                    close={close}
                    fundingGroup={fundingGroup}
                    manuscriptId={manuscript.id}
                    updateGrants={props.updateGrants}
                  />
                )
              case 'embargo':
                return (
                  <EmbargoEdit
                    change={v => props.updateEmbargo(v)}
                    close={close}
                    embargo={releaseDelay}
                  />
                )
              case 'reviewer':
                return (
                  <ReviewerWithMutations
                    close={close}
                    currentUser={currentUser}
                    manuscript={manuscript}
                    refetch={[
                      {
                        query: QUERY_ACTIVITY_INFO,
                        variables: { id: manuscript.id },
                      },
                    ]}
                  />
                )
              case 'citation':
                return (
                  <CitationEdit
                    change={async (e, d) => {
                      await props.changeCitation(e)
                      if (manuscript.status === 'xml-complete') {
                        const {
                          articleIds,
                          volume,
                          location,
                          publicationDates,
                        } = e.meta
                        if (
                          volume &&
                          ((articleIds &&
                            articleIds.find(aid => aid.pubIdType === 'pmid')) ||
                            (location &&
                              (location.fpage || location.elocationId) &&
                              publicationDates.length > 0))
                        ) {
                          props.setStatus('ncbi-ready')
                        }
                      }
                      if (d) {
                        await linkAndDelete(d)
                      }
                    }}
                    close={close}
                    manuscript={manuscript}
                  />
                )
              case 'status':
                return (
                  <StatusEdit
                    close={close}
                    lastStatus={lastStatus}
                    manuscript={manuscript}
                    setStatus={props.setStatus}
                  />
                )
              default:
                return null
            }
          })()}
        </React.Fragment>
      </Portal>
    )
  },
)

export default ManuscriptMutations(MetaEdit)
