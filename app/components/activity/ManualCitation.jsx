import moment from 'moment'
import { omit } from 'lodash'
import { withFormik } from 'formik'
import { graphql, compose } from 'react-apollo'
import * as yup from 'yup'
import CitationForm from './CitationForm'
import { UPLOAD_CITATION, QUERY_ACTIVITY_INFO } from './operations'

const handleSubmit = async (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) => {
  const { meta: prevMeta, citationData, uploadCitation } = props
  const articleIds = prevMeta.articleIds
    ? prevMeta.articleIds.map(aid => omit(aid, '__typename'))
    : []
  const meta = {
    title: values.title || null,
    volume: values.volume || null,
    issue: values.issue || null,
    location: {
      fpage: values.fpage || null,
      lpage: values.lpage || null,
      elocationId: values.elocationId || null,
    },
    publicationDates: [],
    citerefUrl: values.citeref || null,
    fulltextUrl: values.fulltext || null,
  }
  if (values.printyear) {
    const ppub = {
      type: 'ppub',
      jatsDate: {
        year: values.printyear,
      },
    }
    if (values.printseason) {
      ppub.jatsDate.season = values.printseason
      ppub.date = moment.utc(values.printyear, 'YYYY')
    } else {
      ppub.jatsDate.month = values.printmonth
      ppub.jatsDate.day = values.printday
      ppub.date = moment.utc(
        `${values.printyear} ${values.printmonth} ${values.printday}`,
        'YYYY MM DD',
      )
    }
    meta.publicationDates.push(ppub)
  }
  if (values.electronicyear) {
    const epub = {
      type: 'epub',
      jatsDate: {
        year: values.electronicyear,
      },
    }
    if (values.electronicseason) {
      epub.jatsDate.season = values.electronicseason
      epub.date = moment.utc(values.electronicyear, 'YYYY')
    } else {
      epub.jatsDate.month = values.electronicmonth
      epub.jatsDate.day = values.electronicday
      epub.date = moment.utc(
        `${values.electronicyear} ${values.electronicmonth} ${
          values.electronicday
        }`,
        'YYYY MM DD',
      )
    }
    meta.publicationDates.push(epub)
  }
  meta.articleIds = articleIds.filter(aid => aid.pubIdType !== 'doi')
  if (values.doi) {
    meta.articleIds.push({
      pubIdType: 'doi',
      id: values.doi,
    })
  }
  await citationData({ meta })
  await uploadCitation({ variables: { manuscriptId: props.id } })
  resetForm()
  props.close()
}

const enhancedFormik = withFormik({
  initialValues: {
    title: '',
    volume: '',
    issue: '',
    fpage: '',
    lpage: '',
    elocationId: '',
    doi: '',
    printyear: '',
    printmonth: '',
    printday: '',
    printseason: '',
    electronicyear: '',
    electronicmonth: '',
    electronicday: '',
    electronicseason: '',
    citeref: '',
    fulltext: '',
  },
  mapPropsToValues: props => {
    const { meta } = props
    const {
      articleIds,
      volume,
      issue,
      location,
      publicationDates,
      title,
      citerefUrl,
      fulltextUrl,
    } = meta
    const doi =
      (articleIds && articleIds.find(aid => aid.pubIdType === 'doi')) || ''
    const ppub =
      publicationDates && publicationDates.find(d => d.type === 'ppub')
    const epub =
      publicationDates && publicationDates.find(d => d.type === 'epub')
    const p = (ppub && ppub.jatsDate) || null
    const e = (epub && epub.jatsDate) || null
    return {
      title,
      volume: volume || '',
      issue: issue || '',
      fpage: (location && location.fpage) || '',
      lpage: (location && location.lpage) || '',
      elocationId: (location && location.elocationId) || '',
      doi: doi.id || '',
      printyear: (p && p.year) || '',
      printmonth: (p && p.month) || '',
      printday: (p && p.day) || '',
      printseason: (p && p.season) || '',
      electronicyear: (e && e.year) || '',
      electronicmonth: (e && e.month) || '',
      electronicday: (e && e.day) || '',
      electronicseason: (e && e.season) || '',
      citeref: citerefUrl || '',
      fulltext: fulltextUrl || '',
    }
  },
  validationSchema: props => {
    let { firstYear, endYear } =
      props && props.journal ? props.journal.meta : {}
    firstYear = firstYear || '2000'

    let endYearErrorMessage
    if (endYear) {
      endYearErrorMessage = `Invalid year, journal end year is ${endYear}`
    } else {
      endYear = new Date().getFullYear() + 10
      endYearErrorMessage =
        'Invalid year, journal year is beyond the expected range'
    }

    return yup.object().shape({
      title: yup.string().required('Title is required'),
      volume: yup.string(),
      issue: yup.string(),
      fpage: yup.string(),
      lpage: yup.string(),
      elocationId: yup.string(),
      doi: yup.string(),
      printyear: yup
        .number()
        .min(firstYear, `Invalid year, journal first year is ${firstYear}`)
        .max(endYear, endYearErrorMessage)
        .typeError('Invalid year'),
      printmonth: yup
        .number('')
        .integer()
        .min(1, 'Invalid month')
        .max(12, 'Invalid month')
        .typeError('Must be a number'),
      printday: yup
        .number()
        .integer()
        .min(1, 'Invalid day')
        .max(31, 'Invalid day')
        .typeError('Must be a number'),
      printseason: yup.string(),
      electronicyear: yup
        .number()
        .min(firstYear, `Invalid year, journal first year is ${firstYear}`)
        .max(endYear, endYearErrorMessage)
        .typeError('Invalid year'),
      electronicmonth: yup
        .number()
        .integer()
        .min(1, 'Invalid month')
        .max(12, 'Invalid month')
        .typeError('Must be a number'),
      electronicday: yup
        .number()
        .integer()
        .min(1, 'Invalid day')
        .max(31, 'Invalid day')
        .typeError('Must be a number'),
      electronicseason: yup.string(),
      citeref: yup.string().url('Invalid URL'),
      fulltext: yup.string().url('Invalid URL'),
    })
  },
  displayName: 'manual-citation',
  handleSubmit,
})(CitationForm)

export default compose(
  graphql(UPLOAD_CITATION, {
    name: 'uploadCitation',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.id } },
      ],
    }),
  }),
)(enhancedFormik)
