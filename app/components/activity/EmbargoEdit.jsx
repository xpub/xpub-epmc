import React from 'react'
import { Button, TextField } from '@pubsweet/ui'
import { Buttons, Notification } from '../ui'
import { Exit } from './MetaEdit'

class EmbargoEdit extends React.Component {
  state = {
    embargo: this.props.embargo,
    error: false,
  }
  render() {
    return (
      <React.Fragment>
        <TextField
          label="Enter embargo in months"
          name="embargo"
          onChange={e => this.setState({ embargo: e.target.value })}
          value={this.state.embargo}
        />
        {this.state.error && (
          <Notification type="error">
            Input must be a whole number, in digits
          </Notification>
        )}
        <Buttons right>
          <Button
            onClick={() => {
              if (this.state.embargo % 1 !== 0) {
                this.setState({ error: true })
              } else {
                this.props.change(this.state.embargo)
                this.props.close()
              }
            }}
            primary
          >
            Save
          </Button>
          <Exit close={this.props.close} />
        </Buttons>
      </React.Fragment>
    )
  }
}

export default EmbargoEdit
