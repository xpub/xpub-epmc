import React from 'react'
import { Query, compose, graphql } from 'react-apollo'
import { Icon, Action, Button, H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import moment from 'moment'
import { Notification } from '../ui'
import {
  CHECK_DUPES,
  CLAIM_MANUSCRIPT,
  UNCLAIM_MANUSCRIPT,
} from '../operations'
import { UserContext } from '../App'
import MetaEdit from './MetaEdit'
import { QUERY_ACTIVITY_INFO, LINK_AND_DELETE } from './operations'

const Heading = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  margin: calc(${th('gridUnit')} * 3) 0px calc(${th('gridUnit')} * 2);
  padding-bottom: ${th('gridUnit')};
  h2 {
    display: inline-block;
    margin: 0;
  }
  dl {
    display: inline-block;
    margin-left: calc(${th('gridUnit')} * 2);
  }
`
const ClaimAction = styled(Button)`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('fontSize')};
  padding: 0 calc(${th('gridUnit')} * 1.5);
  margin-left: calc(${th('gridUnit')} * 2);
  margin-bottom: 0;
  vertical-align: text-bottom;
  min-width: 0;
`
const Container = styled.div`
  margin: 0 0 calc(${th('gridUnit')} * 2);
  column-count: 3;
  @media screen and (max-width: 1000px) {
    column-count: 2;
  }
  @media screen and (max-width: 600px) {
    column-count: 1;
  }
`
const DL = styled.dl`
  margin: 0 0 ${th('gridUnit')};
  dt {
    font-weight: 600;
    margin-right: ${th('gridUnit')};
    display: inline;
  }
  dd {
    margin: 0;
    display: inline;
    button {
      margin-left: calc(${th('gridUnit')} / 2);
    }
  }
`
const DupeButton = styled(Button)`
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  font-size: ${th('fontSizeBaseSmall')};
`
export const EditIcon = () => (
  <Icon color="currentColor" size={1.9} style={{ verticalAlign: 'bottom' }}>
    edit-3
  </Icon>
)

class MetaSec extends React.Component {
  state = { edit: null, notif: null }
  componentDidMount() {
    this._mounted = true
    this.notifTimer = null
  }
  async componentDidUpdate() {
    const { notif } = this.state
    if (this.notifTimer) {
      clearTimeout(this.notifTimer)
    }
    if (notif && notif.type !== 'error') {
      this.notifTimer = setTimeout(() => {
        if (this._mounted) {
          this.setState({ notif: null })
        }
      }, 3000)
    }
  }
  componentWillUnmount() {
    this._mounted = false
    clearTimeout(this.notifTimer)
  }
  static contextType = UserContext
  render() {
    const currentUser = this.context
    const {
      manuscript,
      claimManuscript,
      unclaimManuscript,
      linkDeleteManuscript,
    } = this.props
    const { teams, audits, meta, journal, claiming } = manuscript
    const {
      fundingGroup,
      releaseDelay,
      articleIds,
      volume,
      issue,
      location,
      publicationDates,
      title,
      notes,
    } = meta
    let duplicates = []
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    const submitter = teams.find(team => team.role === 'submitter')
    const { title: st, givenNames: sg, surname: ss } = submitter
      ? submitter.teamMembers[0].alias.name
      : ''
    const reviewer = teams.find(team => team.role === 'reviewer')
    const { title: rt, givenNames: rg, surname: rs } = reviewer
      ? reviewer.teamMembers[0].alias.name
      : ''
    const reviewerNote =
      notes &&
      notes.find(n => n.notesType === 'selectedReviewer') &&
      JSON.parse(notes.find(n => n.notesType === 'selectedReviewer').content)
    const rnName = reviewerNote && reviewerNote.name
    const auditInfo = audits.reduce((info, audit) => {
      if (!info.timeInProcess) {
        info.timeInProcess = moment(audit.created).fromNow(true)
      }
      info.lastActivity = moment(audit.created).fromNow(true)
      return info
    }, {})
    const lastStatusChange = audits.reduce((log, current) => {
      if (Object.keys(current.changes).includes('status')) {
        return current
      }
      return log
    }, {})
    const year =
      publicationDates &&
      publicationDates.reduce((retYear, date) => {
        const getYear = date.jatsDate
          ? date.jatsDate.year
          : moment(date.date).year()
        if (date.type === 'ppub' || !retYear) {
          return getYear
        }
        return retYear
      }, null)
    const { edit, notif } = this.state
    return (
      <React.Fragment>
        {notif && (
          <React.Fragment>
            <br />
            <Notification type={notif.type}>{notif.message}</Notification>
          </React.Fragment>
        )}
        <DL style={{ float: 'right', marginTop: '1rem' }}>
          <dt>Status:</dt>
          <dd>
            {manuscript.deleted ? (
              <del>{manuscript.status}</del>
            ) : (
              manuscript.status
            )}
            <Action onClick={() => this.setState({ edit: 'status' })}>
              <Icon color="currentColor" size={1.75}>
                send
              </Icon>
            </Action>
          </dd>
        </DL>
        <Heading>
          <H2>Quick view</H2>
          {claiming ? (
            <DL>
              <dt>Claimed:</dt>
              <dd>
                {claiming.givenNames}
                {currentUser.id === claiming.id && (
                  <Action
                    onClick={async () =>
                      unclaimManuscript({
                        variables: { id: manuscript.id },
                      })
                    }
                  >
                    <Icon color="currentColor" size={1.9}>
                      x
                    </Icon>
                  </Action>
                )}
              </dd>
            </DL>
          ) : (
            <ClaimAction
              onClick={async () =>
                claimManuscript({
                  variables: { id: manuscript.id },
                })
              }
            >
              Claim
            </ClaimAction>
          )}
        </Heading>
        <Container>
          {reviewerNote ? (
            <React.Fragment>
              <DL>
                <dt>Submitter:</dt>
                <dd>{`${st ? `${st} ` : ''}${sg} ${ss}`}</dd>
              </DL>
              {rnName && (
                <DL>
                  <dt>Invited reviewer:</dt>
                  <dd>
                    {`${rnName.givenNames} ${rnName.surname}`}
                    <Action onClick={() => this.setState({ edit: 'reviewer' })}>
                      <EditIcon />
                    </Action>
                  </dd>
                </DL>
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {reviewer &&
              reviewer.teamMembers[0].user.id ===
                submitter.teamMembers[0].user.id ? (
                <DL>
                  <dt>Submitter/Reviewer:</dt>
                  <dd>
                    {`${st ? `${st} ` : ''}${sg} ${ss}`}
                    <Action onClick={() => this.setState({ edit: 'reviewer' })}>
                      <EditIcon />
                    </Action>
                  </dd>
                </DL>
              ) : (
                <React.Fragment>
                  <DL>
                    <dt>Submitter:</dt>
                    <dd>{`${st ? `${st} ` : ''}${sg} ${ss}`}</dd>
                  </DL>
                  {reviewer && (
                    <DL>
                      <dt>Reviewer:</dt>
                      <dd>
                        {`${rt ? `${rt} ` : ''}${rg} ${rs}`}
                        <Action
                          onClick={() => this.setState({ edit: 'reviewer' })}
                        >
                          <EditIcon />
                        </Action>
                      </dd>
                    </DL>
                  )}
                </React.Fragment>
              )}
            </React.Fragment>
          )}
          <DL>
            <dt>Time in process:</dt>
            <dd>{auditInfo.timeInProcess || 'N/A'}</dd>
          </DL>
          <DL>
            <dt>Last activity:</dt>
            <dd>{`${auditInfo.lastActivity} ago` || 'N/A'}</dd>
          </DL>
          {fundingGroup && (
            <DL>
              <dt>Grants:</dt>
              <dd>
                {fundingGroup.map((f, t) => (
                  <span key={f.awardId}>
                    {`${f.fundingSource} ${f.awardId}`}
                    {t !== fundingGroup.length - 1 && ', '}
                  </span>
                ))}
                <Action onClick={() => this.setState({ edit: 'grants' })}>
                  <EditIcon />
                </Action>
              </dd>
            </DL>
          )}
          {releaseDelay && (
            <DL>
              <dt>Embargo:</dt>
              <dd>
                {releaseDelay} month{releaseDelay !== '1' && 's'}
                <Action onClick={() => this.setState({ edit: 'embargo' })}>
                  <EditIcon />
                </Action>
              </dd>
            </DL>
          )}
          <DL>
            <dt>Citation:</dt>
            <dd>
              {(journal && journal.meta.nlmta) || 'Unmatched'}
              {year && `. ${year}`}
              {volume && `, ${volume}`}
              {issue && `(${issue})`}
              {location && location.fpage ? (
                <React.Fragment>
                  {`:${location.fpage}`}
                  {location.lpage && `-${location.lpage}`}.
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {location &&
                    location.elocationId &&
                    `:${location.elocationId}.`}
                </React.Fragment>
              )}
              {pmid && ` PMID: ${pmid.id}.`}
              {pmcid && ` PMCID: ${pmcid.id}.`}
              <Action onClick={() => this.setState({ edit: 'citation' })}>
                <EditIcon />
              </Action>
            </dd>
          </DL>
          <Query
            fetchPolicy="cache-and-network"
            query={CHECK_DUPES}
            variables={{
              id: manuscript.id,
              articleIds: articleIds ? articleIds.map(aid => aid.id) : null,
              title,
            }}
          >
            {({ data, loading }) => {
              if (loading || !data || !data.checkDuplicates) {
                return '...'
              }
              const { manuscripts } = data.checkDuplicates
              const dupeNote = notes
                ? notes.find(n => n.notesType === 'notDuplicates')
                : null
              const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
              duplicates = manuscripts.filter(m => !notDupes.includes(m.id))
              return (
                <React.Fragment>
                  {duplicates && duplicates.length > 0 && (
                    <DupeButton
                      onClick={() => this.setState({ edit: 'dupes' })}
                      primary
                    >
                      Resolve duplicates
                    </DupeButton>
                  )}
                  {edit && (
                    <MetaEdit
                      close={() => this.setState({ edit: null })}
                      currentUser={currentUser}
                      duplicates={duplicates}
                      lastStatus={
                        lastStatusChange &&
                        lastStatusChange.originalData &&
                        lastStatusChange.originalData.status
                      }
                      linkAndDelete={async v => {
                        const { data } = await linkDeleteManuscript({
                          variables: { id: manuscript.id },
                        })
                        const result = data.linkDeleteManuscript
                        this.setState({
                          notif: {
                            type: result.success ? 'success' : 'error',
                            message: result.message,
                          },
                        })
                      }}
                      manuscript={manuscript}
                      refetch={[
                        {
                          query: QUERY_ACTIVITY_INFO,
                          variables: { id: manuscript.id },
                        },
                      ]}
                      toEdit={edit}
                    />
                  )}
                </React.Fragment>
              )
            }}
          </Query>
        </Container>
      </React.Fragment>
    )
  }
}

export default compose(
  graphql(LINK_AND_DELETE, {
    name: 'linkDeleteManuscript',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
  graphql(CLAIM_MANUSCRIPT, {
    name: 'claimManuscript',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
  graphql(UNCLAIM_MANUSCRIPT, {
    name: 'unclaimManuscript',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
)(MetaSec)
