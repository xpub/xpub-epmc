import gql from 'graphql-tag'
import { ClaimFragment } from '../operations'

export const CURRENT_USER = gql`
  query($email: String) {
    epmc_currentUser(email: $email) {
      id
    }
  }
`
export const ADD_REVIEWER = gql`
  mutation AddReviewer($noteId: ID!) {
    addReviewer(noteId: $noteId) {
      success
      message
    }
  }
`
export const COUNT_MANUSCRIPTS = gql`
  query CountByStatus {
    countByStatus {
      type
      count
    }
  }
`
export const ALERT_MANUSCRIPTS = gql`
  query CheckAge($states: [String], $interval: String) {
    checkAge(states: $states, interval: $interval) {
      alert
    }
  }
`
export const GET_MANUSCRIPT = gql`
  query SearchArticleIds($id: String!) {
    searchArticleIds(id: $id) {
      manuscript {
        id
        status
      }
      errors {
        message
      }
    }
  }
`

const DashboardFragment = gql`
  fragment DashboardManuscript on Manuscript {
    id
    status
    created
    updated
    deleted
    formState
    journal {
      id
      meta {
        nlmta
      }
    }
    meta {
      title
      articleIds {
        pubIdType
        id
      }
      publicationDates {
        type
        date
      }
      fundingGroup {
        fundingSource
        awardId
        title
        pi {
          surname
          givenNames
          title
          email
        }
      }
      releaseDelay
      unmatchedJournal
      notes {
        id
        notesType
        content
      }
    }
    teams {
      role
      teamMembers {
        user {
          id
        }
        alias {
          name {
            title
            surname
            givenNames
          }
        }
      }
    }
  }
`

export const USER_MANUSCRIPTS = gql`
  query LoadManuscripts {
    manuscripts {
      ...DashboardManuscript
    }
  }
  ${DashboardFragment}
`

export const ADMIN_MANUSCRIPTS = gql`
  query LoadAdminManuscripts($external: Boolean) {
    adminManuscripts(external: $external) {
      total
      manuscripts {
        ...DashboardManuscript
        ...ClaimFragment
      }
    }
  }
  ${ClaimFragment}
  ${DashboardFragment}
`

export const MANUSCRIPTS_BY_STATUS = gql`
  query FindManuscriptsByStatus($query: String!, $page: Int, $pageSize: Int) {
    findByStatus(query: $query, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
        ...ClaimFragment
      }
    }
  }
  ${ClaimFragment}
  ${DashboardFragment}
`

export const MANUSCRIPTS_BY_EMAIL_OR_LASTNAME = gql`
  query MANUSCRIPTS_BY_EMAIL_OR_LASTNAME(
    $query: String!
    $page: Int
    $pageSize: Int
  ) {
    searchManuscripts(query: $query, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
        ...ClaimFragment
      }
    }
  }
  ${ClaimFragment}
  ${DashboardFragment}
`

export const METRICS = gql`
  query METRICS {
    getMetrics {
      id
      created
      updated
      display_mth
      submitted
      xml_review
      xml_review_within_10_days
      xml_review_within_10_days_perc
      xml_review_within_3_days
      xml_review_within_3_days_perc
      published
      ncbi_ready_median
      external_qa
      xml_tagging
    }
  }
`
