import React from 'react'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2 as HTwo } from '@pubsweet/ui'
import { Loading, LoadingIcon, Notification } from '../ui'
import { ADMIN_MANUSCRIPTS } from './operations'
import DashboardList from './DashboardList'

const H2 = styled(HTwo)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
`

const MyQueue = ({ currentUser, history, errors, setErrors }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={ADMIN_MANUSCRIPTS}
    variables={{ external: !!currentUser.external }}
  >
    {({ data, loading }) => {
      if (loading || !data) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const mine = []
      const claimed = []
      const unclaimed = []
      const { manuscripts } = data.adminManuscripts
      if (manuscripts.length === 0) {
        if (currentUser.admin) {
          history.push('/admin-dashboard')
        } else {
          return (
            <Notification type="info">
              No submissions currently need QA.
            </Notification>
          )
        }
      }
      manuscripts.forEach(m => {
        if (m.claiming) {
          if (m.claiming.id === currentUser.id) {
            mine.push(m)
          } else {
            claimed.push(m)
          }
        } else {
          unclaimed.push(m)
        }
      })
      return (
        <React.Fragment>
          {errors.map(e => (
            <Notification key="message" type="warning">
              {e.message}
            </Notification>
          ))}
          {mine.length > 0 && (
            <React.Fragment>
              <H2>My claimed manuscripts ({mine.length})</H2>
              <DashboardList
                currentUser={currentUser}
                errors={setErrors}
                listData={mine}
                sectionRole="admin"
              />
            </React.Fragment>
          )}
          {unclaimed.length > 0 && (
            <React.Fragment>
              <H2>Unclaimed ({unclaimed.length})</H2>
              <DashboardList
                currentUser={currentUser}
                errors={setErrors}
                listData={unclaimed}
                sectionRole="admin"
              />
            </React.Fragment>
          )}
          {claimed.length > 0 && (
            <React.Fragment>
              <H2>In process with another user ({claimed.length})</H2>
              <DashboardList
                currentUser={currentUser}
                errors={setErrors}
                listData={claimed}
                sectionColor="warning"
                sectionRole="admin"
              />
            </React.Fragment>
          )}
        </React.Fragment>
      )
    }}
  </Query>
)

export default withRouter(MyQueue)
