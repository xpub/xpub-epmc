import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link, H3, Icon } from '@pubsweet/ui'
import { Loading, LoadingIcon, Table, Notification } from '../ui'
import { COUNT_MANUSCRIPTS, ALERT_MANUSCRIPTS } from './operations'
import DashboardBase from './DashboardBase'

const ListTable = styled(Table)`
  width: 100%;
  margin-bottom: calc(${th('gridUnit')} * 3);
  tr > *:first-child {
    width: calc(${th('gridUnit')} * 10);
    text-align: right;
  }
`
const Alert = styled.small`
  display: inline-flex;
  align-items: center;
  color: ${th('colorError')};
  margin-left: calc(${th('gridUnit')} * 5);
`
const HelpdeskQueue = {
  'needs submission QA': ['submitted'],
  'needs XML QA': ['xml-qa'],
  'has XML errors': ['xml-triage'],
  'needs citation': ['xml-complete'],
  'failed NCBI loading': ['ncbi-triage'],
}

const SubmitterQueue = {
  'not yet submitted': ['INITIAL', 'READY'],
  'needs reviewer approval': ['in-review'],
  'submission error': ['submission-error'],
  'needs final review': ['xml-review'],
}

const TaggerQueue = { tagging: ['tagging'] }

const Completed = {
  'approved for Europe PMC': ['ncbi-ready'],
  'available in Europe PMC': ['published'],
  'being withdrawn': ['being-withdrawn'],
  deleted: ['deleted'],
}

const AlertQuery = ({ states, interval }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={ALERT_MANUSCRIPTS}
    variables={{ states, interval }}
  >
    {({ data, loading }) => {
      if (loading) {
        return <LoadingIcon size={1.5} />
      }
      if (data.checkAge && data.checkAge.alert) {
        return (
          <Alert>
            <Icon color="currentColor" size={2}>
              alert-octagon
            </Icon>{' '}
            Manuscripts older than {interval}
          </Alert>
        )
      }
      return null
    }}
  </Query>
)

const QueueTable = ({ title, queue, data, alerts }) => {
  let total = 0
  const tableData = Object.keys(queue).map(label => {
    const items = data.filter(s => queue[label].includes(s.type))
    let count = 0
    const states = items.map(i => {
      count += i.count
      total += i.count
      return i.type
    })
    return { label, status: queue[label], count, states }
  })
  return (
    <React.Fragment>
      <H3 style={{ marginTop: 0 }}>{title}</H3>
      <ListTable>
        <tbody>
          <tr>
            <th>{total}</th>
            <th>All</th>
          </tr>
          {tableData.map(row => (
            <tr key={row.label}>
              <td>{row.count}</td>
              <td>
                <span style={{ display: 'inline-flex', alignItems: 'center' }}>
                  {row.count ? (
                    <Link to={`/search?status=${row.status}`}>{row.label}</Link>
                  ) : (
                    row.label
                  )}
                  {alerts && (
                    <AlertQuery interval={alerts} states={row.states} />
                  )}
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </ListTable>
    </React.Fragment>
  )
}

const QueueCheck = ({ data }) => {
  const myQueue = ['submitted', 'xml-qa', 'xml-triage']
  const needCite = data.find(r => r.type === 'xml-complete').count
  const filtered = data.reduce(
    (sum, row) => (myQueue.includes(row.type) ? sum + row.count : sum),
    0,
  )
  if (filtered === 0) {
    return (
      <React.Fragment>
        <Notification type="info">
          Your queue is empty.
          {needCite > 0 && (
            <React.Fragment>
              {' '}
              <Link to="/search?status=xml-complete">{needCite}</Link>{' '}
              submission{needCite !== 1 && 's'} need{needCite === 1 && 's'} a
              citation
            </React.Fragment>
          )}
        </Notification>
        <br />
      </React.Fragment>
    )
  }
  return null
}

const Dashboard = ({ currentUser }) => (
  <Query fetchPolicy="cache-and-network" query={COUNT_MANUSCRIPTS}>
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      return (
        <React.Fragment>
          <QueueCheck data={data.countByStatus} />
          <QueueTable
            data={data.countByStatus}
            queue={HelpdeskQueue}
            title="Helpdesk queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={SubmitterQueue}
            title="Submitter/Reviewer queue"
          />
          <QueueTable
            alerts="5 days"
            data={data.countByStatus}
            queue={TaggerQueue}
            title="Tagger queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={Completed}
            title="Completed"
          />
          <Link
            style={{ display: 'flex', alignItems: 'center' }}
            to="/admin-metrics"
          >
            <span>Go to metrics</span>
            <Icon color="currentColor">trending-up</Icon>
          </Link>
        </React.Fragment>
      )
    }}
  </Query>
)

const AdminDashboard = DashboardBase(Dashboard)

const AdminDashboardPage = ({ currentUser }) => {
  if (currentUser.admin) {
    return <AdminDashboard currentUser={currentUser} pageTitle="Dashboard" />
  }
  return null
}

export default AdminDashboardPage
