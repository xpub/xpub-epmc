import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { H2 as HTwo, Link } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { pageSize, states } from 'config'
import URLSearchParams from 'url-search-params'
import { Loading, LoadingIcon, Notification, Toggle, Pagination } from '../ui'
import {
  USER_MANUSCRIPTS,
  MANUSCRIPTS_BY_STATUS,
  COUNT_MANUSCRIPTS,
} from './operations'
import DashboardList from './DashboardList'
import SearchBoxes from './SearchBoxes'

const H2 = styled(HTwo)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
`
const ToggleBar = styled(Toggle)`
  float: left;
  @media screen and (max-width: 880px) {
    float: none;
  }
`
const PaginationPane = styled.div`
  display: flex;
  justify-content: flex-end;
`

const MyManuscripts = ({ currentUser, errors, history }) => {
  const onPageEntered = p => {
    history.push(`/dashboard?completed=true&page=${p}`)
  }
  return (
    <Query fetchPolicy="cache-and-network" query={COUNT_MANUSCRIPTS}>
      {({ data, loading }) => {
        if (loading || !data) {
          return (
            <Loading>
              <LoadingIcon />
            </Loading>
          )
        }
        // get the total number of manuscripts to decide whether the toggles are required
        const { countByStatus } = data
        const done = states.indexOf('xml-complete')
        const totalCount = countByStatus.reduce(
          (sum, status) => sum + parseInt(status.count, 10),
          0,
        )
        const totalComplete = countByStatus
          .filter(s => states.slice(done).includes(s.type))
          .reduce((sum, status) => sum + parseInt(status.count, 10), 0)
        const togglesRequired = totalCount > pageSize && totalComplete > 0
        // determine the query and variables used to get manuscripts
        const query = togglesRequired ? MANUSCRIPTS_BY_STATUS : USER_MANUSCRIPTS
        const params = new URLSearchParams(history.location.search)
        // togglesRequired and on the completed toggle
        const completed = params.get('completed')

        const variables = {}
        if (togglesRequired) {
          if (completed) {
            variables.query = states.slice(done).join(',')
            variables.page = params.get('page') ? params.get('page') - 1 : 0
            variables.pageSize = pageSize
          } else {
            variables.query = states.slice(0, done).join(',')
            variables.page = -1
          }
        }
        return (
          <Query
            fetchPolicy="cache-and-network"
            query={query}
            variables={variables}
          >
            {({ data, loading, errors: dErrors }) => {
              if (loading) {
                return (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )
              }
              if (dErrors) {
                return dErrors.map(e => (
                  <Notification type="error">{e.message}</Notification>
                ))
              }
              const manuscripts = togglesRequired
                ? data.findByStatus.manuscripts
                : data.manuscripts
              const total = togglesRequired
                ? data.findByStatus.total
                : data.manuscripts.length

              const toggles = togglesRequired && (
                <React.Fragment>
                  <ToggleBar>
                    <Link
                      className={!completed ? 'current' : ''}
                      to="/dashboard"
                    >
                      In process
                    </Link>
                    <Link
                      className={completed ? `current (${total})` : ''}
                      to="/dashboard?completed=true"
                    >
                      Completed
                    </Link>
                  </ToggleBar>
                  <SearchBoxes reviewer />
                </React.Fragment>
              )

              if (completed) {
                const currentPage = params.get('page')
                  ? parseInt(params.get('page'), 10)
                  : 1

                const PageNavigation = () => (
                  <PaginationPane>
                    <Pagination
                      currentPage={currentPage}
                      onPageEntered={onPageEntered}
                      pageSize={pageSize}
                      totalSize={total}
                    />
                  </PaginationPane>
                )
                return (
                  <React.Fragment>
                    {toggles}
                    {total && <PageNavigation />}
                    <DashboardList
                      currentUser={currentUser}
                      listData={manuscripts}
                    />
                    {total && <PageNavigation />}
                  </React.Fragment>
                )
              }

              const attention = []
              const warning = []
              const processing = []
              const complete = []
              const highlight = [
                'INITIAL',
                'READY',
                'submission-error',
                'in-review',
                'xml-review',
              ]
              manuscripts.forEach(m => {
                const reviewer = m.teams.find(t => t.role === 'reviewer')
                const submitter = m.teams.find(t => t.role === 'submitter')
                if (highlight.includes(m.status)) {
                  if (
                    reviewer &&
                    reviewer.teamMembers[0].user.id === currentUser.id &&
                    highlight.slice(-2).includes(m.status)
                  ) {
                    attention.push(m)
                  } else if (
                    submitter.teamMembers[0].user.id === currentUser.id &&
                    highlight.slice(0, 3).includes(m.status)
                  ) {
                    attention.push(m)
                  } else {
                    warning.push(m)
                  }
                } else if (
                  !togglesRequired &&
                  states.slice(done).includes(m.status)
                ) {
                  complete.push(m)
                } else {
                  processing.push(m)
                }
              })
              return (
                <React.Fragment>
                  {toggles}
                  {manuscripts.length === 0 && (
                    <Notification type="info">
                      There are currently no manuscripts connected to this
                      account.
                    </Notification>
                  )}
                  {errors.map(e => (
                    <Notification
                      key="message"
                      type={e.type ? e.type : 'warning'}
                    >
                      {e.message}
                    </Notification>
                  ))}
                  {attention.length > 0 && (
                    <React.Fragment>
                      <H2>Needs my attention ({attention.length})</H2>
                      <DashboardList
                        currentUser={currentUser}
                        listData={attention}
                        sectionColor="error"
                      />
                    </React.Fragment>
                  )}
                  {warning.length > 0 && (
                    <React.Fragment>
                      <H2>
                        Waiting for action by another user ({warning.length})
                      </H2>
                      <DashboardList
                        currentUser={currentUser}
                        listData={warning}
                        sectionColor="warning"
                      />
                    </React.Fragment>
                  )}
                  {processing.length > 0 && (
                    <React.Fragment>
                      <H2>In process at Europe PMC ({processing.length})</H2>
                      <DashboardList
                        currentUser={currentUser}
                        listData={processing}
                      />
                    </React.Fragment>
                  )}
                  {complete.length > 0 && (
                    <React.Fragment>
                      <H2>Submission complete ({complete.length})</H2>
                      <DashboardList
                        listData={complete}
                        sectionRole="submitter"
                      />
                    </React.Fragment>
                  )}
                </React.Fragment>
              )
            }}
          </Query>
        )
      }}
    </Query>
  )
}

export default MyManuscripts
