import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { Action } from '@pubsweet/ui'
import { METRICS } from './operations'
import { Loading, LoadingIcon, Table } from '../ui'
import DashboardBase from './DashboardBase'

const INIT_SHOW = 5

const MetricsTable = styled.div`
  overflow-x: auto;
  th,
  td {
    white-space: nowrap;
  }
`

const ShowAll = styled(Action)`
  float: right;
`

class Metrics extends React.Component {
  state = {
    show: INIT_SHOW,
  }

  onShowAll = (number, e) => {
    this.setState({
      show: number,
    })
  }

  render() {
    return (
      <Query fetchPolicy="cache-and-network" query={METRICS}>
        {({ data, loading }) => {
          if (loading) {
            return (
              <Loading>
                <LoadingIcon />
              </Loading>
            )
          }

          const allMetrics = data.getMetrics

          const { show } = this.state

          const metrics = allMetrics.slice(0, show)

          const headerCols = metrics.map(metrix => metrix.display_mth)

          const dataRows = [
            {
              prop: 'xml_review',
              text: 'Total manuscripts processed',
            },
            {
              prop: 'xml_review_within_10_days',
              text: 'Number of manuscripts processed in 10 days',
            },
            {
              prop: 'xml_review_within_10_days_perc',
              text: 'Percent of manuscripts processed in 10 days',
            },
            {
              prop: 'xml_review_within_3_days',
              text: 'Number of manuscripts processed in 3 days',
            },
            {
              prop: 'xml_review_within_3_days_perc',
              text: 'Percent of manuscripts processed in 3 days',
            },
            {
              prop: 'submitted',
              text: 'Manuscripts submitted to Europe PMC',
            },
            {
              prop: 'published',
              text: 'Manuscripts published to Europe PMC',
            },
            {
              prop: 'ncbi_ready_median',
              text:
                'Median processing times from submission to NCBI packet (days)',
            },
            {
              prop: 'external_qa',
              text: 'External QA',
            },
            {
              prop: 'xml_tagging',
              text: 'Manuscripts tagged',
            },
          ]

          const nextShow = show === INIT_SHOW ? allMetrics.length : INIT_SHOW

          return (
            <React.Fragment>
              <MetricsTable>
                <Table>
                  <tbody>
                    <tr>
                      <th />
                      {headerCols.map(col => (
                        <th key={col}>{col}</th>
                      ))}
                    </tr>
                    {dataRows.map(row => (
                      <tr key={row.prop}>
                        <td>{row.text}</td>
                        {metrics.map(metrix => (
                          <td key={metrix.display_mth}>
                            {!metrix[row.prop] && metrix[row.prop] !== 0
                              ? 'unknown'
                              : metrix[row.prop]}
                          </td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </MetricsTable>
              <ShowAll onClick={() => this.setState({ show: nextShow })}>
                Show the last {nextShow} months
              </ShowAll>
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

const MetricsPage = DashboardBase(Metrics)

export default MetricsPage
