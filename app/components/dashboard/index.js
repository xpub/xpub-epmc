const submitterState = {
  INITIAL: {
    status: 'Incomplete',
    color: 'error',
    url: 'create',
  },
  READY: {
    status: 'Incomplete',
    color: 'error',
    url: 'submit',
  },
  'submission-error': {
    status: 'Submission error',
    color: 'error',
    url: 'submit',
  },
  'in-review': {
    status: 'Initial review',
    color: 'warning',
    url: 'submit',
  },
  submitted: {
    status: 'Europe PMC QA',
    color: 'normal',
    url: 'submit',
  },
  tagging: {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-qa': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-review': {
    status: 'Final review',
    color: 'warning',
    url: 'submit',
  },
  'xml-triage': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-complete': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'ncbi-ready': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'ncbi-triage': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  published: {
    status: 'Available in Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'being-withdrawn': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'submit',
  },
}

const reviewerState = {
  INITIAL: {
    status: 'Incomplete',
    color: 'warning',
    url: 'submit',
  },
  READY: {
    status: 'Incomplete',
    color: 'warning',
    url: 'submit',
  },
  'submission-error': {
    status: 'Submission error',
    color: 'warning',
    url: 'submit',
  },
  'in-review': {
    status: 'Initial review',
    color: 'error',
    url: 'submit',
  },
  submitted: {
    status: 'Europe PMC QA',
    color: 'normal',
    url: 'submit',
  },
  tagging: {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-qa': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-review': {
    status: 'Final review',
    color: 'error',
    url: 'review',
  },
  'xml-triage': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-complete': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'ncbi-ready': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'ncbi-triage': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  published: {
    status: 'Available in Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'being-withdrawn': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'submit',
  },
}

const adminState = {
  INITIAL: {
    status: 'Incomplete',
    color: 'normal',
    url: 'create',
  },
  READY: {
    status: 'Incomplete',
    color: 'normal',
    url: 'submit',
  },
  'submission-error': {
    status: 'Submission error',
    color: 'normal',
    url: 'submit',
  },
  'in-review': {
    status: 'Initial review',
    color: 'normal',
    url: 'submit',
  },
  submitted: {
    status: 'Needs QA',
    color: 'error',
    url: 'submit',
  },
  tagging: {
    status: 'XML tagging',
    color: 'normal',
    url: 'review',
  },
  'xml-qa': {
    status: 'Needs XML QA',
    color: 'error',
    url: 'review',
  },
  'xml-review': {
    status: 'Final review',
    color: 'normal',
    url: 'review',
  },
  'xml-triage': {
    status: 'XML errors',
    color: 'error',
    url: 'review',
  },
  'xml-complete': {
    status: 'Needs citation',
    color: 'warning',
    url: 'activity',
  },
  'ncbi-ready': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'activity',
  },
  'ncbi-triage': {
    status: 'NCBI errors',
    color: 'error',
    url: 'review',
  },
  published: {
    status: 'Available in Europe PMC',
    color: 'success',
    url: 'activity',
  },
  'being-withdrawn': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'activity',
  },
}

const States = {
  submitter: submitterState,
  reviewer: reviewerState,
  admin: adminState,
}

const timeSince = date => {
  const seconds = Math.floor((new Date() - new Date(date)) / 1000)
  let interval = Math.floor(seconds / 31536000)
  if (interval >= 1) {
    return `${interval} year${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 2592000)
  if (interval >= 1) {
    return `${interval} month${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 86400)
  if (interval >= 1) {
    return `${interval} day${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 3600)
  if (interval >= 1) {
    return `${interval} hour${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 60)
  if (interval >= 1) {
    return `${interval} min${interval !== 1 ? 's' : ''}`
  }

  const time = Math.floor(seconds) < 0 ? 0 : Math.floor(seconds)

  return `${time} sec${interval !== 1 ? 's' : ''}`
}

export { default as SearchResultsPage } from './SearchResultsPage'
export { default as DashboardPage } from './DashboardPage'
export { default as AdminDashboard } from './AdminDashboard'
export { default as MetricsPage } from './MetricsPage'
export { States, timeSince }
