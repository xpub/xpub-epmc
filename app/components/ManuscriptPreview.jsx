import React from 'react'
import { Wax } from 'wax-prose-mirror'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PDFViewer from '../component-pdf-viewer'
import { Notification, Loading, LoadingIcon } from './ui'

const Doc = styled.div`
  font-family: ${th('fontReading')};
  padding: calc(${th('gridUnit')} * 2);
`

class ManuscriptPreview extends React.Component {
  state = { hasError: false, html: '' }
  async componentDidMount() {
    if (this.props.file.type === 'source') {
      const response = await fetch(this.props.file.url)
      const html = await response.text()
      if (this.refdiv) {
        this.setState({ html })
      }
    }
  }
  componentDidCatch(error, info) {
    if (error) {
      this.setState({ hasError: true })
    }
  }
  setRef = refdiv => {
    this.refdiv = refdiv
  }
  render() {
    const { file } = this.props
    if (!file || this.state.hasError) {
      return (
        <Notification type="error">
          Error: Unable to generate manuscript preview.
        </Notification>
      )
    }
    if (file.type === 'source') {
      return (
        <Doc id={this.state.html && 'html-preview'} ref={this.setRef}>
          {this.state.html ? (
            <Wax
              readonly
              renderLayout={({ editor, ...props }) => <div>{editor}</div>}
              value={this.state.html}
            />
          ) : (
            <Loading>
              <LoadingIcon />
            </Loading>
          )}
        </Doc>
      )
    } else if (file.mimeType === 'application/pdf') {
      return (
        <PDFViewer
          maxPages={50}
          textContent={this.props.textContent}
          url={file.url}
        />
      )
    }
    return (
      <Notification type="info">
        {`Unable to generate manuscript preview from ${file.filename}. `}
        {file.mimeType === 'application/pdf' ||
        file.mimeType ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ? (
          <React.Fragment>
            Please ensure your file is uncorrupted.
          </React.Fragment>
        ) : (
          <React.Fragment>
            {`Previews can only be generated from files with the extensions `}
            <em>.docx</em>
            {` and `}
            <em>.pdf</em>
          </React.Fragment>
        )}
      </Notification>
    )
  }
}

export default ManuscriptPreview
