import React from 'react'
import { ApolloConsumer } from 'react-apollo'
import styled from 'styled-components'
import moment from 'moment'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Button, Icon, H3, H4, Link } from '@pubsweet/ui'
import {
  A,
  B,
  Buttons,
  HTMLString,
  Loading,
  LoadingIcon,
  Notification,
  SearchForm,
  Toggle,
  ZebraList,
} from '../ui'
import { JOURNAL_INFO } from './operations'
import PubMedSearchResult from './PubMedSearchResult'
import UnmatchedCitation from './UnmatchedCitation'

const LoadMore = styled(Button)`
  margin: 15px auto;
`
const Notice = styled(H4)`
  margin: 0 auto;
  & + div {
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
`
const Links = styled.p`
  a {
    display: inline-flex;
    align-items: center;
  }
`
const FlexP = styled.p`
  display: flex;
  align-items: center;
  & > button {
    flex: 0 0 auto;
  }
  & > span {
    flex: 1 1 50%;
    margin-left: ${th('gridUnit')};
  }
`
const isDate = d => {
  let date = moment.utc(d, 'YYYY MMM DD')
  if (!date.isValid()) {
    date = moment.utc(d, 'YYYY MMM')
    if (!date.isValid()) {
      return false
    }
  }
  return date
}
class PubMedSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      query: this.props.search || '',
      retstart: 0,
      results: [],
      enabled: !!this.props.search,
      loading: false,
      hitcount: null,
      inPMC: null,
      inEPMC: false,
      unmatched: false,
      pubProvided: false,
      cannotLoad: false,
    }
    this.onSearch = this.onSearch.bind(this)
    this.onLoad = this.onLoad.bind(this)
    this.onQueryChange = this.onQueryChange.bind(this)
    this.onSelected = this.onSelected.bind(this)
    this.selectResult = this.selectResult.bind(this)
    this.handleUnmatched = this.handleUnmatched.bind(this)
  }
  componentDidMount() {
    if (
      this.props.manuscript &&
      (!this.props.manuscript.meta.articleIds ||
        this.props.manuscript.meta.articleIds.length === 0)
    ) {
      this.setState({ unmatched: true })
    }
    if (this.props.search) {
      this.onSearch()
    }
  }
  handleUnmatched(unm) {
    if (unm.journal && unm.journal.meta.pmcStatus) {
      this.setState({
        pubProvided: unm.journal.journalTitle,
        unmatched: false,
      })
    } else {
      const citationData = unm
      if (this.props.newNote && unm.note) {
        const { notes } = this.props.manuscript.meta
        const note = notes
          ? notes.find(n => n.notesType === 'userCitation')
          : null
        if (note) {
          this.props.changeNote({ id: note.id, ...unm.note })
        } else {
          this.props.newNote({
            manuscriptId: this.props.manuscript.id,
            ...unm.note,
          })
        }
        delete citationData.note
      }
      this.props.citationData(citationData)
    }
  }
  selectResult(result, journal, removePMCID) {
    const publicationDates = []
    const articleIds = [
      {
        pubIdType: 'pmid',
        id: result.uid,
      },
    ]
    if (result.pubdate) {
      const ppub = { type: 'ppub' }
      if (isDate(result.pubdate)) {
        ppub.date = isDate(result.pubdate)
      } else {
        ppub.date = moment.utc(result.pubdate.substring(0, 4), 'YYYY')
        ppub.jatsDate = {
          year: result.pubdate.substring(0, 4),
          season: result.pubdate.substring(5),
        }
      }
      publicationDates.push(ppub)
    }
    if (result.epubdate) {
      const epub = { type: 'epub' }
      if (isDate(result.epubdate)) {
        epub.date = isDate(result.epubdate)
      } else {
        epub.date = moment.utc(result.pubdate.substring(0, 4), 'YYYY')
        epub.jatsDate = {
          year: result.pubdate.substring(0, 4),
          season: result.epubdate.substring(5),
        }
      }
      publicationDates.push(epub)
    }
    if (result.articleids) {
      const doi = result.articleids.find(i => i.idtype === 'doi')
      const pmc = result.articleids.find(i => i.idtype === 'pmc')
      doi &&
        articleIds.push({
          pubIdType: 'doi',
          id: doi.value,
        })
      pmc &&
        articleIds.push({
          pubIdType: 'pmcid',
          id: pmc.value,
        })
    }
    const citationData = {
      journalId: journal.id,
      meta: {
        unmatchedJournal: null,
        title: result.title,
        volume: result.volume,
        issue: result.issue,
        location: {
          fpage: result.pages.split('-')[0] || null,
          lpage: result.pages.split('-').pop() || null,
          elocationId: result.elocationid,
        },
        publicationDates,
        articleIds,
      },
    }
    this.props.citationData(citationData, removePMCID)
  }
  onQueryChange(event) {
    this.setState({
      enabled: event ? !!event.target.value : false,
      retstart: 0,
      results: [],
      hitcount: null,
      cannotLoad: false,
      query: event ? event.target.value : '',
    })
  }
  onLoad(event) {
    this.onSearch(event)
  }
  async onSearch(event) {
    event && event.preventDefault()
    this.setState({
      enabled: false,
      loading: true,
    })
    const { query, retstart, results } = this.state

    if (retstart >= results.length && query) {
      const SearchUrl = `/eutils/esearch?term=${query
        .replace(/http.*doi\.org\//gi, '')
        .replace(/(.*)\W$/g, '$1')}&retstart=${retstart}&db=pubmed`

      try {
        const response = await fetch(SearchUrl, {
          headers: new Headers({
            Authorization: `Bearer ${window.localStorage.getItem('token')}`,
          }),
        })
        const json = await response.json()
        const ids = json.esearchresult.idlist
        const hitcount = parseInt(json.esearchresult.count, 10)
        const SummaryUrl = `/eutils/esummary?db=pubmed&id=${ids.join()}`
        const summary = await fetch(SummaryUrl, {
          headers: new Headers({
            Authorization: `Bearer ${window.localStorage.getItem('token')}`,
          }),
        })
        const summaryResponse = await summary.json()
        const { result } = summaryResponse
        const newResults = this.state.results.slice()
        ids.forEach(id => {
          newResults.push(result[id])
        })
        this.setState({
          hitcount,
          results: newResults,
          retstart: newResults.length > 0 ? newResults.length : 0,
        })
      } catch (e) {
        this.setState({
          cannotLoad: true,
          loading: false,
        })
      }
    }
    this.setState({ loading: false })
  }
  async onSelected(result, journal) {
    const pmcid =
      result.articleids.find(id => id.idtype === 'pmc') &&
      result.articleids.find(id => id.idtype === 'pmc').value
    if (pmcid) {
      this.setState({ loading: true })
      const obj = {
        inPMC: { pmcid, result, journal },
        loading: false,
      }
      const epmcURL = `https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=SRC:MED%20EXT_ID:${
        result.uid
      }&resulttype=lite&format=json`
      const response = await fetch(epmcURL)
      const json = await response.json()
      const epmc = json.resultList.result[0] && json.resultList.result[0].inEPMC
      if (epmc === 'Y') {
        obj.inEPMC = true
      }
      this.setState(obj)
    } else if (journal.meta.pmcStatus) {
      this.setState({
        pubProvided: { title: journal.journalTitle, result, journal },
      })
    } else {
      this.selectResult(result, journal)
    }
  }
  render() {
    const {
      results,
      hitcount,
      loading,
      query,
      inPMC,
      inEPMC,
      pubProvided,
      unmatched,
      cannotLoad,
    } = this.state
    const { manuscript, toggle, adminRemove } = this.props
    const { meta, journal } = manuscript || {}
    const { notes } = meta || {}
    const note = notes ? notes.find(n => n.notesType === 'userCitation') : null
    let journalInfo = journal
    if (!journalInfo && meta && meta.unmatchedJournal) {
      journalInfo = {
        journalTitle: meta.unmatchedJournal,
      }
    }
    return (
      <React.Fragment>
        {toggle && (
          <Toggle>
            <Action
              className={!unmatched && 'current'}
              onClick={() => this.setState({ unmatched: false })}
            >
              Populate from PubMed
            </Action>
            <Action
              className={unmatched && 'current'}
              onClick={() => this.setState({ unmatched: true })}
            >
              Enter manually
            </Action>
          </Toggle>
        )}
        {unmatched ? (
          <UnmatchedCitation
            citation={this.handleUnmatched}
            close={!toggle ? () => this.setState({ unmatched: false }) : false}
            journal={journalInfo}
            note={note ? note.content : ''}
            title={meta && meta.title}
          />
        ) : (
          <React.Fragment>
            {inPMC || pubProvided ? (
              <React.Fragment>
                {inPMC ? (
                  <React.Fragment>
                    <H3>No further action is required for this manuscript</H3>
                    <p>
                      The full text of the article &apos;
                      <HTMLString string={inPMC.result.title} />
                      &apos;
                      {` has already been sent to Europe PMC. Please use `}
                      <B>{inPMC.pmcid}</B>
                      {` for grant reporting purposes.`}
                    </p>
                    {inEPMC && (
                      <Links>
                        <A
                          href={`https://europepmc.org/articles/${inPMC.pmcid}`}
                          target="_blank"
                        >
                          <Icon color="currentColor">arrow-right-circle</Icon>
                          View this article on Europe PMC
                        </A>
                        {/* TODO: ORCID claiming
                        <br />
                        <A
                          href={`https://europepmc.org/orcid/import?3-1.ILinkListener-startwizard?query=${inPMC.pmcid}`}
                        >
                          <Icon color="currentColor">arrow-right-circle</Icon>
                          Claim this article to your ORCID
                        </A> */}
                      </Links>
                    )}
                    {adminRemove && (
                      <React.Fragment>
                        <H4>Admin options</H4>
                        <FlexP>
                          <Button
                            onClick={() =>
                              this.selectResult(inPMC.result, inPMC.journal)
                            }
                          >
                            Use citation data
                          </Button>
                          <span>
                            Force use of this citation data regardless of
                            manuscript being already in PMC.
                          </span>
                        </FlexP>
                        <FlexP>
                          <Button
                            onClick={() => {
                              this.selectResult(
                                inPMC.result,
                                inPMC.journal,
                                inPMC.pmcid,
                              )
                            }}
                          >
                            Remove submission
                          </Button>
                          <span>
                            Cancel the submission as already in PMC. Any
                            associated grants will be linked, and an email will
                            be sent to the submitter.
                          </span>
                        </FlexP>
                      </React.Fragment>
                    )}
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <H3>Your article will be provided by the publisher</H3>
                    <p>
                      {`Your article is published in `}
                      <B>{pubProvided.title}</B>
                      {` This journal participates in PMC.  Your article will be provided to PMC and made available in Europe PMC by the publisher. Please contact the publisher regarding any delays in this process.`}
                    </p>
                    <p>
                      {`Would you like to `}
                      <Action
                        onClick={() =>
                          this.selectResult(
                            pubProvided.result,
                            pubProvided.journal,
                          )
                        }
                      >
                        continue your submission with this citation
                      </Action>
                      {` regardless?`}
                    </p>
                  </React.Fragment>
                )}
                <Buttons>
                  <Link to="/">
                    <Button primary>End Submission</Button>
                  </Link>
                  <Button
                    onClick={() =>
                      this.setState({
                        inPMC: null,
                        inEPMC: false,
                        pubProvided: null,
                      })
                    }
                  >
                    Back to Search
                  </Button>
                </Buttons>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <H3>Populate from PubMed</H3>
                <SearchForm
                  buttonLabel="Search"
                  disabled={!this.state.enabled}
                  label="Search by article title, PMID, DOI, etc."
                  name="Search"
                  onChange={this.onQueryChange}
                  onSubmit={this.onSearch}
                  placeholder="Search for your article"
                  value={query}
                />
                {cannotLoad && (
                  <Notification type="warning">
                    {`Cannot load results. Please try a more specific query. Or, `}
                    <Action onClick={() => this.setState({ unmatched: true })}>
                      enter your citation manually
                    </Action>
                    .
                  </Notification>
                )}
                {hitcount !== null && (
                  <React.Fragment>
                    <Notice>Select your citation from results</Notice>
                    {(hitcount === 0 || results.length > 0) && (
                      <React.Fragment>
                        {this.props.changeNote ? (
                          <Notification type="info">
                            {hitcount === 0 && 'No results found. '}
                            {results.length > 0 &&
                              'Manuscript not in results? '}
                            <Action
                              onClick={() => this.setState({ unmatched: true })}
                            >
                              Click to enter citation manually.
                            </Action>
                          </Notification>
                        ) : (
                          <React.Fragment>
                            {hitcount === 0 && (
                              <Notification type="info">
                                No results found.
                              </Notification>
                            )}
                          </React.Fragment>
                        )}
                      </React.Fragment>
                    )}
                  </React.Fragment>
                )}
                {results.length > 0 && (
                  <ApolloConsumer>
                    {client => (
                      <ZebraList style={{ textAlign: 'center' }}>
                        {results.map(
                          result =>
                            result.fulljournalname && (
                              <PubMedSearchResult
                                key={result.uid}
                                onClick={async () => {
                                  const { data } = await client.query({
                                    query: JOURNAL_INFO,
                                    variables: { nlmId: result.nlmuniqueid },
                                  })
                                  this.onSelected(result, data.selectWithNLM)
                                }}
                                result={result}
                              />
                            ),
                        )}
                        {loading && (
                          <Loading>
                            <LoadingIcon />
                          </Loading>
                        )}
                        {results.length < hitcount && !loading && (
                          <LoadMore onClick={this.onLoad} secondary>
                            Load More Results
                          </LoadMore>
                        )}
                      </ZebraList>
                    )}
                  </ApolloConsumer>
                )}
                {results.length === 0 && loading && (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )}
              </React.Fragment>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default PubMedSearch
