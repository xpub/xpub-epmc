import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import createHistory from 'history/createBrowserHistory'
import * as yup from 'yup' // for everything
import Signup from './Signup'
import mutations from './mutations'

const handleSubmit = (
  values,
  { props, setSubmitting, setErrors, setValues },
) => {
  if (values.password.length < 8) {
    return
  }
  return props
    .epmc_signupUser({
      variables: {
        input: {
          title: values.title,
          givenNames: values.givenNames,
          surname: values.surname,
          password: values.password,
          email: values.email,
        },
      },
    })
    .then(({ data, errors }) => {
      if (!errors) {
        setValues(
          Object.assign(values, {
            success: 'Account has been created successfully!',
            error: '',
          }),
        )
        setTimeout(() => {
          createHistory({ forceRefresh: true }).push(
            `/login${values.location.search}`,
          )
        }, 2000)
        setSubmitting(true)
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
        setValues(
          Object.assign(values, {
            error: e.graphQLErrors[0].message,
          }),
        )
      }
    })
}

const enhancedFormik = withFormik({
  mapPropsToValues: props => ({
    email: props.email,
    password: props.password,
    title: props.title,
    givenNames: props.givenNames,
    surname: props.surname,
    confirmPassword: props.confirmPassword,
    signup: true,
    location: props.location,
  }),
  validationSchema: yup.object().shape({
    email: yup
      .string()
      .email('Email not valid')
      .required('Email is required'),
    givenNames: yup.string().required('Given names is required'),
    surname: yup.string().required('Surname is required'),
    password: yup.string().required('Password is required'),
  }),
  displayName: 'login',
  handleSubmit,
})(Signup)

export default compose(
  graphql(mutations.SIGNUP_USER, { name: 'epmc_signupUser' }),
)(enhancedFormik)
