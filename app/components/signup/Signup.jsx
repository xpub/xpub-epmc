import React from 'react'
import { Field } from 'formik'
import {
  H1,
  Link,
  Button,
  Checkbox,
  TextField,
  Icon,
  ErrorText,
} from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled, { createGlobalStyle, withTheme } from 'styled-components'
import { Page, Notification } from '../ui'
import SignInFooter from '../SignInFooter'
import UserCore from '../UserCore'

const NoClick = createGlobalStyle`
  body {
    pointer-events: none;
  }
`
const SignIn = styled.p``

const AcceptDiv = styled.div`
  margin-top: calc(${th('gridUnit')} * 1.5);
`
const Notice = styled.p`
  margin: ${th('gridUnit')} 0 0;
  text-indent: calc(3 * ${th('gridUnit')});
`
const ShowPassword = styled(Checkbox)`
  position: absolute;
  right: 0;
  top: 0;
  font-size: ${th('fontSizeBaseSmall')};
`
const PasswordDetails = styled.div`
  display: flex;
  align-items: center;
  width: 50%;
  max-width: 100%;
  @media screen and (max-width: 1000px) {
    width: 600px;
  @media screen and (max-width: 800px) {
    display: block;
    width: 408px;
  }
`
const PasswordField = styled.div`
  position: relative;
  width: 408px;
  max-width: 100%;
  margin-right: calc(${th('gridUnit')} * 1.5);
  @media screen and (max-width: 800px) {
    width: 100%;
  }
`
const PasswordRules = styled(ErrorText)`
  display: flex;
  align-items: center;
  color: ${props => (props.success ? th('colorSuccess') : th('colorError'))};
`

const PasswordIcon = withTheme(({ theme, valid }) => (
  <Icon color={valid ? theme.colorSuccess : theme.colorError} size={2.5}>
    {valid ? 'check' : 'x'}
  </Icon>
))

class Signup extends React.Component {
  state = {
    accept: false,
    emptyPassword: true,
    validPassword: false,
  }
  setButtonRef = button => {
    this.button = button
  }
  toggleAccept = () => {
    const { accept, showPassword } = this.state
    this.setState({
      accept: !accept,
      showPassword,
    })
  }
  toggleShowPassword = () => {
    const { showPassword } = this.state
    this.setState({
      showPassword: !showPassword,
    })
  }
  validatePassword = e => {
    const validPassword = e.target.value && e.target.value.length >= 8
    const emptyPassword = !e.target.value
    this.setState({
      emptyPassword,
      validPassword,
    })
  }
  PasswordInput = props => (
    <TextField
      invalidTest={props.invalidTest}
      label="Password"
      {...props.field}
      onBlur={e => this.validatePassword(e)}
      onKeyUp={e => this.validatePassword(e)}
      type="password"
    />
  )
  PlainPasswordInput = props => (
    <TextField
      invalidTest={props.invalidTest}
      label="Password"
      onBlur={e => this.validatePassword(e)}
      onKeyUp={e => this.validatePassword(e)}
      {...props.field}
    />
  )
  render() {
    const { values, handleSubmit, location, submitCount } = this.props
    return (
      <Page>
        <H1>Create a Europe PMC plus account</H1>
        {values.success && (
          <React.Fragment>
            <NoClick />
            <Notification type="success">{values.success}</Notification>
          </React.Fragment>
        )}
        {values.error && (
          <Notification type="error">{values.error}</Notification>
        )}
        <form onSubmit={handleSubmit}>
          <UserCore {...this.props}>
            <PasswordDetails>
              <PasswordField>
                {!this.state.showPassword && (
                  <Field component={this.PasswordInput} name="password" />
                )}

                {this.state.showPassword && (
                  <Field component={this.PlainPasswordInput} name="password" />
                )}
                <ShowPassword
                  checked={this.state.showPassword}
                  label="Show password"
                  onChange={() => this.toggleShowPassword()}
                />
                {(submitCount || !this.state.emptyPassword) && (
                  <PasswordRules success={this.state.validPassword}>
                    <PasswordIcon valid={this.state.validPassword} /> 8
                    character minimum
                  </PasswordRules>
                )}
              </PasswordField>
            </PasswordDetails>
          </UserCore>
          <AcceptDiv className="accept">
            <Checkbox
              checked={this.state.accept}
              label="I have read and accept the privacy notice."
              onChange={() => this.toggleAccept()}
            />
            <Notice>
              {`See `}
              <Link
                target="_blank"
                to="//www.ebi.ac.uk/data-protection/privacy-notice/europe-pmc-plus"
              >
                Privacy notice
              </Link>
              {` for Europe PMC’s Advanced User Services.`}
            </Notice>
          </AcceptDiv>
          <p>
            <Button
              disabled={!this.state.accept}
              primary
              ref={this.setButtonRef}
              type="submit"
            >
              Create account
            </Button>
          </p>
        </form>
        <SignIn>
          {`Already have a Europe PMC plus account? `}
          <Link to={`/login${location.search}`}>Sign in.</Link>
        </SignIn>
        <SignInFooter />
      </Page>
    )
  }
}

export default Signup
