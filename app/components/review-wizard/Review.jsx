import React from 'react'
import { Mutation } from 'react-apollo'
import styled, { createGlobalStyle } from 'styled-components'
import ReactHtmlParser from 'react-html-parser'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { Action, Button, H1, H2, H3, H4, Icon } from '@pubsweet/ui'
import {
  B,
  Buttons,
  LoadingIcon,
  PreviewPage,
  PreviewPanel,
  EditPanel,
  PanelHeader,
  PanelContent,
  SectionContent as Content,
  SectionHeader as Header,
  Toggle as Toggles,
  Notification,
  Page,
} from '../ui'
import UploadFiles, {
  SubmissionTypes,
  ReviewTypes,
  AllTypes,
} from '../upload-files'
import ManuscriptPreview from '../ManuscriptPreview'
import PDFViewer from '../../component-pdf-viewer'
import { FileThumbnails, FileLightbox } from '../preview-files'
import { GET_MANUSCRIPT } from '../operations'
import HTMLPreview from './HTMLPreview'
import ReviewForm from './ReviewForm'
import ReviewFooter from './ReviewFooter'
import Annotator from './Annotator'
import { CONVERT_XML } from './operations'

const NoClick = createGlobalStyle`
  body {
    pointer-events: none;
  }
`

const PreviewPageDiv = styled(PreviewPage)`
  .show-mobile {
    display: none;
  }
  .pad {
    padding: 0 calc(2 * ${th('gridUnit')});
  }
  @media screen and (max-width: 870px) {
    .show-mobile {
      display: inline-block;
    }
    .hide-mobile {
      display: none;
    }
  }
`
const PreviewPanelDiv = styled.div`
  top: calc(-${th('gridUnit')} * 25) !important;
  @media screen and (max-width: 1305px) {
    top: calc(-${th('gridUnit')} * 28) !important;
  }
  @media screen and (max-width: 896px) {
    top: calc(-${th('gridUnit')} * 31) !important;
  }
`
const PreviewPanelHeader = styled(PanelHeader)`
  height: calc(${th('gridUnit')} * 10);
  h1 {
    margin-bottom: ${th('gridUnit')};
  }
  @media screen and (max-width: 870px) {
    height: auto;
  }
`
const Instructions = styled.div`
  padding: 0 calc(2 * ${th('gridUnit')});
  margin-bottom: calc(4 * ${th('gridUnit')});
  font-style: italic;
`
const Toggle = styled(Toggles)`
  height: calc(${th('gridUnit')} * 7);
  &.hide-mobile button {
    margin-left: calc(${th('gridUnit')} * 3);
  }
  @media screen and (max-width: 870px) {
    height: auto;
  }
`
const PreviewError = styled.div`
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorError')};
  background-color: ${lighten('colorError', 100)};
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 2);
  margin: ${th('gridUnit')} 0;
  white-space: pre-wrap;
  overflow-wrap: break-word;
`
const ButtonSpacer = styled.div`
  height: calc(${th('gridUnit')} * 30) !important;
  @media screen and (max-width: 1305px) {
    height: calc(${th('gridUnit')} * 33) !important;
  }
  @media screen and (max-width: 896px) {
    height: calc(${th('gridUnit')} * 36) !important;
  }
  display: flex;
  align-items: flex-end;
  padding-bottom: calc(${th('gridUnit')} * 2);
`
const ManuscriptDiv = styled.div`
  h4 {
    margin: 0;
  }
  a {
    font-size: ${th('fontSizeBaseSmall')};
    display: block;
    margin: ${th('gridUnit')} 0;
  }
`
const PDFNotif = styled(Notification)`
  margin-top: 0;
`
const AnnotatePDF = Annotator(PDFViewer)
const AnnotateHTML = Annotator(HTMLPreview)

class Review extends React.Component {
  state = {
    open: true,
    pane: 'web',
    showManuscript: false,
    showAll: false,
    startProcessing: false,
    success: false,
  }
  componentDidMount() {
    if (this.page && this.props.manuscript) {
      const { files, status } = this.props.manuscript
      if (files && !files.find(f => f.type === 'PMC')) {
        this.setState({ showAll: true })
      }
      if (
        ['xml-triage', 'tagging', 'ncbi-triage'].includes(status) &&
        this.props.currentUser.admin
      ) {
        this.setState({ pane: 'files', open: status === 'ncbi-triage' })
      }
    }
  }
  showSuccess = () =>
    new Promise((resolve, reject) => {
      if (this.page) {
        window.scrollY = 0
        window.pageYOffset = 0
        document.scrollingElement.scrollTop = 0
        this.setState({ success: true })
        setTimeout(() => {
          this.setState({ success: false }, resolve())
        }, 3000)
      }
    })
  setRef = page => {
    this.page = page
  }
  render() {
    const { manuscript, currentUser, review } = this.props
    const { open, pane, showManuscript, showAll, startProcessing } = this.state
    const { files: allfiles, status, teams, pdfDepositState } = manuscript
    if (teams && allfiles) {
      const sourceFile =
        allfiles.find(file => file.type === 'source') ||
        allfiles.find(file => file.type === 'manuscript')
      const files = allfiles.filter(
        f => !f.type || ReviewTypes.some(rev => rev.value === f.type),
      )
      const originalFiles = allfiles.filter(f =>
        SubmissionTypes.some(sub => sub.value === f.type),
      )
      const combinedFiles = allfiles.filter(
        f =>
          f.type === 'manuscript' || AllTypes.some(all => all.value === f.type),
      )
      const html = files.find(file => file.type === 'tempHTML')
      const pdf =
        allfiles.find(f => f.type === 'pdf4load') ||
        allfiles.find(f => f.type === 'pdf4print')
      const xml = files.find(f => f.type === 'PMC') || null
      const reviewer =
        teams &&
        teams.find(t => t.role === 'reviewer') &&
        teams.find(t => t.role === 'reviewer').teamMembers[0]
      if (
        !currentUser.admin &&
        !(currentUser.external && manuscript.status === 'xml-qa') &&
        !(
          currentUser.id === reviewer.user.id &&
          manuscript.status === 'xml-review'
        )
      ) {
        this.props.history.push(`/submission/${manuscript.id}/submit`)
        return null
      }
      if (!reviewer) {
        return (
          <Page withHeader>
            <Notification type="error">
              Unable to load page: No reviewer selected
            </Notification>
          </Page>
        )
      }
      return (
        <React.Fragment>
          {this.state.success && (
            <React.Fragment>
              <NoClick />
              <Notification fullscreen type="success">
                {`Submission status change was successful`}
              </Notification>
            </React.Fragment>
          )}
          <PreviewPageDiv ref={this.setRef}>
            <PreviewPanel
              style={{
                flex: showManuscript && '1 1 1000px',
                width: showManuscript && '50%',
                maxWidth: showManuscript && '50%',
              }}
            >
              <PreviewPanelDiv style={{ maxWidth: showManuscript && '1000px' }}>
                <PreviewPanelHeader>
                  {status === 'ncbi-triage' ? (
                    <H1>Fix NCBI loading errors</H1>
                  ) : (
                    <H1>
                      {(status === 'xml-triage' && 'Correct') ||
                        (status === 'tagging' && 'Tag') ||
                        'Review'}
                      {` final web versions`}
                    </H1>
                  )}
                </PreviewPanelHeader>
                {pane !== 'files' && (
                  <Instructions>
                    <B>
                      {`To report an error, select text and describe the error in the comment box that appears.`}
                    </B>
                    {` Please only report errors or omissions that impact the scientific accuracy of your article. Only rendering errors need to be reported in the PDF preview.`}
                  </Instructions>
                )}
                <Toggle>
                  {currentUser.admin && (
                    <Action
                      className={pane === 'files' ? 'current' : ''}
                      onClick={() => this.setState({ pane: 'files' })}
                    >
                      XML files
                    </Action>
                  )}
                  {['xml-qa', 'xml-triage', 'ncbi-triage'].includes(status) && (
                    <Action
                      className={pane === 'xml' ? 'current' : ''}
                      disabled={!xml}
                      onClick={() => this.setState({ pane: 'xml' })}
                    >
                      XML view
                    </Action>
                  )}
                  <Action
                    className={pane === 'web' ? 'current' : ''}
                    disabled={!html}
                    onClick={() => this.setState({ pane: 'web' })}
                  >
                    Web preview
                  </Action>
                  <Action
                    className={pane === 'pdf' ? 'current' : ''}
                    disabled={!pdf}
                    onClick={() => this.setState({ pane: 'pdf' })}
                  >
                    PDF preview
                  </Action>
                  <Action
                    className={
                      pane === 'original'
                        ? 'current show-mobile'
                        : 'show-mobile'
                    }
                    onClick={() => this.setState({ pane: 'original' })}
                    primary={pane !== 'original'}
                  >
                    Submitted file
                  </Action>
                </Toggle>
                <PanelContent>
                  {pane === 'files' && (
                    <Mutation
                      mutation={CONVERT_XML}
                      refetchQueries={() => [
                        {
                          query: GET_MANUSCRIPT,
                          variables: { id: manuscript.id },
                        },
                      ]}
                    >
                      {(convertXML, { data }) => {
                        const generatePreviews = async () => {
                          if (!pdfDepositState) {
                            this.setState({ startProcessing: true })
                          }
                          await convertXML({
                            variables: { id: xml.id },
                          })
                          this.setState({ startProcessing: false })
                        }
                        const XMLButtons = () => (
                          <Buttons left top>
                            <Button
                              disabled={!xml}
                              onClick={() => generatePreviews()}
                              style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                              }}
                            >
                              {(pdfDepositState || startProcessing) && (
                                <LoadingIcon color="currentColor" size={2} />
                              )}
                              Process
                              {(pdfDepositState || startProcessing) &&
                                'ing'}{' '}
                              XML
                            </Button>
                            <Button
                              onClick={() =>
                                this.setState({ showAll: !showAll })
                              }
                            >
                              {showAll ? 'Hide ' : 'Show '}
                              submission files
                            </Button>
                          </Buttons>
                        )
                        return (
                          <React.Fragment>
                            <XMLButtons />
                            {manuscript.formState && (
                              <PreviewError>
                                {ReactHtmlParser(manuscript.formState)}
                              </PreviewError>
                            )}
                            <UploadFiles
                              checked
                              files={showAll ? combinedFiles : files}
                              manuscript={manuscript.id}
                              pdfSend={allfiles.find(
                                f => f.type === 'pdf4load',
                              )}
                              types={showAll ? AllTypes : ReviewTypes}
                            />
                            <XMLButtons />
                          </React.Fragment>
                        )
                      }}
                    </Mutation>
                  )}
                  {pane === 'pdf' && pdf && (
                    <React.Fragment>
                      {pdf.type === 'pdf4load' && (
                        <PDFNotif type="info">
                          The original manuscript file will be made available
                          for download in Europe PMC. No errors can be reported.
                        </PDFNotif>
                      )}
                      {status === 'tagging' || pdf.type === 'pdf4load' ? (
                        <PDFViewer url={pdf.url} />
                      ) : (
                        <React.Fragment>
                          <AnnotatePDF
                            file={pdf}
                            reload={this.props.reload}
                            revId={(review && review.id) || null}
                            userId={currentUser.id}
                          />
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                  {pane === 'web' && html && (
                    <React.Fragment>
                      {status === 'tagging' ? (
                        <HTMLPreview url={html.url} />
                      ) : (
                        <React.Fragment>
                          <AnnotateHTML
                            file={html}
                            reload={this.props.reload}
                            revId={(review && review.id) || null}
                            userId={currentUser.id}
                          />
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                  {pane === 'xml' && xml && (
                    <React.Fragment>
                      <React.Fragment>
                        <AnnotateHTML
                          file={xml}
                          reload={this.props.reload}
                          revId={(review && review.id) || null}
                          userId={currentUser.id}
                          xml
                        />
                      </React.Fragment>
                    </React.Fragment>
                  )}
                  {pane === 'original' && sourceFile && (
                    <ManuscriptPreview file={sourceFile} />
                  )}
                </PanelContent>
              </PreviewPanelDiv>
            </PreviewPanel>
            <EditPanel
              style={{
                flex: showManuscript && '1 1 1000px',
                width: showManuscript && '50%',
                maxWidth: showManuscript && '50%',
              }}
            >
              <div
                style={{
                  maxWidth: showManuscript && '1000px',
                  border: showManuscript && '0',
                  backgroundColor: showManuscript && 'transparent',
                }}
              >
                {showManuscript && sourceFile ? (
                  <PanelContent className="pad">
                    <ButtonSpacer className="hide-mobile">
                      <Button
                        onClick={() => this.setState({ showManuscript: false })}
                      >
                        Close manuscript file
                      </Button>
                    </ButtonSpacer>
                    <ManuscriptPreview file={sourceFile} />
                  </PanelContent>
                ) : (
                  <React.Fragment>
                    <PreviewPanelHeader>
                      <H2>Compare &amp; approve</H2>
                    </PreviewPanelHeader>
                    <PanelContent>
                      {status === 'xml-qa' && xml && (
                        <React.Fragment>
                          <Header>
                            <H3>XML file</H3>
                          </Header>
                          <Content>
                            <ManuscriptDiv>
                              <FileLightbox file={xml} />
                            </ManuscriptDiv>
                          </Content>
                        </React.Fragment>
                      )}
                      <React.Fragment>
                        <Action
                          onClick={() => this.setState({ open: !open })}
                          style={{ width: '100%', textDecoration: 'none' }}
                          title={`${open ? 'Hide' : 'Show'} files`}
                        >
                          <Header>
                            <H3>Submitted files</H3>
                            <Icon color="currentColor">
                              chevron-{open ? 'down' : 'right'}
                            </Icon>
                          </Header>
                        </Action>
                        {open && (
                          <Content>
                            {sourceFile && (
                              <ManuscriptDiv>
                                <H4>Manuscript file</H4>
                                <FileLightbox
                                  file={allfiles.find(
                                    f => f.type === 'manuscript',
                                  )}
                                />
                                <Button
                                  className="hide-mobile"
                                  onClick={() =>
                                    this.setState({ showManuscript: true })
                                  }
                                >
                                  View manuscript file alongside preview
                                </Button>
                              </ManuscriptDiv>
                            )}
                            <FileThumbnails files={originalFiles} />
                          </Content>
                        )}
                      </React.Fragment>
                      <ReviewForm
                        manuscript={manuscript}
                        previews={!!html && !!pdf}
                        review={review}
                      />
                    </PanelContent>
                  </React.Fragment>
                )}
              </div>
            </EditPanel>
          </PreviewPageDiv>
          <ReviewFooter
            currentUser={currentUser}
            files={originalFiles}
            manuscript={manuscript}
            previews={!!html && !!pdf}
            review={review}
            showSuccess={this.showSuccess}
          />
        </React.Fragment>
      )
    }
    return null
  }
}

export default Review
