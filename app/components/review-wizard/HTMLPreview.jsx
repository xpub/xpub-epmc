import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import CssModules from 'react-css-modules'
import { createGlobalStyle } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import MathJax from '../../react-mathjax-preview-local'
import styles from '../../assets/epmc_new_plus.scss'

const CodeBox = createGlobalStyle`
  #html-preview pre {
    white-space: pre-wrap !important;
    overflow-wrap: break-word;
    border: 0 !important;
    margin-top: 0 !important;
    font-size: ${th('fontSizeBaseSmall')} !important;
    background-color: #fff !important;
  }
`

class HTMLPreview extends React.Component {
  state = { html: null }
  async componentDidMount() {
    try {
      const response = await fetch(this.props.url)
      const html = await response.text()
      if (this.div) {
        await this.setState({ html })
      }
    } catch (error) {
      if (this.div) {
        this.setState({ html: `'No Web Preview generated. Error: ${error}` })
      }
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.loaded && this.div) {
      if (prevState.html === null && this.state.html !== null) {
        this.props.loaded(true)
      }
    }
  }
  setRef = div => {
    this.div = div
  }
  render() {
    return (
      <div id="html-preview" ref={this.setRef}>
        {this.props.xml ? (
          <React.Fragment>
            <CodeBox />
            <pre data-lang="xml">{this.state.html}</pre>
          </React.Fragment>
        ) : (
          ReactHtmlParser(this.state.html, {
            transform: function transform(node, index) {
              if (
                node.type === 'tag' &&
                node.name === 'span' &&
                node.attribs.class === 'f mathjax mml-math'
              ) {
                return (
                  <MathJax key={node.attribs.id} math={node.children[0].data} />
                )
              }
            },
          })
        )}
      </div>
    )
  }
}

export default CssModules(HTMLPreview, styles)
