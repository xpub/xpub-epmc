import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import { Action, Button, H2, Icon, RadioGroup } from '@pubsweet/ui'
import { PreviewFooter, Notification, Buttons, Portal, CloseModal } from '../ui'
import SubmissionCancel from '../SubmissionCancel'
import { REVIEW_MANUSCRIPT, GET_REVIEWS, CURRENT_REVIEW } from './operations'
import ReviewInstructions from './ReviewInstructions'
import ListErrors from './ListErrors'
import SubmissionError from './SubmissionError'

class ReviewFooter extends React.Component {
  state = {
    report: false,
    approve: false,
    cancel: false,
    radio: '',
  }
  render() {
    const {
      currentUser,
      manuscript,
      previews,
      review,
      history,
      showSuccess,
    } = this.props
    const { report, approve, cancel, radio } = this.state
    const { annotations } = review || []
    const { status, teams } = manuscript
    const submitter = teams.find(t => t.role === 'submitter').teamMembers[0]
    return (
      <PreviewFooter>
        <div>
          <Mutation
            mutation={REVIEW_MANUSCRIPT}
            refetchQueries={() => [
              {
                query: GET_REVIEWS,
                variables: { manuscriptId: manuscript.id },
              },
              {
                query: CURRENT_REVIEW,
                variables: { manuscriptId: manuscript.id },
              },
            ]}
          >
            {(reviewManuscript, { data }) => {
              const setStatus = async newStatus => {
                await showSuccess()
                await reviewManuscript({
                  variables: { data: { id: manuscript.id, status: newStatus } },
                })
              }
              if (status === 'tagging') {
                return (
                  <Button
                    disabled={!previews}
                    onClick={() => setStatus('xml-qa')}
                    primary
                  >
                    Send for XML QA
                  </Button>
                )
              } else if (status === 'ncbi-triage') {
                return (
                  <Button
                    onClick={async () => {
                      await setStatus('xml-complete')
                      history.push(`/submission/${manuscript.id}/activity`)
                    }}
                    primary
                  >
                    Retry send to NCBI
                  </Button>
                )
              } else if (status === 'xml-triage') {
                const options = [
                  {
                    value: 'xml-qa',
                    label: 'XML QA',
                  },
                  {
                    value: 'xml-review',
                    label: 'Author review',
                  },
                ]
                return (
                  <div>
                    <Button
                      onClick={() => this.setState({ approve: true })}
                      primary
                    >
                      Approve &amp; route
                    </Button>
                    <Button onClick={() => this.setState({ report: true })}>
                      Ask for correction
                    </Button>
                    {approve && (
                      <Portal transparent>
                        <CloseModal
                          onClick={() => this.setState({ approve: false })}
                        />
                        <H2>Errors fixed? Send for:</H2>
                        <RadioGroup
                          name="setstatus"
                          onChange={radio => this.setState({ radio })}
                          options={options}
                        />
                        <Buttons right>
                          <Button
                            disabled={!radio}
                            onClick={async () => {
                              await setStatus(radio)
                              if (radio === 'xml-qa') {
                                this.setState({ approve: false })
                              } else {
                                history.push(
                                  `/submission/${manuscript.id}/activity`,
                                )
                              }
                            }}
                            primary
                          >
                            Confirm &amp; send
                          </Button>
                          <Button
                            onClick={() => this.setState({ approve: false })}
                          >
                            Cancel
                          </Button>
                        </Buttons>
                      </Portal>
                    )}
                    {report && (
                      <SubmissionError
                        annotations={annotations}
                        close={() => this.setState({ report: false })}
                        currentUser={currentUser}
                        manuscript={manuscript}
                      />
                    )}
                  </div>
                )
              } else if (status === 'xml-qa' || status === 'xml-review') {
                return (
                  <React.Fragment>
                    <div>
                      <Button
                        disabled={!previews}
                        onClick={() => this.setState({ approve: true })}
                        primary
                      >
                        Approve
                      </Button>
                      <Button onClick={() => this.setState({ report: true })}>
                        Report errors
                      </Button>
                    </div>
                    {currentUser.id === submitter.user.id && (
                      <Action
                        onClick={() => this.setState({ cancel: true })}
                        style={{ display: 'inline-flex', alignItems: 'center' }}
                      >
                        <Icon color="currentColor" size={2.5}>
                          trash-2
                        </Icon>
                        Cancel submission
                      </Action>
                    )}
                    {approve && (
                      <Portal style={{ width: '600px' }} transparent>
                        <p>
                          {status === 'xml-review'
                            ? `Approve the final web versions of the manuscript for release to Europe PMC now?`
                            : `Confirm QA has been completed, and approve the web versions of the manuscript?`}
                        </p>
                        {annotations && annotations.length > 0 && (
                          <Notification type="warning">
                            {`Errors have been highlighted! Are you sure you want to approve the submission, rather than submitting your error report?`}
                          </Notification>
                        )}
                        <Buttons right>
                          <Button
                            onClick={async () => {
                              this.setState({ approve: false })
                              await setStatus(
                                status === 'xml-review'
                                  ? 'xml-complete'
                                  : 'xml-review',
                              )
                              history.push('/')
                            }}
                            primary
                          >
                            Yes
                          </Button>
                          <Button
                            onClick={() => this.setState({ approve: false })}
                          >
                            No
                          </Button>
                        </Buttons>
                      </Portal>
                    )}
                    {report && (
                      <React.Fragment>
                        {annotations && annotations.length > 0 ? (
                          <Portal transparent>
                            <CloseModal
                              onClick={() => this.setState({ report: false })}
                            />
                            <H2>Send the following errors?</H2>
                            <ListErrors annotations={annotations} />
                            <Buttons right>
                              <Button
                                onClick={async () => {
                                  this.setState({ report: false })
                                  await setStatus('xml-triage')
                                  history.push('/')
                                }}
                                primary
                              >
                                Send error report
                              </Button>
                              <Button
                                onClick={() => this.setState({ report: false })}
                              >
                                Cancel
                              </Button>
                            </Buttons>
                          </Portal>
                        ) : (
                          <ReviewInstructions
                            close={() => this.setState({ report: false })}
                          />
                        )}
                      </React.Fragment>
                    )}
                  </React.Fragment>
                )
              }
              history.push('/')
              return null
            }}
          </Mutation>
        </div>
        {cancel && (
          <SubmissionCancel
            callback={() => this.props.history.push('/')}
            close={() => this.setState({ cancel: false })}
            manuscriptId={manuscript.id}
          />
        )}
      </PreviewFooter>
    )
  }
}

export default withRouter(ReviewFooter)
