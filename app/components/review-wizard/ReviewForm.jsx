import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import { H3 } from '@pubsweet/ui'
import {
  Loading,
  LoadingIcon,
  SectionContent as Content,
  SectionHeader as Header,
} from '../ui'
import ListErrors from './ListErrors'
import ReviewHistory from './ReviewHistory'
import { GET_REVIEWS } from './operations'

const ReviewFormDisplay = ({
  manuscript,
  previews,
  review,
  deleted,
  history,
  reportError = false,
}) => {
  const { annotations } = review || []
  const { status, teams } = manuscript
  const reviewer = teams.find(t => t.role === 'reviewer').teamMembers[0]
  if (status === 'tagging') {
    return (
      <React.Fragment>
        <Content>
          <H3>Tagging complete?</H3>
          <p>
            {`When all files have been uploaded and both the Web and PDF previews have been generated, please click the button below to move the manuscript to XML QA.`}
          </p>
        </Content>
      </React.Fragment>
    )
  } else if (manuscript.status === 'xml-triage') {
    const user = review ? review.user : null
    const { givenNames, surname } = (user && user.identities[0].name) || ''
    return (
      <React.Fragment>
        <Header>
          <H3>
            Review
            {(review && ' correction request') ||
              (manuscript.formState && ' errors') ||
              ' and route'}
          </H3>
        </Header>
        <Content>
          <div>
            {review && (
              <p>
                Reported by {givenNames} {surname}
                {user.id === reviewer.user.id && ' (Reviewer)'}
              </p>
            )}
            {annotations && <ListErrors annotations={annotations} />}
            {deleted.length > 0 && <ReviewHistory reviews={deleted} />}
            <p>
              {`You can fix any errors and approve for review, or ${
                review
                  ? 'send this list of issues to the taggers to fix. You may edit the annotation text to provide more detail if necessary'
                  : 'request a fix from the taggers'
              }.`}
            </p>
            <p>
              {`If an error is caused by missing or incomplete files, click 'Return for user upload' and you will be given the opportunity to write a message explaining what is needed, and send the manuscript back to the submitter or reviewer to upload files.`}
            </p>
          </div>
        </Content>
      </React.Fragment>
    )
  } else if (status === 'xml-qa' || status === 'xml-review') {
    if (status === 'xml-review') {
      deleted = deleted.filter(r => r.user.id === reviewer.user.id)
    }
    if (annotations || deleted.length > 0) {
      return (
        <React.Fragment>
          <Header>
            <H3>Error report</H3>
          </Header>
          <Content>
            <div>
              {annotations && <ListErrors annotations={annotations} />}
              {deleted.length > 0 && <ReviewHistory reviews={deleted} />}
            </div>
          </Content>
        </React.Fragment>
      )
    }
    return null
  } else if (status === 'ncbi-triage') {
    return null
  }
  history.push('/')
  return null
}

const ReviewFormRouter = withRouter(ReviewFormDisplay)

const ReviewForm = ({ manuscript, ...props }) => (
  <Query query={GET_REVIEWS} variables={{ manuscriptId: manuscript.id }}>
    {({ data: { deletedReviews }, loading }) => {
      if (loading || !deletedReviews) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      return (
        <ReviewFormRouter
          deleted={deletedReviews}
          manuscript={manuscript}
          {...props}
        />
      )
    }}
  </Query>
)

export default ReviewForm
