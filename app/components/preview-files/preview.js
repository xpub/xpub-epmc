export const ImageTypes = [
  'image/png',
  'image/jpeg',
  'image/bmp',
  'image/gif',
  'image/svg+xml',
]

export const fileSort = files =>
  files.sort((a, b) => {
    if (a.type === 'manuscript') {
      return -1
    }
    if (b.type === 'manuscript') {
      return 1
    }
    if (!a.type) {
      return 1
    }
    if (!b.type) {
      return -1
    }
    if (a.type > b.type) {
      return 1
    }
    if (a.type === b.type) {
      if (!a.label) {
        return 1
      }
      if (!b.label) {
        return -1
      }
      if (/\d/.test(a.label) && /\d/.test(b.label)) {
        if (
          parseInt(a.label.match(/\d+/)[0], 10) >
          parseInt(b.label.match(/\d+/)[0], 10)
        ) {
          return 1
        }
        return -1
      }
      if (a.label > b.label) {
        return 1
      }
      return -1
    }
    return -1
  })
