import React from 'react'
import { Mutation } from 'react-apollo'
import { withRouter } from 'react-router'
import { Button, CheckboxGroup, Icon } from '@pubsweet/ui'
import { A, Portal, Buttons } from '../ui'
import { ALERT_PIS } from '../operations'
import { SUBMIT_MANUSCRIPT } from './operations'

const SubmitApprove = ({
  showSuccess,
  adminButton,
  disabled,
  xml,
  manuscriptId,
  status,
  checkedBoxes,
  close,
  journal,
  releaseDelay,
  setChecked,
  history,
}) => {
  const options = [
    {
      value: '1',
      label: `This manuscript has been accepted for publication in ${journal} and includes all modifications resulting from the peer review process.`,
    },
    {
      value: '2',
      label: `I request that this manuscript be publicly accessible through Europe PMC ${releaseDelay &&
        (releaseDelay === '0'
          ? 'immediately'
          : `${releaseDelay} month${
              releaseDelay === '1' ? '' : 's'
            }`)} after the publisher's official date of final publication, and I confirm that the publisher is aware of, and has agreed to, this action.`,
    },
    {
      value: '3',
      label:
        'This manuscript is the result of research supported, in whole or in part, by a member of the Europe PMC Funders Group.',
    },
  ]
  return (
    <Mutation mutation={SUBMIT_MANUSCRIPT}>
      {(submitManuscript, { data }) => {
        const submit = async newStatus => {
          const variables = { data: { id: manuscriptId, status: newStatus } }
          if (newStatus === 'submitted' && xml) {
            variables.data.status = 'xml-triage'
          }
          await submitManuscript({ variables })
          await showSuccess()
          if (close) {
            close()
          }
        }
        if (adminButton === 'tagging') {
          return (
            <Mutation mutation={ALERT_PIS}>
              {(epmc_emailGrantPis, { data }) => {
                const sendAlert = async () => {
                  await epmc_emailGrantPis({ variables: { manuscriptId } })
                }
                return (
                  <Button
                    disabled={disabled}
                    onClick={() => {
                      sendAlert()
                      submit('tagging')
                    }}
                    primary
                  >
                    Approve for tagging
                  </Button>
                )
              }}
            </Mutation>
          )
        }
        if (adminButton === 'review') {
          return (
            <Button
              disabled={disabled}
              onClick={() => {
                submit('submitted')
              }}
              primary
            >
              Send for new review
            </Button>
          )
        }
        return (
          <Portal transparent>
            <p>Please certify the following statements are true:</p>
            <CheckboxGroup
              name="reviewer-checklist"
              onChange={c => setChecked(c)}
              options={options}
              value={checkedBoxes}
            />
            <p
              style={{
                fontStyle: 'italic',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Icon size={2}>help-circle</Icon>
              <span>
                What is the{' '}
                <A href="https://europepmc.org/Funders/" target="_blank">
                  Europe PMC Funder&apos;s Group?
                </A>
              </span>
            </p>
            <Buttons right>
              <Button
                disabled={
                  checkedBoxes.length <= 0 ||
                  checkedBoxes.reduce(
                    (acc, val) => parseInt(acc, 10) + parseInt(val, 10),
                  ) < 6
                }
                onClick={() => submit('submitted')}
                primary
              >
                Confirm &amp; submit
              </Button>
              <Button onClick={() => close()}>Cancel</Button>
            </Buttons>
          </Portal>
        )
      }}
    </Mutation>
  )
}

export default withRouter(SubmitApprove)
