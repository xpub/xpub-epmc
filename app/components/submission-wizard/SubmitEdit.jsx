import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2, Icon } from '@pubsweet/ui'
import { SplitPage, StepPanel, InfoPanel, Buttons, CloseModal } from '../ui'
import UploadFiles, { SubmissionTypes } from '../upload-files'
import SelectReviewer from '../SelectReviewer'
import CreateInfo from './CreateInfo'

const CloseEdit = styled(CloseModal)`
  margin: calc(${th('gridUnit')} * 6) auto calc(${th('gridUnit')} * 2);
`
export const ErrorMessage = styled.p`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  display: flex;
  align-items: flex-start;
  margin-left: calc(-${th('gridUnit')} / 2);
`
export class EditSubmissionReviewer extends React.Component {
  state = { newReviewer: null }
  render() {
    const {
      manuscript,
      changeNote,
      deleteNote,
      newNote,
      reviewerNote,
      close,
      ...props
    } = this.props
    const { newReviewer } = this.state
    return (
      <React.Fragment>
        <SelectReviewer
          {...props}
          reviewerNote={reviewerNote}
          setReviewerNote={v => this.setState({ newReviewer: v })}
        />
        <Buttons right>
          <Button
            disabled={!newReviewer}
            onClick={async () => {
              let result = false
              if (reviewerNote && newReviewer) {
                if (newReviewer === 'delete') {
                  result = await deleteNote(reviewerNote.id)
                } else {
                  result = await changeNote({
                    id: reviewerNote.id,
                    notesType: 'selectedReviewer',
                    content: JSON.stringify(newReviewer),
                  })
                }
              } else {
                result = await newNote({
                  notesType: 'selectedReviewer',
                  content: JSON.stringify(newReviewer),
                })
              }
              if (result) {
                close()
              }
            }}
            primary
          >
            Save
          </Button>
        </Buttons>
      </React.Fragment>
    )
  }
}

const SubmitEdit = ({
  close,
  currentStep,
  error,
  files,
  mId,
  pdfSend,
  children,
  ...props
}) => (
  <SplitPage>
    <StepPanel>
      <div>
        <CloseEdit onClick={() => close()} />
        {children.props['data-upload'] ? (
          <div>
            <H2>Files</H2>
            <UploadFiles
              checked
              files={files}
              manuscript={mId}
              pdfSend={pdfSend}
              types={SubmissionTypes}
            />
          </div>
        ) : (
          <React.Fragment>
            {children.props['data-reviewer']
              ? React.cloneElement(children, { close })
              : children}
          </React.Fragment>
        )}
        {error && (
          <ErrorMessage>
            <Icon color="currentColor" size={2}>
              alert_circle
            </Icon>
            {error}
          </ErrorMessage>
        )}
        {!children.props['data-reviewer'] && (
          <Buttons>
            <Button
              onClick={() => close()}
              primary={!children.props['data-citation']}
            >
              {children.props['data-citation'] ? 'Cancel' : 'Save'}
            </Button>
          </Buttons>
        )}
      </div>
    </StepPanel>
    <InfoPanel>
      <CreateInfo currentStep={currentStep} />
    </InfoPanel>
  </SplitPage>
)

export default SubmitEdit
