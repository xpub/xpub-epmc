import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { HTMLString } from '../ui'

const Small = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  display: block;
`

const Citation = props => {
  const { title, articleIds, unmatchedJournal } = props.metadata
  return (
    <p>
      <HTMLString string={title} />
      <Small>
        <span>
          <HTMLString
            string={props.journal ? props.journal.meta.nlmta : unmatchedJournal}
          />
        </span>
        {articleIds &&
          articleIds
            .filter(aid => aid.pubIdType !== 'doi')
            .map(aid => (
              <span key={aid.id}>
                , {aid.pubIdType.toUpperCase()}: {aid.id}
              </span>
            ))}
      </Small>
    </p>
  )
}

export default Citation
