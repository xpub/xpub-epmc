import React from 'react'
import { H2, H3, Link } from '@pubsweet/ui'
import { A } from '../ui'

const CreateInfo = ({ currentStep }) => (
  <React.Fragment>
    {currentStep === 0 && (
      <div className="createInfo">
        <H2>Getting started</H2>
        <H3>What you’ll need to complete the submission</H3>
        <ul>
          <li>Manuscript title</li>
          <li>
            Journal name, other citation information if not found in PubMed
          </li>
          <li>Grant numbers</li>
          <li>All manuscript files including supplemental data</li>
        </ul>
        <H3>Overview of the process</H3>
        <ol>
          <li>Submit manuscript</li>
          <li>Review submission for completeness</li>
          <li>Proof final web version</li>
        </ol>
        <H3>How long it will take</H3>
        <p>
          {`If all manuscript files are submitted in appropriate formats, and if approval and communication are prompt, the process typically takes about six to eight weeks.`}
        </p>
      </div>
    )}
    {currentStep === 1 && (
      <div className="createInfo">
        <H2>Preparing files</H2>
        <H3>Which versions of files should I upload?</H3>
        <p>
          {`Generally, the accepted peer reviewed version of the manuscript, or the version of record for creative commons licensed articles, should be submitted. Check your journal’s version policy at `}
          <A href="http://www.sherpa.ac.uk/romeo/index.php" target="_blank">
            SHERPA/RoMEO
          </A>
        </p>
        <H3>What is the best way to label files?</H3>
        <p>
          {`The labels are intended to help put your manuscript together as intended. Provide a label that matches the corresponding file as mentioned in the text.`}
        </p>
        <H3>What file formats can I use?</H3>
        <p>
          {`For the manuscript text, only Microsoft Word or PDF files can be used. For figures, tables, and supplementary files, check `}
          <Link target="_blank" to="/user-guide/allowedfiles">
            our list of acceptable file types
          </Link>
          .
        </p>
      </div>
    )}
    {currentStep === 2 && (
      <div className="createInfo">
        <H2>Grants & embargo periods</H2>
        <H3>
          {`What if a grant or project that supported this manuscript is not in the list?`}
        </H3>
        <p>
          {`If a particular grant or project that you are expecting to see is not listed, please verify that the proper PI name was entered. You should use an initial rather than the full first name, e.g. “R” instead of “Robert”.`}
        </p>
        <p>
          {`If you can’t locate your grant and/or project in the list, click the link above to contact the Helpdesk for help locating your grant. Please provide the grant/contract number, project title, and contact PI's name.`}
        </p>
        <H3>How to select an embargo period</H3>
        <p>
          {`The embargo period lets us know the delay from the time the manuscript is published in the journal until it can be published in Europe PMC. The embargo periods listed are those allowed by your funder. `}
        </p>
        <p>
          {`If you’re not sure which embargo period to choose, check your funder's requirements with the `}
          <A href="http://sherpa.ac.uk/fact/" target="_blank">
            SHERPA/FACT tool
          </A>
          {`, or if there is a conflict between the period required by your journal and those allowed by your funder, please contact the Helpdesk.`}
        </p>
      </div>
    )}
    {currentStep === 3 && (
      <div className="createInfo">
        <H2>Selecting a reviewer</H2>
        <H3>What is the reviewer responsible for?</H3>
        <p>
          {`The reviewer is responsible for approving the final web version of the manuscript which will be released to Europe PMC. The designated reviewer must be one of the authors of the manuscript.`}
        </p>
      </div>
    )}
  </React.Fragment>
)

export default CreateInfo
