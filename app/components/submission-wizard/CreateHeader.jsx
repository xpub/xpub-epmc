import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Steps, H1 } from '@pubsweet/ui'

const Title = styled(H1)`
  text-align: center;
`
const Progress = styled.div`
  margin: 0 auto;
  padding: 0 calc(${th('gridUnit')} * 6) calc(${th('gridUnit')} * 9);

  div {
    min-width: 0 !important;
  }
`
const Header = props => (
  <div>
    <Title>Submit a new manuscript</Title>
    <Progress>
      <Steps currentStep={props.currentStep}>
        <Steps.Step title="Citation" />
        <Steps.Step title="Files" />
        <Steps.Step title="Funding" />
        <Steps.Step title="Reviewer" />
      </Steps>
    </Progress>
  </div>
)

export default Header
