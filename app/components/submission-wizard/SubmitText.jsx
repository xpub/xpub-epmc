import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router'
import { th } from '@pubsweet/ui-toolkit'
import { B, Notification } from '../ui/'

const Notif = styled(Notification)`
  border-right: 0;
  border-left: 0;
  margin-top: 0;
  margin-bottom: calc(${th('gridUnit')} * 2);
  p {
    margin-bottom: 0;
  }
`

const SubmitText = ({ currentUser, teams, status, history }) => {
  const submitter = teams.find(team => team.role === 'submitter')
    ? teams.find(team => team.role === 'submitter').teamMembers[0]
    : null
  const reviewer = teams.find(team => team.role === 'reviewer')
    ? teams.find(team => team.role === 'reviewer').teamMembers[0]
    : null

  const AdminWarning = () => (
    <React.Fragment>
      <Notif type="warning">
        <B>This manuscript has not yet been submitted!</B>
        <br />
        {` Its status is '${status}'. Be wary of committing any actions on behalf of a user.`}
      </Notif>
    </React.Fragment>
  )

  if (submitter && submitter.user.id === currentUser.id) {
    if (status === 'submission-error') {
      return (
        <Notif type="error">
          {`Errors have been reported with this submission. Please make any needed corrections and resubmit.`}
        </Notif>
      )
    }
    if (currentUser.admin && status === 'in-review') {
      return <AdminWarning />
    }
    return null
  } else if (reviewer && reviewer.user.id === currentUser.id) {
    return (
      <React.Fragment>
        {status === 'in-review' && (
          <Notif type="info">
            {`You have been designated as the reviewer for this submission. If you are not an author of the manuscript, please reject the manuscript with a note to that effect.`}
            <p>
              {`You may approve this submission as is, or after editing it. You may also reject it with comments for the submitter. After processing, you will be contacted for a final review.`}
            </p>
          </Notif>
        )}
        {status === 'submission-error' && (
          <Notif type="warning">
            {`Errors have been reported with this submission. You may correct and resubmit it yourself, or you will be contacted once the submitter has made corrections.`}
          </Notif>
        )}
      </React.Fragment>
    )
  } else if (currentUser.admin) {
    if (status !== 'submitted') {
      return <AdminWarning />
    }
    return null
  }
  history.push('/')
  return null
}

export default withRouter(SubmitText)
