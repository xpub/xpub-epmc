import React from 'react'
import { omit } from 'lodash'
import styled from 'styled-components'
import { Button, H2, H4, Icon } from '@pubsweet/ui'
import { th, darken } from '@pubsweet/ui-toolkit'
import { B } from '../ui'
import { SubmissionTypes } from '../upload-files'
import PubMedSearch from '../citation-search'
import { FileThumbnails, FileLightbox } from '../preview-files'
import { NoteMutations } from '../SubmissionMutations'
import GrantSearch from '../GrantSearch'
import Citation from './Citation'
import SubmitHighlights from './SubmitHighlights'
import { EditSubmissionReviewer, ErrorMessage } from './SubmitEdit'

const PubMedWithMutations = NoteMutations(PubMedSearch)
const ReviewerWithMutations = NoteMutations(EditSubmissionReviewer)

const ManuscriptDiv = styled.div`
  h4 {
    margin: 0;
    display: inline-block;
  }
  p {
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
  em {
    font-size: ${th('fontSizeBaseSmall')};
    font-style: italic;
    display: inline-block;
    margin-left: calc(${th('gridUnit')} * 2);
  }
  a {
    font-size: ${th('fontSizeBaseSmall')};
  }
`
const IndexedWarning = styled.p`
  color: ${darken('colorWarning', 30)};
  font-size: ${th('fontSizeBaseSmall')};
  display: flex;
  align-items: center;
`

const formatName = name =>
  `${name.title ? `${name.title} ` : ''}${name.givenNames} ${name.surname}`

const submitSections = (
  manuscript,
  checkDupes,
  changeCitation,
  currentUser,
  highlights,
  pruneDupes,
  updateEmbargo,
  updateGrants,
) => {
  const { meta, journal, files: allfiles, status, teams } = manuscript
  if (teams && allfiles) {
    const {
      fundingGroup: grants,
      releaseDelay = '',
      unmatchedJournal,
      notes,
      articleIds,
    } = meta
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []
    const files = allfiles
      ? allfiles.filter(
          file =>
            !file.type || SubmissionTypes.some(t => t.value === file.type),
        )
      : []
    const manFile = allfiles.find(f => f.type === 'manuscript')
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    let reviewer = null
    if (teams && teams.find(team => team.role === 'reviewer')) {
      const rev = teams.find(team => team.role === 'reviewer').teamMembers[0]
      reviewer = {
        id: rev.user.id,
        name: rev.alias.name,
      }
    }
    const selectedReviewer = reviewerNote
      ? JSON.parse(reviewerNote.content)
      : reviewer
    const submitter =
      teams && teams.find(team => team.role === 'submitter')
        ? teams.find(team => team.role === 'submitter').teamMembers[0]
        : null
    const dupeNote = notes
      ? notes.find(n => n.notesType === 'notDuplicates')
      : null
    const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
    const duplicates = checkDupes.filter(d => !notDupes.includes(d.id))
    const pmid = articleIds && articleIds.find(a => a.pubIdType === 'pmid')
    const citationInformation = {
      title: 'Citation',
      content: (
        <React.Fragment>
          <Citation journal={journal} metadata={meta} />
          {currentUser.admin && journal && journal.meta.pubmedStatus && !pmid && (
            <IndexedWarning>
              <Icon color="currentColor" size={2}>
                alert-triangle
              </Icon>
              This journal is indexed, but no PMID is attached
            </IndexedWarning>
          )}
        </React.Fragment>
      ),
      edit: (
        <div data-citation>
          <H2>Citation</H2>
          <PubMedWithMutations
            citationData={changeCitation}
            manuscript={manuscript}
            toggle
          />
        </div>
      ),
      error:
        (currentUser.admin && status === 'submitted' && unmatchedJournal) ||
        duplicates.length > 0 ? (
          <React.Fragment>
            {unmatchedJournal && (
              <ErrorMessage>
                <Icon color="currentColor" size={2}>
                  alert_circle
                </Icon>
                Journal is not in the NLM Catalog.
              </ErrorMessage>
            )}
            {duplicates.length > 0 && (
              <React.Fragment>
                <Button onClick={() => pruneDupes()} primary>
                  Resolve duplicates
                </Button>
                <ErrorMessage>
                  <Icon color="currentColor" size={2}>
                    alert_circle
                  </Icon>
                  Submission is likely a duplicate.
                </ErrorMessage>
              </React.Fragment>
            )}
          </React.Fragment>
        ) : null,
    }
    const manuscriptFiles = {
      title: 'Files',
      content: (
        <React.Fragment>
          {manFile && (
            <ManuscriptDiv>
              <H4>Manuscript file</H4>
              {currentUser.admin && manFile.mimeType === 'application/pdf' && (
                <em>
                  {allfiles.find(f => f.type === 'pdf4load')
                    ? 'This PDF will be shown in PMC'
                    : 'PMC PDF will be generated'}
                </em>
              )}
              <p>
                <FileLightbox file={manFile} withTime />
              </p>
            </ManuscriptDiv>
          )}
          <FileThumbnails files={files} />
        </React.Fragment>
      ),
      edit: <div data-upload />,
      error:
        files &&
        files
          .filter(
            file =>
              !file.type || SubmissionTypes.some(s => s.value === file.type),
          )
          .some(file => !file.label || !file.type) ? (
          <ErrorMessage>
            <Icon color="currentColor" size={2}>
              alert_circle
            </Icon>
            All files must have a type and a label.
          </ErrorMessage>
        ) : null,
    }

    const fundingInfo = {
      title: 'Funding',
      content: (
        <div>
          <p>
            <B>Grants: </B>
            {fundingGroup &&
              fundingGroup.length > 0 &&
              fundingGroup.map((f, t) => (
                <span key={f.awardId}>
                  {`${f.fundingSource} ${f.awardId} `}({f.pi.surname})
                  {t !== fundingGroup.length - 1 && ', '}
                </span>
              ))}
          </p>
          <p>
            <B>Embargo: </B>
            {(releaseDelay || typeof releaseDelay === 'number') &&
              `${releaseDelay} month${
                parseInt(releaseDelay, 10) === 1 ? '' : 's'
              }`}
          </p>
        </div>
      ),
      edit: (
        <GrantSearch
          changedEmbargo={updateEmbargo}
          changedGrants={updateGrants}
          selectedEmbargo={releaseDelay}
          selectedGrants={fundingGroup}
        />
      ),
      error:
        (currentUser.admin ||
          (selectedReviewer.id && currentUser.id === selectedReviewer.id)) &&
        (!fundingGroup || fundingGroup.length === 0 || !releaseDelay) ? (
          <ErrorMessage>
            <Icon color="currentColor" size={2}>
              alert_circle
            </Icon>
            {(!fundingGroup || fundingGroup.length === 0) &&
              'Grants from Europe PMC Funders must be listed.'}
            {(!fundingGroup || fundingGroup.length === 0) && !releaseDelay && (
              <br />
            )}
            {!releaseDelay && 'Embargo period must be set.'}
          </ErrorMessage>
        ) : null,
    }

    const reviewerSelect = {
      title: `${reviewerNote ? 'Invited r' : 'R'}eviewer`,
      content: selectedReviewer && (
        <p>
          {selectedReviewer.id && submitter.user.id === selectedReviewer.id ? (
            `Manuscript Submitter (${formatName(submitter.alias.name)})`
          ) : (
            <React.Fragment>
              {formatName(selectedReviewer.name)}
              {selectedReviewer.id &&
                currentUser.id === selectedReviewer.id &&
                ' (Me)'}
            </React.Fragment>
          )}
        </p>
      ),
      edit:
        !reviewer || currentUser.id !== reviewer.id ? (
          <ReviewerWithMutations
            currentUser={currentUser}
            data-reviewer
            funding={fundingGroup}
            manuscript={manuscript}
            reviewer={reviewer}
            reviewerNote={reviewerNote}
            submitter={submitter}
          />
        ) : null,
      error: !selectedReviewer && (
        <ErrorMessage>
          <Icon color="currentColor" size={2}>
            alert_circle
          </Icon>
          Reviewer must be indicated.
        </ErrorMessage>
      ),
    }

    const sections = [
      citationInformation,
      manuscriptFiles,
      fundingInfo,
      reviewerSelect,
    ]

    const highlightTerms = {
      title: 'Referenced attachments',
      content: <SubmitHighlights highlights={highlights} />,
      edit: '',
      error: '',
    }
    if (currentUser.admin && status === 'submitted') {
      sections.splice(2, 0, highlightTerms)
    }

    return sections
  }
}

export default submitSections
