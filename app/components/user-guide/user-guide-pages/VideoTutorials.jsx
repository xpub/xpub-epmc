import React from 'react'
import { H2, H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { A } from '../../ui'

const Videos = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  margin: 0 calc(${th('gridUnit')} * -2);
  & > div {
    flex: 1 1 50%;
    padding: calc(${th('gridUnit')} * 2);
    h3 {
      margin: calc(${th('gridUnit')} * 2) auto ${th('gridUnit')};
    }
    p {
      margin-top: ${th('gridUnit')};
    }
  }
`
const IFrameWrapper = styled.div`
  position: relative;
  overflow: hidden;
  padding-top: 56.25%;
`
const IFrame = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0;
`

const videos = [
  {
    title: 'Creating a submission',
    link: '6Df99dp-njM',
    length: '3:15',
  },
  {
    title: 'Reviewing a submission',
    link: 'e_1zRpp0XTw',
    length: '1:38',
  },
  {
    title: 'Checking your submissions status',
    link: 'oKNB8EG9J5s',
    length: '0:57',
  },
  {
    title: 'Requesting changes after a submission is complete',
    link: 'D39IktJg_vk',
    length: '0.50',
  },
]

export const VideoTutorials = () => (
  <React.Fragment>
    <H2>Video tutorials</H2>
    <Videos>
      {videos.map(({ link, title, length }) => (
        <div>
          <IFrameWrapper>
            <IFrame
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              frameBorder="0"
              src={`https://www.youtube.com/embed/${link}`}
              title={title}
            />
          </IFrameWrapper>
          <H3>
            <A href={`https://www.youtube.com/watch?v=${link}`} target="_blank">
              {title}
            </A>
          </H3>
          <p>
            {length} minutes
            <br />
            Subtitles
          </p>
        </div>
      ))}
    </Videos>
  </React.Fragment>
)
