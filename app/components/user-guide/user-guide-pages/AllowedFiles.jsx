import React from 'react'
import * as mime from 'mime-types'
import { H2, H3, H4 } from '@pubsweet/ui'
import { FileTypes } from '../../upload-files'
import { Table } from '../../ui'

export const AllowedFiles = () => {
  const mimetypes = FileTypes.reduce((types, type) => {
    const mimetype = type.substring(0, type.indexOf('/'))
    if (mime.extension(type) && !types.some(t => t === mimetype)) {
      types.push(mimetype)
    }
    return types
  }, [])
  return (
    <React.Fragment>
      <H2>Acceptable file types</H2>
      <H3>Manuscript text file</H3>
      <p>This file can be either a PDF or Microsoft Word file.</p>
      <H3>Figures and supplemental files</H3>
      {mimetypes.map(type => (
        <div key={type}>
          <H4>{type.charAt(0).toUpperCase() + type.slice(1)}</H4>
          <Table>
            <tbody>
              <tr>
                <th>MIME type</th>
                <th>Default file extension</th>
              </tr>
              {FileTypes.filter(subtype => subtype.startsWith(type)).map(
                subtype =>
                  mime.extension(subtype) && (
                    <tr key={subtype}>
                      <td>{subtype}</td>
                      <td>.{mime.extension(subtype)}</td>
                    </tr>
                  ),
              )}
            </tbody>
          </Table>
        </div>
      ))}
    </React.Fragment>
  )
}

export default AllowedFiles
