import React from 'react'
import { Field } from 'formik'
import { capitalize } from 'lodash'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import {
  Action,
  Button,
  H2,
  Icon,
  TextField,
  Checkbox,
  CheckboxGroup,
} from '@pubsweet/ui'
import { Buttons, TextArea, Portal, CloseModal } from '../ui'

const Recipients = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 3);
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
`
const Recipient = styled.fieldset`
  border: 0;
  padding: 0;
  & > label:first-child {
    ${override('ui.Label')};
  }
`

const SubjectInput = props => <TextField label="Subject" {...props.field} />
const MessageInput = props => (
  <TextArea label="Message" rows={15} {...props.field} />
)
const CCInput = props => (
  <TextField
    label="CC (enter comma-separated email addresses)"
    {...props.field}
  />
)
const CCMeInput = props => (
  <Checkbox
    checked={props.field.value}
    label="Send me a copy"
    name={props.field.name}
    onChange={e => {
      props.form.setFieldValue('bcc', e.target.checked)
    }}
  />
)

class MailerForm extends React.Component {
  state = { showCC: false }
  // Assumption: there is at most one submitter and one reviewer
  setRef = elem => {
    this.el = elem
  }
  render() {
    const { props, state } = this
    const { showCC } = state
    const people = props.currentUser.admin
      ? []
      : [
          {
            value: 'helpdesk',
            label: 'Europe PMC Helpdesk',
          },
        ]
    props.manuscript.teams.forEach(t => {
      if (t.teamMembers[0].user.id !== props.currentUser.id) {
        const i = people.findIndex(p => p.value === t.teamMembers[0].user.id)
        if (i >= 0) {
          people[i].label = `${people[i].label}/${capitalize(t.role)}`
        } else {
          const { title, givenNames, surname } = t.teamMembers[0].alias.name
          people.push({
            value: t.teamMembers[0].user.id,
            label: `${
              title ? `${title} ` : ''
            } ${givenNames} ${surname}, ${capitalize(t.role)}`,
          })
        }
      }
    })
    return (
      <Portal transparent>
        <CloseModal onClick={() => props.close()} />
        <H2>Send an email</H2>
        <form onSubmit={props.handleSubmit} ref={this.setRef}>
          <Recipients>
            <Recipient>
              <label>Recipient(s)</label>
              <CheckboxGroup
                name="recipients"
                onChange={checked => {
                  props.setFieldValue('recipients', checked)
                }}
                options={people}
                value={props.values.recipients}
              />
              {!props.currentUser.admin && (
                <Field component={CCMeInput} name="bcc" />
              )}
            </Recipient>
            <Action
              onClick={() => this.setState({ showCC: true })}
              style={{
                display: showCC ? 'none' : 'inline-flex',
                alignItems: 'center',
              }}
            >
              <Icon color="currentColor" size={2.5}>
                plus
              </Icon>
              CC
            </Action>
          </Recipients>
          <div
            className={!showCC ? 'hidden' : ''}
            style={{ marginBottom: '1em' }}
          >
            <Field component={CCInput} name="cc" />
          </div>
          <Field component={SubjectInput} name="subject" />
          <Field component={MessageInput} name="message" />
          <Buttons right>
            <Button primary type="submit">
              Send
            </Button>
            <Button onClick={() => props.close()}>Cancel</Button>
          </Buttons>
        </form>
      </Portal>
    )
  }
}

export default MailerForm
