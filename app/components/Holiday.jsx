import React from 'react'
import { Icon } from '@pubsweet/ui'
import { th, lighten } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { notification } from 'config'
import moment from 'moment'
import { Notification } from './ui'

const Christmas = styled(Notification)`
  background-color: ${lighten('colorWarning', 70)};
  div {
    display: flex;
    align-items: flex-start;
    .text {
      margin-left: ${th('gridUnit')};
    }
  }
`
const start = moment(notification.holiday.start).format('dddd, D MMMM')
const end = moment(notification.holiday.end).format('dddd, D MMMM')

const Holiday = props => (
  <Christmas>
    <Icon color="green" size={2.5}>
      gift
    </Icon>
    <Icon color="#a80202" size={2.5}>
      gift
    </Icon>
    <span className="text">
      Please note that the Helpdesk will be unavailable from {start} to {end}.
      Any calls or e-mails will be saved and responded to on our return. Happy
      Holidays from the Europe PMC team.{' '}
    </span>
  </Christmas>
)

export default Holiday
