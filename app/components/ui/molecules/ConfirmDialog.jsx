import React from 'react'
import { Button } from '@pubsweet/ui'
import { Cover, Modal, Buttons } from '../'

const ConfirmDialog = ({ children, type, message, isOpen, action }) => {
  if (!isOpen) {
    return null
  }
  return (
    <Cover transparent>
      <Modal show={isOpen} style={{ width: '60%', maxWidth: '650px' }}>
        {message}
        <Buttons right>
          <Button onClick={() => action(true)} primary>
            Yes
          </Button>
          <Button onClick={() => action(false)}>No</Button>
        </Buttons>
      </Modal>
    </Cover>
  )
}
export { ConfirmDialog }
