import React from 'react'
import styled, { css } from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'
import { Icon } from '@pubsweet/ui'

const fullscreen = css`
  margin: 0 auto;
  justify-content: center;
  & > span {
    padding-left: calc(${th('gridUnit')} * 2);
    flex: 0 0 30px;
  }
  & > div {
    flex: 0 1 1470px;
  }
  @media screen and (max-width: 1500px) {
    & > span {
      padding-left: ${th('gridUnit')};
      flex: 0 0 15px;
    }
    & > div {
      flex: 0 1 1485px;
    }
  }
`
const Container = styled.div`
  padding: ${th('gridUnit')};
  margin: ${th('gridUnit')} 0;
  width: 100%;
  display: flex;
  align-items: flex-start;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  box-sizing: border-box;
  &.hidden {
    display: none;
  }
  &.warning {
    color: ${th('colorText')};
    background-color: ${th('colorWarning')};
  }
  &.error {
    color: ${th('colorTextReverse')};
    background-color: ${th('colorError')};
    button,
    a:link,
    a:visited {
      color: ${th('colorBackgroundHue')};
      text-decoration: underline;
    }
  }
  &.info {
    color: ${th('colorText')};
    background-color: ${darken('colorBackgroundHue', 7)};
  }
  &.success {
    color: ${th('colorTextReverse')};
    background-color: ${th('colorSuccess')};
    button,
    a:link,
    a:visited {
      color: ${th('colorBackgroundHue')};
      text-decoration: underline;
    }
  }
  & > div,
  & > div * {
    font-size: ${th('fontSizeBaseSmall')};
  }
  ${props => props.fullscreen && fullscreen};
`
const NotifIcon = {
  warning: 'alert-triangle',
  error: 'alert-circle',
  info: 'info',
  success: 'check-circle',
}

const Notification = ({ children, type, className, ...props }) => (
  <Container className={`${className || ''} ${type}`} {...props}>
    {NotifIcon[type] && (
      <Icon color="currentColor" size={2}>
        {NotifIcon[type]}
      </Icon>
    )}
    <div>{children}</div>
  </Container>
)
export { Notification }
