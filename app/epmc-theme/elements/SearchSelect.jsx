import { css } from 'styled-components'
import { lighten, th } from '@pubsweet/ui-toolkit'

export default {
  SelectedContainer: css`
    min-height: 0;
    margin-bottom: ${th('gridUnit')};
  `,
  SelectedLabel: css`
    display: none;
  `,
  Selected: css`
    border-color: ${th('colorSuccess')};
    color: ${th('colorSuccess')};
    &:hover {
      border-color: ${lighten('colorError', 20)};
      color: ${lighten('colorError', 20)} !important;
    }
    &:focus {
      outline: none;
      color: ${th('colorSuccess')};
      box-shadow: ${th('dropShadow')};
    }
  `,
  SelectContainer: css`
    &.focus {
      outline: none;
      & > div,
      ul {
        box-shadow: ${th('dropShadow')};
        border-color: ${th('colorPrimary')};
      }
    }
  `,
  Option: css`
    &:focus {
      outline: none;
      border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorPrimary')};
      border-right: 0;
      border-left: 0;
      box-shadow: ${th('dropShadow')};
    }
  `,
  OptionList: css`
    max-height: calc(${th('gridUnit')} * 30);
  `,
}
