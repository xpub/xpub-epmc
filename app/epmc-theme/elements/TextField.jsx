import { css } from 'styled-components'

const th = name => props => props.theme[name]

const invalid = css`
  border-color: ${th('colorError')};
`

export default {
  Input: css`
    &:focus {
      outline: none;
      border-color: ${th('colorPrimary')};
      box-shadow: ${th('dropShadow')};
    }
    ${props => props.invalidTest && invalid};
  `,
}
