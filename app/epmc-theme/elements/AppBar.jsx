import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default {
  Root: css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: stretch;
    height: 100%;
    max-width: 1500px;
    margin: 0 auto;
    position: relative;
  `,
  Logo: css`
    box-sizing: border-box;
    height: 100%;
    width: 100%;
    max-width: 1000px;
    margin: 0 0 0 auto;
  `,
  LogoLink: css`
    display: block;
    height: 100%;
    width: 100%;
    max-width: 1000px;
    font-size: ${th('fontSizeHeading3')};
    font-weight: 600;
    &,
    &:link,
    &:visited {
      color: ${th('colorTextPlaceholder')};
    }
    &:hover {
      color: ${th('colorTextPlaceholder')};
    }
    @media screen and (max-width: 1300px) {
      font-size: ${th('fontSizeBase')};
    }
  `,
}
