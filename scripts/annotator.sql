CREATE DATABASE annotations_dev;

\c annotations_dev
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TABLE annotations
(
  id uuid DEFAULT uuid_generate_v4 () NOT NULL,
  user_id varchar,
  file_id varchar,
  quote varchar,
  comment varchar,
  ranges json,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT annotations_pkey PRIMARY KEY (id)
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON annotations
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE TABLE annotations_test
(
  id uuid DEFAULT uuid_generate_v4 () NOT NULL,
  user_id varchar,
  file_id varchar,
  quote varchar,
  comment varchar,
  ranges json,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT annotations_test_pkey PRIMARY KEY (id)
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON annotations_test
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
