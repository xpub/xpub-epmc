const path = require('path')

module.exports = {
  rootDir: path.resolve(__dirname, '../../'),
  moduleFileExtensions: ['js', 'json'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
  },
  // add snapshotSerializers and setupFiles if necessary
  // mapCoverage: true,
  coverageDirectory: '<rootDir>/test/unit/coverage',
  // collectCoverageFrom: [
  //   'app/**/*.js',
  //   'config/**/*.js',
  //   'scripts/**/*.js',
  //   'server/**/*.js'
  // ]
}
