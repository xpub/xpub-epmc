How to use the Minio component?

### 1. Create .env.minio in the root directory of the project
```
MINIO_ACCESS_KEY=xxxxxxxxxxxxxxx
MINIO_SECRET_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxx
```
You can define them yourselves, and random strings will do.

### 2. Add to .env
Configuration 1:

Use this configuration if you start the service using `yarn server`

```
MINIO_ACCESS_KEY=xxxxxxxxxxxxxxx
MINIO_SECRET_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxx
MINIO_ENDPOINT='127.0.0.1'
MINIO_PORT=9000
MINIO_SECURITY=false
MINIO_BUCKET=manuscripts
MINIO_UPLOADS_FOLDER_NAME=uploads
```

Configuration 2:

Use this configuration if you start the service using `yarn start`

```
MINIO_ACCESS_KEY=xxxxxxxxxxxxxxx
MINIO_SECRET_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxx
MINIO_ENDPOINT=minio
MINIO_PORT=9000
MINIO_SECURITY=false
MINIO_BUCKET=manuscripts
MINIO_UPLOADS_FOLDER_NAME=uploads
```
In both cases, be sure that the key and secret are the same as those in .env.minio
### 3. REST APIs to use

#### Upload a file
```shell
POST: /api/upload
Return: {"filename":"24383c60-792b-11e8-9f1f-d9194b25e031.pdf"}
```

Request body:

```
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryWfPNVh4wuWBlyEyQ

------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
Content-Disposition: form-data; name="fileType"

supplementary
------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
Content-Disposition: form-data; name="fragmentId"

545
------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
Content-Disposition: form-data; name="file"; filename="attachment.txt"
Content-Type: text/plain

[file content goes there]
------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
```

Once a file has been uploaded successfully, you will be informed of the filename known to Minio for this file. Later on, you can use this filename to download or delete the file.

Also you can login to Minio Web UI to check if the file is uploaded: http://127.0.0.1:9000

#### Download a file
```shell
GET: /api/files/{filename}
```

#### Delete a file
```
DELETE: /api/files/{filename}
```

#### List all files
```
GET: /api/files
```
